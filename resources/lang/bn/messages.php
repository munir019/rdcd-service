<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */
    'language' => 'বাংলা',

    'user' => 'ব্যবহারকারী',
    'citizen_profile' => 'সেবাগ্রহীতার প্রোফাইল',
    'personal_information' => 'ব্যাক্তিগত তথ্য',
    'workplace_information' => 'কর্মস্থলের তথ্য',
    'communication_information' => 'যোগাযোগের তথ্য',
    'information' => 'তথ্য',
    'address' => 'ঠিকানা',
    'documents' => 'ডকুমেন্টস',
    'attachment' => 'সংযুক্তি',
    'name_bengali' => 'নাম (বাংলায়)',
    'name_english' => 'নাম (ইংরেজিতে)',
    'mobile' => 'মোবাইল',
    'email' => 'ইমেইল',
    'gender' => 'জেন্ডার',
    'mother_name' => 'মাতার নাম (বাংলায়)',
    'mother_name_en' => 'মাতার নাম (ইংরেজিতে)',
    'father_name' => 'পিতার নাম (বাংলায়)',
    'father_name_en' => 'পিতার নাম (ইংরেজিতে)',
    'spouse_name' => 'স্বামী/স্ত্রী নাম (বাংলায়)',
    'spouse_name_en' => 'স্বামী/স্ত্রী নাম (ইংরেজিতে)',
    'date_of_birth' => 'জন্ম তারিখ',
    'occupation' => 'পেশা/পদবি',
    'educational_qualification' => 'শিক্ষাগত যোগ্যতা',
    'nationality' => 'জাতীয়তা',
    'nid' => 'জাতীয় পরিচয়পত্র',
    'religion' => 'ধর্ম',
    'passport_number' => 'পাসপোর্ট নম্বর',
    'birth_certificate_no' => 'জন্মনিবন্ধন সনদ',
    'driving_licence_no' => 'ড্রাইভিং লাইসেন্স নম্বর',
    'tin_no' => 'টিন নম্বর',
    'permanent_address' => 'স্থায়ী ঠিকানা',
    'present_address' => 'বর্তমান ঠিকানা',
    'work_address' => 'কর্মস্থলের ঠিকানা',
    'photo' => 'ছবি',
    'nid_verify' => 'জাতীয় পরিচয়পত্র যাচাই করুন',

    /**Employee Profile*/
    'employee_information' => 'কর্মচারীর তথ্য',
    'office_information' => 'অফিসের তথ্য',
    'designation' => 'পদবী',
    'short_name' => 'সংক্ষিপ্ত নাম',
    'office_name' => 'অফিসের নাম',
    'unit_name' => 'ইউনিটের নাম',
    'joining_date' => 'যোগদানের তারিখ',
    'last_office_date' => 'অফিসের শেষ তারিখ',
    'incharge_label' => 'ভারপ্রাপ্ত',
    'designation_level' => 'পদবী স্তর',
    'designation_sequence' => 'পদবী ক্রম',


    /**Menu Item*/
    'profile' => 'প্রোফাইল',
    'myprofile' => 'আমার প্রোফাইল',
    'account_settings' => 'অ্যাকাউন্ট সেটিংস',
    'services_widget' => 'অন্যান্য সেবাসমূহ',


    'about' => 'আমাদের কথা',
    'contact' => 'যোগাযোগ',
    'contact_us' => 'যোগাযোগ',
    'faq' => 'সচরাচর জিজ্ঞাসা',
    'privacy_policy' => 'গোপনীয় নীতিমালা',
    'recent_news' => 'সাম্প্রতিক খবর',
    'help_desk' => 'অভিযোগসমূহ',
    'home' => 'হোম',
    'about_us' => 'আমাদের পরিচিতি',
    'quick_links' => 'কুইক লিঙ্ক',
    'services' => 'সেবা',
    'service_management' => 'সেবা ব্যবস্থাপনা',
    'view_manual' => 'সহায়িকা দেখুন',
    'mygov' => 'মাইগভ',
    'national_portal' => 'জাতীয় তথ্য বাতায়ন',
    'planning_n_implementation' => 'পরিকল্পনা ও বাস্তবায়ন',
    'technical_assistance' => 'কারিগরি সহায়তায়',
    'copyright' => 'কপিরাইট © ২০২১ সমবায় ও পল্লী উন্নয়ন কর্তৃক সংরক্ষিত',

    'learn_more' => 'আরও পড়ুন',
    'login' => 'লগইন',
    'USERNAME' => 'ব্যবহারকারীর মোবাইল',
    'EMAIL_MOBILE_NID' => 'মোবাইল/ইমেল/এনআইডি',
    'PASSWORD' => 'পাসওয়ার্ড',
    'logout' => 'লগ আউট',
    'register' => 'নিবন্ধন',
    'subscribe' => 'সাবস্ক্রাইব করুন',
    'no_account_yet' => 'আপনি এখনও অ্যাকাউন্ট করেন নি?',
    'have_an_account' => 'ইতিমধ্যে একাউন্ট আছে ?',
    'lost_your_password' => 'আপনার পাসওয়ার্ড হারিয়েছেন?',
    'submit' => 'Submit',

    /**Admin Dashboard */
    'dashboard' => 'ড্যাশবোর্ড',

];
