<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */
    'language' => 'English',

    /**User Profile*/
    'user' => 'User',
    'citizen_profile' => 'Citizen Profile',
    'personal_information' => 'Persoanl Information',
    'workplace_information' => 'Workplace Information',
    'communication_information' => 'Communication Information',
    'information' => 'Information',
    'address' => 'Address',
    'documents' => 'Documents',
    'attachment' => 'Attachments',
    'name_bengali' => 'Name (Bengali)',
    'name_english' => 'Name (English)',
    'mobile' => 'Mobile',
    'email' => 'Email',
    'gender' => 'Gender',
    'mother_name' => 'Mother Name (Bengali)',
    'mother_name_en' => 'Mother Name (English)',
    'father_name' => 'Father Name (Bengali)',
    'father_name_en' => 'Father Name (English)',
    'spouse_name' => 'Spouse Name (Bengali)',
    'spouse_name_en' => 'Spouse Name (English)',
    'date_of_birth' => 'Date of Birth',
    'occupation' => 'Occupation',
    'educational_qualification' => 'Educational Qualification',
    'nationality' => 'Nationality',
    'nid' => 'NID',
    'religion' => 'Religion',
    'passport_number' => 'Passport Number',
    'birth_certificate_no' => 'BRN',
    'driving_licence_no' => 'Driving Licence No',
    'tin_no' => 'TIN Number',
    'permanent_address' => 'Permanent Address',
    'present_address' => 'Present Address',
    'work_address' => 'Work Place',
    'photo' => 'Profile Image',
    'nid_verify' => 'Verify NID',

    /**Employee Profile*/
    'employee_information' => 'Employee Information',
    'office_information' => 'Office Information',
    'designation' => 'Designation',
    'short_name' => 'Short Name',
    'office_name' => 'Office Name',
    'unit_name' => 'Unit Name',
    'joining_date' => 'Joining Date',
    'last_office_date' => 'Last Office Date',
    'incharge_label' => 'Incharge',
    'designation_level' => 'Designation Level',
    'designation_sequence' => 'Designation Sequence',

    /**Menu  Items*/
    'profile' => 'Profile',
    'myprofile' => 'My Profile',
    'account_settings' => 'Account Settings',

    'user_notification' => 'citizen Notification',
    'notification' => 'Notification',
    'no_notification' => 'There is no notification!',
    'services_widget' => 'Other Services',

    'language' => 'English',
    'about' => 'About',
    'contact' => 'Contact',
    'contact_us' => 'Contact Us',
    'faq' => 'FAQ',
    'privacy_policy' => 'Privacy Policy',
    'recent_news' => 'Recent News',
    'help_desk' => 'Help Desk',
    'home' => 'Home',
    'about_us' => 'About Us',
    'quick_links' => 'Quick Links',
    'services' => 'Services',
    'service_management' => 'Service Management',
    'view_manual' => 'View Manual',
    'mygov' => 'myGov',
    'national_portal' => 'National Portal',
    'planning_n_implementation' => 'Planning & Implementation',
    'technical_assistance' => 'Technical Assistance',
    'copyright' => 'Copyright ©2021 Rural Development & Cooperative Division',

    'learn_more' => 'Learn More',
    'login' => 'Login',
    'USERNAME' => 'Username',
    'EMAIL_MOBILE_NID' => 'Mobile/Email/NID',
    'CITIZEN_EMPLOYEE_LOGIN' => 'Citizen/Employee Login',
    'PASSWORD' => 'Password',
    'logout' => 'Log Out',
    'register' => 'Register',
    'subscribe' => 'Subscribe',
    'no_account_yet' => 'Didn\'t you account yet ?',
    'have_an_account' => 'Already have an account ?',
    'lost_your_password' => 'Lost your password ?',
    'submit' => 'Submit',

    /**Admin Dashboard */
    'dashboard' => 'Dashboard',

];
