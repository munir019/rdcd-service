@extends('layouts.app')

@section('content')

    <?php
        $helper = new \App\Helpers\AppHelper();
    ?>
    <main class="page-wrapper">
        <div class="container">
            <div class="row">
                <div class="col-3">
                    @include('dashboard.leftbar')
                </div>
                <div class="col-9">
                    <table class="table table-striped">
                        <thead>
                            <tr class="table-primary">
                                <td class="_bg-success _text-white">ক্রমিক</td>
                                <td class="_bg-success _text-white _w-50">সেবার নাম</td>
                                <td class="_bg-success _text-white text-start">আবেদনের তারিখ</td>
                                <td class="_bg-success _text-white text-center">সেবা প্রদানের সম্ভাব্য তারিখ</td>
                                <td class="_bg-success _text-white text-center">আবেদনের অবস্থা</td>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                                $i = 1;
                                foreach ($applicationLog['data'] as $val){
                                    $url = config('app.url').'application/view?aid='.$val['aid'];
                                    echo '<tr>';
                                        echo '<td>'.$helper->engToBngNum($i).'</td>';
                                        echo '<td>';
                                            echo '<a href="'.$url.'">';
                                                echo '<div>'.$val['service_name'].'</div>';
                                                echo '<div>ট্র্যাকিং নম্বরঃ <span class="application-no">'.$helper->engToBngNum($val['uaid']).'</span></div>';
                                            echo '</a>';
                                        echo '</td>';
                                        echo '<td class="text-center">';
                                            if($val['is_submitted']==1){
                                                $appLog = json_decode($val['app_log'],true);
                                                $lastLog = array();
                                                foreach ($appLog as $v){
                                                    if($v['action']=='L2')
                                                        echo $helper->bngOnlyDate(date('Y-m-d',strtotime($v['time'])));
                                                    $lastLog = $v;
                                                }
                                            }
                                        echo '</td>';
                                        echo '<td class="text-center">';
                                            if($val['is_submitted']==1 && strtotime($val['approximate_solve_date'])!='')
                                                echo $helper->bngOnlyDate(date('Y-m-d',strtotime($val['approximate_solve_date'])));
                                        echo '</td>';
                                        echo '<td class="text-center">';
                                            if(!empty($lastLog['action']) && isset($workflowDecision[$lastLog['action']]))
                                                echo $workflowDecision[$lastLog['action']]['name'];
                                            else if(!empty($lastLog['action'])) echo $lastLog['action'];
                                        echo '</td>';
                                    echo '</tr>';
                                    $i = $i + 1;
                                }
                            ?>
                        </tbody>
                    </table>

                    @include('dashboard.pagination')
                </div>
            </div>
        </div>

    </main>

    @include('message.message')
@endsection
