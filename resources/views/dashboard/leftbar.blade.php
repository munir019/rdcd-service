<div class="profile">
    <?php
        echo '<div class="profile-photo">';
        if(isset(session()->get('user')['photo']))
            echo '<img src="'.session()->get('user')['photo'].'" />';
        echo '</div>';

        if(isset(session()->get('user')['name']))
            echo '<div class="profile-name">'.session()->get('user')['name'].'</div>';

        if(isset(session()->get('user')['mobile']))
            echo '<div class="profile-mobile">'.session()->get('user')['mobile'].'</div>';
    ?>
</div>

<div class="dashboard-menu">
    <ul>
        <li>
            <a href="<?php echo config('app.url') ?>profile"><i class="fas fa-user-tie"></i><span>প্রোফাইল</span></a>
        </li>
        <li>
            <a href="<?php echo config('app.url') ?>dashboard"><i class="fas fa-layer-group"></i><span>দাখিলকৃত আবেদনসমূহ</span></a>
        </li>
        <li>
            <a href="<?php echo config('app.url') ?>services"><i class="fas fa-certificate"></i><span>নতুন আবেদন</span></a>
        </li>
        <li>
            <a href="<?php echo config('app.url') ?>application/status"><i class="fas fa-info-circle"></i><span>আবেদনের অবস্থা জানুন</span></a>
        </li>
    </ul>
</div>