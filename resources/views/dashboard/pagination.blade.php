<?php
    $helper = new \App\Helpers\AppHelper();
?>
<div class="">
    <span class="float-start mt-1">
        <span class="me-4">মোট: <b class="text-primary"><?php echo $helper->engToBngNum($applicationLogObj->toArray()['total']); ?></b></span>
        <?php if(empty($search)) { ?>
        প্রতি পৃষ্ঠায়: <select id="per-page" class="form-control form-control-sm w-auto d-inline-block">
            <option value="20" <?php echo ((isset($limit) && $limit==20)?'selected':'') ?>><?php echo $helper->engToBngNum(20) ?></option>
            <option value="30" <?php echo ((isset($limit) && $limit==30)?'selected':'') ?>><?php echo $helper->engToBngNum(30) ?></option>
            <option value="40" <?php echo ((isset($limit) && $limit==40)?'selected':'') ?>><?php echo $helper->engToBngNum(40) ?></option>
            <option value="50" <?php echo ((isset($limit) && $limit==50)?'selected':'') ?>><?php echo $helper->engToBngNum(50) ?></option>
            {{--<option value="all" <?php echo ((isset($limit) && $limit=='all')?'selected':'') ?>>All</option>--}}
        </select>
        <?php } ?>
    </span>
    <span class="float-end">
        {{ $applicationLogObj->links("pagination::bootstrap-4") }}
    </span>
</div>
<script>
    jQuery(document).ready(function() {
        if ($('#per-page').is(':visible')) {
            $('#per-page').change(function () {
                window.location = window.location.href.split('?')[0] + '?limit=' + $(this).val();
            });
        }
    });
</script>