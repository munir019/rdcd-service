<?php
/**
 * Md. Munir Hossain
 */
?>
<!DOCTYPE html>
<html>
<head lang="{{app()->getLocale()}}">
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title><?php echo (isset($title) && trim($title)!=''?$title.' | ':'').$site['title'] ?></title>

    <link rel="stylesheet" href="<?php echo $baseUrl ?>css/bootstrap.min.css?v=<?php echo $version ?>">
    <link rel="stylesheet" href="<?php echo $baseUrl ?>css/fontawesome.min.css?v=<?php echo $version ?>">
    <link rel="stylesheet" href="<?php echo $baseUrl ?>css/app.css?v=<?php echo $version ?>">
    {{-- <link rel="stylesheet" href="<?php echo $baseUrl ?>css/style.css?v=<?php echo $version ?>"> --}}

    <script>
        var baseUrl = '<?php echo $baseUrl ?>';
        var appData = '';
        <?php if(isset($application->data)) { ?>appData = '<?php echo $application->data ?>'; appData = JSON.parse(appData);<?php } ?>
        var officeLayer = '{}';
        <?php if(isset($getFullOfficeLayer)) { ?>officeLayer = '<?php echo json_encode($getFullOfficeLayer, JSON_UNESCAPED_UNICODE) ?>'; <?php } ?>
    </script>

    <script src="<?php echo $baseUrl ?>js/jquery.min.js?v=<?php echo $version ?>"></script>
    <script src="<?php echo $baseUrl ?>js/bootstrap.bundle.min.js?v=<?php echo $version ?>"></script>
    <script src="<?php echo $baseUrl ?>js/sweetalert.min.js?v=<?php echo $version ?>"></script>
    <script src="<?php echo $baseUrl ?>js/app.js?v=<?php echo $version ?>"></script>
    <script src="<?php echo $baseUrl ?>js/office.js?v=<?php echo $version ?>"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.form/4.3.0/jquery.form.min.js"></script>
    <script src="<?php echo $baseUrl ?>js/jquery.easy-autocomplete.min.js?v=<?php echo $version ?>"></script>
    <link rel="stylesheet" href="<?php echo $baseUrl ?>css/easy-autocomplete.min.css?v=<?php echo $version ?>">

    <link rel="stylesheet" href="<?php echo $baseUrl ?>css/cropper.css?v=<?php echo $version ?>">
    <script src="<?php echo $baseUrl ?>js/cropper.js?v=<?php echo $version ?>"></script>

    <link rel="apple-touch-icon" sizes="57x57" href="<?php echo $baseUrl ?>img/favicon/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="<?php echo $baseUrl ?>img/favicon/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="<?php echo $baseUrl ?>img/favicon/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="<?php echo $baseUrl ?>img/favicon/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="<?php echo $baseUrl ?>img/favicon/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="<?php echo $baseUrl ?>img/favicon/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="<?php echo $baseUrl ?>img/favicon/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="<?php echo $baseUrl ?>img/favicon/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="<?php echo $baseUrl ?>img/favicon/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="<?php echo $baseUrl ?>img/favicon/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="<?php echo $baseUrl ?>img/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="<?php echo $baseUrl ?>img/favicon/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="<?php echo $baseUrl ?>img/favicon/favicon-16x16.png">
    {{--<link rel="manifest" href="<?php echo $baseUrl ?>img/favicon/manifest.json">--}}
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="<?php echo $baseUrl ?>img/favicon/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">

    {{-- <script src="https://www.mygov.bd/js/lib/list/list.min.js?v=1.2.27"></script> --}}
</head>
<body>
    <div class="wrapper">
        {{-- @if (url()->current() == trim(config('app.url'), '/')) --}}
            @include('include.header')
        {{--@else
            @include('include.innerpage-header')
        @endif --}}

        @yield('content')

        @include('include.footer')
    </div>
    <!-- Custom scripts -->
    <script src="<?php echo $baseUrl ?>js/script.js?v=<?php echo $version ?>"></script>
    <!-- BEGIN PAGE LEVEL SCRIPTS -->
    @yield('scripts')
    <!-- END PAGE LEVEL SCRIPTS -->
</body>
</html>
