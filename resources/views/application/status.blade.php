@extends('layouts.app')

@section('content')

    <?php
    $helper = new \App\Helpers\AppHelper();
    ?>
    <div class="container innerpage-container py-5">
        <div class="row">
            <div class="col-xl-8 col-lg-8 col-md-10 col-xs-12 col-12 pb-5 service_status_search">
                <div class="bg-light border border-1 rounded p-2 noprint">
                    <form action="{{ url('application/get-status') }}" method="post">
                        <div class="form-group row my-3">
                            <label for="mobile" class="col-form-label col-xl-4 col-lg-4 col-md-4 col-xs-12 col-12 text-xl-center text-lg-center">{{ __('মোবাইল') }}</label>
                            <div class="col-xl-6 col-lg-6 col-md-6 col-xs-12 col-12">
                                <input id="mobile" type="mobile" class="form-control @error('mobile') is-invalid @enderror" name="mobile" value="{{ $mobile??old('mobile') }}" required autocomplete="mobile" autofocus>
                                @error('mobile')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="application_number" class="col-form-label col-xl-4 col-lg-4 col-md-4 col-xs-12 col-12 text-xl-center text-lg-center">{{ __('আবেদনপত্রের নম্বর') }}</label>
                            <div class="col-xl-6 col-lg-6 col-md-6 col-xs-12 col-12">
                                <input id="application_number" type="application_number" class="form-control @error('application_number') is-invalid @enderror" name="application_number" value="{{ $application_number??old('application_number') }}" required autocomplete="application_number" autofocus>
                                @error('application_number')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row my-3">
                            <div class="col-xl-8 col-lg-8 col-md-10 col-xs-12 col-12 offset-md-4">
                                <button type="submit" class="btn btn-primary">অনুসন্ধান করুন</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="col-xl-8 col-lg-8 col-md-10 col-xs-12 col-12 service_status_search">
                <div class="application-status">
                @if (!empty($response))
                    <h5>{{ $response['service_name']??'' }}</h5>
                    <p>আবেদনপত্রের নম্বর: <b>{{ $helper->engToBngNum($application_number??'') }}</b></p>

                    @if (!empty($response) && $response['currentDesk'] != '')
                        @php
                        if(isset($response['steps'])) {
                            $logs = $response['steps'];
                            $lastStep = $logs[count($logs) - 1];
                        }
                        @endphp

                        <p>বর্তমান অবস্থা: <b>{{ $lastStep['action'] }} : {{ $helper->bngDate($lastStep['time']) }}</b></p>
                        <p>বর্তমান ডেস্ক: <b>{{  $response['currentDesk']??'' }}</b></p>

                        @if ($response['steps'])
                        <table class="table table-bordered mt-2">
                            <thead>
                              <tr class="bg-light">
                                <th>#</th>
                                <th>অবস্থা</th>
                                <th>তারিখ এবং সময়</th>
                              </tr>
                            </thead>
                            <tbody>
                                @foreach ($logs as $item)
                                <tr>
                                    <td>{{ $helper->engToBngNum($loop->index+1) }}</td>
                                    <td>{{ $item['action'] }}</td>
                                    <td>{{ $helper->bngDate($item['time']) }}</td>
                                </tr>
                                @endforeach
                            </tbody>
                          </table>
                        @endif
                    @else
                        <h5>দুঃখিত, কোনও তথ্য পাওয়া যায়নি।</h5>
                    @endif
                @endif
                </div>
            </div>
        </div>
    </div>
@endsection
