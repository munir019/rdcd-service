@extends('layouts.app')

@section('content')

    <?php
        $helper = new \App\Helpers\AppHelper();
    ?>
    <div class="container py-3">
        <div class="row">
            <div class="col-3">
                <h5 class="border-bottom p-2">সেবা সংশ্লিষ্ট তথ্য</h5>
                <div class="row">
                    <div class="col-12 mb-2 mt-2">
                        <div class="mb-1">
                            <div class="alert alert-primary p-1 px-2 mb-0"><i class="fas fa-info-circle me-2"></i>সেবা প্রদানের পদ্ধতি</div>
                        </div>
                        <div class="border border-primary rounded p-2">
                            <?php echo $serviceCitizen['way_of_service']; ?>
                        </div>
                    </div>

                    <div class="col-12 mb-2 mt-2">
                        <div class="mb-1">
                            <div class="alert alert-success p-1 px-2 mb-0"><i class="fas fa-wallet me-2"></i>সেবার মূল্য এবং পরিশোধ পদ্ধতি</div>
                        </div>
                        <div class="border border-success rounded p-2">
                            <?php echo $serviceCitizen['payment_details']; ?>
                        </div>
                    </div>

                    <div class="col-12 mb-2 mt-2">
                        <div class="mb-1">
                            <div class="alert alert-warning p-1 px-2 mb-0"><i class="far fa-clock me-2"></i>সেবা প্রদানের সময়সীমা</div>
                        </div>
                        <div class="border border-warning rounded p-2">
                            <?php echo $helper->engToBngNum($serviceCitizen['time_duration']); ?> কার্যদিবস
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-9">
                <h5 class="text-center p-2 border-0 border-bottom"><?php echo ($lang=='en'?$service['name_en']:$service['name']) ?></h5>
                <div class="row">
                    <div class="col-12 mt-2">
                        <div class="application-form">
                            <form id="application-form" method="post" action="<?php echo $baseUrl ?>application/apply?id=<?php echo $service['sid'] ?>">
                                @csrf
                                @include('application.location')
                                <?php

                                    if(isset($application->aid))
                                        echo '<input type="hidden" name="aid" value="'.$application->aid.'"/>';
                                    echo '<input type="hidden" name="is_complete" value=""/>';

                                    echo $forms['html_form'];
                                ?>
                            </form>
                            <div class="text-end">
                                <span class="btn btn-primary apply-application" data-type="apply"><i class="far fa-paper-plane me-2"></i>আবেদন প্রেরণ করুন</span>
                                <span class="btn btn-success apply-application" data-type="draft"><i class="far fa-save me-2"></i>ভবিষ্যতের জন্য সংরক্ষণ করুন</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="modal fade" id="cropImageModal" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="cropImageModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content border-0">
                <div class="modal-header">
                    <h6 class="modal-title" id="cropImageModalLabel" obd="cropImage-title">Resize Signature</h6>
                    <button type="button" class="close px-1 bg-danger text-white border-0 rounded" data-bs-dismiss="modal" aria-label="Close">
                        <i class="fa fa-times-circle"></i>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="img-container">
                        <div class="row">
                            <div class="col-12">
                                <span class="alert alert-primary p-1 d-block" obd="cropImage-title-1">Your Signature</span>
                                <div class="text-center">
                                    <img id="imageForCropped">
                                </div>
                            </div>
                            <div class="col-12 mt-2">
                                <span class="alert alert-success p-1 d-block mb-2" obd="cropImage-title-2">New resize signature</span>
                                <div class="text-center">
                                    <div obd="previewCroppedImageBox">
                                        <div obd="previewCroppedImage" class="mx-auto"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-sm btn-secondary" data-bs-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-sm btn-primary" obd="cropSignature">Crop</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="previewApplicationModal" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="previewApplicationModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content border-0">
                <div class="modal-header">
                    <h6 class="modal-title" id="cropImageModalLabel" obd="cropImage-title"><?php echo $service['name'] ?></h6>
                    <button type="button" class="close px-1 bg-danger text-white border-0 rounded" data-bs-dismiss="modal" aria-label="Close">
                        <i class="fa fa-times-circle"></i>
                    </button>
                </div>
                <div class="modal-body">

                </div>
                <div class="modal-footer">
                    <button class="btn btn-primary apply-application-btn" data-type="apply"><i class="far fa-paper-plane me-2"></i>আবেদন প্রেরণ করুন</button>
                    <button class="btn btn-success apply-application-btn" data-type="draft"><i class="far fa-save me-2"></i>ভবিষ্যতের জন্য সংরক্ষণ করুন</button>
                    <button type="button" class="btn btn-danger" data-bs-dismiss="modal"><i class="fa fa-times-circle me-2"></i>বন্ধ করুন</button>
                </div>
            </div>
        </div>
    </div>
@endsection
