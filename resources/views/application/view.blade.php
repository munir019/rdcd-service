@extends('layouts.app')

@section('content')

    <?php
    $helper = new \App\Helpers\AppHelper();
    ?>
    <div class="container py-3">
        <div class="row">
            <div class="col-3">
                <h5 class="border-bottom p-2">সেবা সংশ্লিষ্ট তথ্য</h5>
                <div class="row">
                    <div class="col-12 mb-2 mt-2">
                        <div class="mb-1">
                            <div class="alert alert-primary p-1 px-2 mb-0"><i class="fas fa-info-circle me-2"></i>সেবা প্রদানের পদ্ধতি</div>
                        </div>
                        <div class="border border-primary rounded p-2">
                            <?php echo $serviceCitizen['way_of_service']; ?>
                        </div>
                    </div>

                    <div class="col-12 mb-2 mt-2">
                        <div class="mb-1">
                            <div class="alert alert-success p-1 px-2 mb-0"><i class="fas fa-wallet me-2"></i>সেবার মূল্য এবং পরিশোধ পদ্ধতি</div>
                        </div>
                        <div class="border border-success rounded p-2">
                            <?php echo $serviceCitizen['payment_details']; ?>
                        </div>
                    </div>

                    <div class="col-12 mb-2 mt-2">
                        <div class="mb-1">
                            <div class="alert alert-warning p-1 px-2 mb-0"><i class="far fa-clock me-2"></i>সেবা প্রদানের সময়সীমা</div>
                        </div>
                        <div class="border border-warning rounded p-2">
                            <?php echo $helper->engToBngNum($serviceCitizen['time_duration']); ?> কার্যদিবস
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-9 printable">
                <h5 class="text-center p-2 border-0 border-bottom"><?php echo ($lang=='en'?$service['name_en']:$service['name']) ?></h5>
                <div class="bg-light border border-1 rounded p-2 noprint text-end">
                    <a href="" class="btn btn-sm btn-info"><i></i>৳ পেমেন্ট করুন</a>
                    <a href="<?php echo config('app.url').'application/modify?aid='.$application->aid ?>" class="btn btn-sm btn-warning"><i class="fas fa-pencil-alt me-2"></i>সংশোধন করুন</a>
                    <a href="<?php echo config('app.url').'application/view?aid='.$application->aid ?>#print" target="_blank" class="btn btn-sm btn-primary"><i class="fas fa-print me-2"></i>প্রিন্ট করুন </a>
                    <a href="<?php echo config('app.url').'application/status?aid='.$application->aid ?>" class="btn btn-sm btn-success"><i class="fas fa-info me-2"></i>আবেদনের অবস্থা জানুন</a>
                </div>
                <div class="row">
                    <div class="col-12 mt-2">
                        <div class="application-form">
                            <form method="post" action="<?php echo $baseUrl ?>application/apply?id=<?php echo $service['sid'] ?>">
                                @csrf
                                {{--@include('application.location')--}}
                                <?php
                                    //echo $forms['html_form'];
                                    echo $data;
                                ?>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
