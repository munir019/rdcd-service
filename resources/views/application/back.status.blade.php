@extends('layouts.app')

@section('content')

    <?php
    $helper = new \App\Helpers\AppHelper();
    ?>
    <div class="container innerpage-container py-5">
        <div class="row">
            <div class="col-12">
                <div class="bg-light border border-1 rounded p-2 noprint text-end">
                    <a href="<?php echo config('app.url').'application/view?aid='.$applicationLog['aid'] ?>" class="btn btn-sm btn-warning"><i class="fas fa-angle-left me-2"></i>ফিরে যান </a>
                </div>
            </div>

            <div class="col-12">
                <div class="row bs-wizard border-0">

                    <?php
                    $stepByDecision = array();
                    if(isset($applicationLog['app_log'])) {
                        $logs = json_decode($applicationLog['app_log'], true);
                        if(is_array($logs)) {
                            krsort($logs);
                            foreach ($logs as $key => $lastLog) break;

                            $lastStep = $lastLog['action'];
                            foreach ($workflowStep as $key => $value) {
                                if (in_array($lastStep, json_decode($value['decision'], true))) {
                                    $lastStep = $key;
                                    break;
                                }
                            }

                            foreach ($workflowStep as $key => $value) {
                                $tmp = json_decode($value['decision'], true);
                                foreach($tmp as $k=>$v){
                                    $stepByDecision[$v] = array('key'=>$key);
                                }
                            }

                            if(isset($serviceDecision['step']))
                                $steps = json_decode($serviceDecision['step'], true);
                            $totalStep = count($steps);
                        }
                    }

                    if(!empty($applicationLog)) {
                        $color = array('#f48f22', '#f85490', '#4b8ddf', '#3aa821', '#814adc', '#d22486', '#b8a611', '#eff13c', '#f4ab75', '#75f4f2');
                        $finalColor = array();

                        $i = 1;
                        $action = '';
                        if (isset($steps) && is_array($steps)) {
                            foreach ($steps as $key => $value) {
                                if (!empty($workflowStep[$value]['name'])) {
                                    $finalColor[$value] = $color[$i - 1];
                                    if ($lastStep == $value) {
                                        $action = 'active';
                                        $lastStep = 0;
                                    } else if ($lastStep >= 1) $action = 'complete';
                                    else if ($lastStep == 0) $action = 'disabled';
                                    else $action = 'disabled';

                                    echo '<div class=" bs-wizard-step ' . $action . '" style="width:' . (100 / $totalStep) . '%">';
                                    echo '<div class="text-center bs-wizard-stepnum"></div>';//'.$this->engToBngNum($i).'
                                    echo '<div class="progress"><div class="progress-bar"></div></div>';
                                    echo '<a href="#" class="bs-wizard-dot"></a>';
                                    echo '<div class="bs-wizard-info text-center">' . $workflowStep[$value]['name'] . '</div>';
                                    echo '</div>';
                                    $i = $i + 1;
                                }
                            }
                        }
                    }
                    ?>
                </div>
            </div>

            <div class="col-12">

                <div class="application-status">
                    <?php

                        if(!empty($applicationLog)) {
                        ?>
                        <h4><?php echo $applicationLog['service_name'] ?></h4>
                        <h5>আবেদনপত্রের নম্বর: <?php echo $helper->engToBngNum($applicationLog['aid']) ?></h5>
                        <?php
                        echo '<h5>বর্তমান অবস্থা: ' . (isset($workflowDecision[$lastLog['action']]) ? $workflowDecision[$lastLog['action']]['name'] : $lastLog['action']) . ', ' . $helper->bngDate($lastLog['time']) . '</h5>';
                        if(!empty($track_info)) {
                            echo '<h5>দপ্তর: '.(isset($office_info['office'])?$office_info['office']:'').'</h5>';
                            echo '<h5>বর্তমান ডেস্ক: '.$track_info.'</h5>';
                        }

                        echo '<table class="application-status-history">';
                        echo '<tr>';
                        echo '<th style="border-left: 5px solid #707f91">#</th>';
                        echo '<th>অবস্থা</th>';
                        echo '<th>তারিখ এবং সময়</th>';
                        echo '</tr>';
                        $i = 1;
                        ksort($logs);
                        $previousAction = '';
                        foreach ($logs as $k => $v) {

                            //if($previousAction!=$v['action'])
                            {
                                $borderColor = 'border-left: 5px solid #707f91';
                                if(is_numeric($v['action']))
                                    $borderColor = 'border-left: 5px solid '.$finalColor[$stepByDecision[$v['action']]['key']];

                                echo '<tr data="'.(isset($v['note'])?'feedback-needed':'').'">';
                                echo '<td style="'.$borderColor.'"><span>' . $helper->engToBngNum($i) . '</span></td>';
                                echo '<td>' . (isset($workflowDecision[$v['action']]) ? $workflowDecision[$v['action']]['name'] : $v['action']) . (isset($v['note']) ? '<div class="">' . $v['note'] . '</div>' : '') . '</td>';
                                echo '<td>' . $helper->bngDate($v['time']) . '</td>';
                                echo '</tr>';
                                $i = $i + 1;
                                //$previousAction = $v['action'];
                            }
                        }
                        echo '</table>';

                        echo '<ul class="steps-indicate">';

                        foreach($steps as $key=>$value){
                            if(isset($workflowStep[$value]['name'])) {
                                echo '<li> <span class="steps-dot" style="background: ' . $finalColor[$value] . '"></span>'. $workflowStep[$value]['name'] . '</li>';
                            }
                        }
                        echo '</ul>';

                    }else {
                        if(isset($search_id))
                            echo '<h5>দুঃখিত, কোনও তথ্য পাওয়া যায়নি।</h5>';
                    }
                    ?>
                </div>
            </div>
        </div>
    </div>
@endsection
