<div class="alert alert-primary p-1 px-2 mb-0" id="office-selection">
    <div class="row">
        <div class="col-4">
            <div class="mt-1">প্রাপক অফিস</div>
        </div>
        <div class="col-8">
            <div class="row">
                <div class="col-12 mb-3">
                    <select id="new_office_layer_category" data="<?php echo (isset($organization['office_layer'])?$organization['office_layer']:'') ?>" name="office_layer" class="form-control form-control-sm">
                        <option value="">বাছাই করুন</option>
                        <?php
                            if(isset($getFullOfficeLayer['ministryOffices']))
                                echo '<option value="1">মন্ত্রণালয়/বিভাগ</option>';
                            if(isset($getFullOfficeLayer['doptorOfffices']))
                                echo '<option value="2">অধিদপ্তর</option>';
                            if(isset($getFullOfficeLayer['otherOffices']))
                                echo '<option value="3">অন্যান্য দপ্তর/সংস্থা</option>';
                            if(isset($getFullOfficeLayer['divisionOffices']))
                                echo '<option value="4">বিভাগীয় পর্যায়ের কার্যালয়</option>';
                            if(isset($getFullOfficeLayer['districtOffices']))
                                echo '<option value="5">জেলা পর্যায়ের কার্যালয়</option>';
                            if(isset($getFullOfficeLayer['upazilaOffices']))
                                echo '<option value="6">উপজেলা পর্যায়ের কার্যালয়</option>';
                            if(isset($getFullOfficeLayer['localOffices']))
                                echo '<option value="7">আঞ্চলিক কার্যালয়</option>';
                        ?>
                    </select>
                </div>

                <div class="col-12 mb-3">
                    <select id="new_office_layer_origin" name="office_origin" class="form-control form-control-sm" data="<?php echo (isset($organization['office_origin'])?$organization['office_origin']:'') ?>"></select>
                </div>
                <div class="col-12 mb-3">
                    <select id="new_office_layer_district" name="office_district" class="form-control form-control-sm" data="<?php echo (isset($organization['office_district'])?$organization['office_district']:'') ?>"></select>
                </div>
                <div class="col-12 mb-3">
                    <select id="new_office_layer_office" name="office_id" class="form-control form-control-sm" data="<?php echo (isset($organization['office_id'])?$organization['office_id']:'') ?>" required></select>
                </div>
            </div>
        </div>
    </div>
</div>