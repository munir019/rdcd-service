<ul class="top-nav">
    {{-- <li class="item1"><a target="_blank" href="https://doptor.gov.bd">নথি</a></li> --}}
    <li class="item2"><a target="_blank" href="https://www.mygov.bd">{{ __('messages.mygov', [], session()->get('locale')) }}</a></li>
    <li class="item3"><a target="_blank" href="https://bangladesh.gov.bd">{{ __('messages.national_portal', [], session()->get('locale')) }}</a></li>
    <li class="item4"><a href="#">{{ __('messages.view_manual', [], session()->get('locale')) }}</a></li>
</ul>
