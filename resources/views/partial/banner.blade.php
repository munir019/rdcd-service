<div class="row">
    <div class="homepage-banner col-lg-12 col-md-12 col-sm-12 col-12">
        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
            <div class="banner-left">
                <div class="slide-title">পল্লী উন্নয়ন ও সমবায় বিভাগের সকল সেবা এক ঠিকানায়</div>
                <form class="search-container" method="post" action="{{$baseUrl}}search">
                    @csrf
                    <input type="text" name="searchtext" id="service_search" class="search-key" placeholder="সেবা অনুসন্ধান করুন">
                    {{-- <span id="home_search_btn"><i class="fas fa-search"></i></span> --}}
                    <button class="button-site-search"><i class="fas fa-search"></i></button>
                </form>
            </div>
        </div>
        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
            <div class="banner-right">
                <img src="{{ asset('img/home/banner.png') }}" alt="Priminister in RDCD">
            </div>
        </div>
    </div>
</div>
