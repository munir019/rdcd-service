@extends('layouts.app')

@section('content')
    <main class="page-wrapper">
        <!--Page Banner-->
        @if (url()->current() == trim(config('app.url'), '/'))
            @include('partial.top_banner')
        @else
            @include('include.innerpage-banner')
        @endif
        <!--inner-page-content-->
        <div class="inner-page-container">
            <div class="innerpage-content">
                <div class="row">
                    <div class="col-12">
                        <h2 class="page-header">গোপনীয়তার নীতিমালা</h2>
                        <div>
                            <p>বাংলাদেশ সরকার এবং তথ্য ও যোগাযোগ প্রযুক্তি বিভাগ আপনার ব্যক্তিগত তথ্যাবলীর গোপনীয়তা বজায় রাখবে। আপনি আপনার ব্যক্তিগত কোন তথ্যাবলী প্রদান না করেও এই ওয়েবসাইট ব্রাউজ করতে পারবেন। যদি আপনি আপনার সম্পর্কে কোন তথ্য দিতে ইচ্ছুক থাকেন, সেক্ষেত্রে আমরা সে তথ্যাবলী সংরক্ষণ করব। আমরা সেক্ষেত্রে অন্যান্য সরকারি অফিস এবং সংস্থার সাথে এসব তথ্যাবলী বিনিময় করতে পারি। যেসব তথ্যাবলী সংগ্রহ করা হবে তা শুধুমাত্র দাপ্তরিক উদ্দেশ্যে, অভ্যন্তরীণ পর্যালোচনা, ওয়েবসাইটের কনটেন্টের মানোন্নয়নের ক্ষেত্রে ব্যবহৃত হবে। বাণিজ্যিক উদ্দেশ্যে আপনার তথ্যাবলী ব্যবহার করা হবে না। কোন ধরনের আইনী প্রয়োজন যেমন-সার্চ ওয়ারেন্ট কিংবা আদালতের আদেশের ক্ষেত্রে বাংলাদেশ সরকার যে কোন ব্যক্তির ব্যক্তিগত তথ্য প্রকাশ করতে পারে।</p>
                            <p>আপনি যখন ওয়েবসাইটের মাধ্যমে ই-মেইল, কোন জরিপে অংশগ্রহণ করেন অথবা কোন বিষয়ে মতামত প্রদান অথবা যোগাযোগের ক্ষেত্রে কোন তথ্য প্রদান করে থাকেন সেক্ষেত্রে আপনার ব্যক্তিগত তথ্যাবলী সংগ্রহ করা সম্ভব হবে। আপনার প্রশ্নের সঠিক উত্তর প্রদানের জন্য আপনার তথ্যগুলো অন্যান্য সরকারি এজেন্সী অথবা ব্যক্তির নিকট তা প্রেরণ করা হতে পারে । এই ওয়েবসাইটটিতে সরকারের অন্যান্য এজেন্সী বা সংগঠনের সাথে লিংক প্রদান করা হয়েছে। আপনি যখন এই সাইট ব্যতীত অন্য সাইট ব্যবহার করছেন সেক্ষেত্রে নতুন সাইটের গোপনীয়তার নীতিমালা প্রযোজ্য হবে। </p>
                            <p>কোন প্রকার নোটিশ ব্যতীত তথ্য ও যোগাযোগ প্রযুক্তি বিভাগ যেকোন সময় এই নীতিমালা সংশোধন করতে পারে। অথবা এই ওয়েবসাইটের ব্যবহারকারীগণকে নোটিশের মাধ্যমে জানাতে পারে। যেকোন তথ্যাদি যা বর্তমান নীতিমালার মাধ্যমে সংরক্ষণ করা হয়েছিল তা এসব শর্তাবলী মেনে চলবে। নীতিমালার পরিবর্তনের পর যদি কোন তথ্যাদি সংগ্রহ করা হয় তা অবশ্যই পরিবর্তিত নীতিমালার মাধ্যমে পালনীয় হবে।</p>

                            <p>আরো তথ্যের জন্য যোগাযোগ করুনঃ</p>

                            <p><b>তথ্য ও যোগাযোগ প্রযুক্তি বিভাগ</b><br/>
                            ই-১৪/এক্স, আইসিটি টাওয়ার, আগারগাঁও<br />
                            শেরে-বাংলানগর, ঢাকা-১২০৭<br />
                            ফ্যাক্স: +৮৮-০২-৮১৮১৫৬৫<br />
                            ই-মেইল: info@ictd.gov.bd</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
@endsection
