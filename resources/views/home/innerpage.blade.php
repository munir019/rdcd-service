@extends('layouts.app')

@section('content')
    <main class="page-wrapper">
        <!--Page Banner-->
        @include('include.innerpage-banner')

        <!--services-->
        @include('include.service')
    </main>
@endsection
