@extends('layouts.app')

@section('content')
    <main class="page-wrapper">
        <!--Page Banner-->
        {{-- @include('include.innerpage-banner') --}}

        <!--search-page-content-->
        <div class="inner-page-container py-5">
            <div class="container">
                <div class="row">
                    <div class="col-xl-8 col-lg-10 col-md-12 col-xs-12 col-12 pb-2 service_status_search">
                        <div class="bg-light border border-1 rounded p-2 noprint">
                            <form action="{{$baseUrl}}search" method="post">
                                @csrf
                                <div class="form-group row my-4">
                                    <h5 class="col-xl-10 col-lg-10 col-md-12 col-xs-12 col-12 text-center px-5">পল্লী উন্নয়ন ও সমবায় বিভাগের সেবা অনুসন্ধান</h5>
                                </div>
                                <div class="form-group row my-3">
                                    <label for="searchtext" class="col-form-label col-xl-3 col-lg-3 col-md-3 col-xs-12 col-12 text-xl-center text-lg-center">{{ __('সেবা অনুসন্ধান করুন') }}</label>
                                    <div class="col-xl-7 col-lg-9 col-md-9 col-xs-12 col-12">
                                        <input type="searchtext" id="service_search" class="form-control @error('searchtext') is-invalid @enderror" name="searchtext" value="{{ $searchtext??old('searchtext') }}" required autocomplete="searchtext" autofocus>
                                        @error('searchtext')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group row my-4">
                                    <div class="col-xl-9 col-lg-9 col-md-10 col-xs-12 col-12 offset-md-8">
                                        <button type="submit" class="btn btn-primary">অনুসন্ধান করুন</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="col-xl-12 col-lg-12 col-md-12 col-xs-12 col-12">
                        <div class="service-search-result">
                            @if (count($services) > 0)
                                <h5 class="search-header">অনুসন্ধান ফলাফল: <b>{{$searchtext}}</b> নামে {{AppHelper::engToBngNum(count($services))}} টি সেবা পাওয়া গেছে</h5>
                            @else
                                <h5 class="search-header">অনুসন্ধান ফলাফল: <b>{{$searchtext}}</b> নামে সেবা খুঁজে পাওয়া যায় নি</h5>
                            @endif
                            <ul>
                            @foreach ($services as $service)
                                <li>
                                    <a href="{{$baseUrl}}services/info?id={{ $service['sid'] }}">
                                        <i class="fa fa-angle-right"></i>&nbsp;&nbsp;{{ $service['name'] }}
                                    </a>
                                    <div class="service-search-note">
                                        {{-- @if ($service->serviceRecipient)
                                            <span>সেবাগ্রহীতা: <b>{{ $service->serviceRecipient->name }}</b></span>
                                        @endif --}}
                                        @if ($service['sector_name'])
                                            <span>খাত: <b>{{ $service['sector_name'] }}</b></span>
                                        @endif
                                        @if ($service['type_name'])
                                            <span>দপ্তর: <b>{{ $service['type_name'] }}</b></span>
                                        @endif
                                    </div>
                                </li>
                            @endforeach
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
@endsection
