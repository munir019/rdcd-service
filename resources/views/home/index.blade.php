@extends('layouts.app')

@section('content')
    <main class="page-wrapper">
        <!--Page Banner-->
        @if (url()->current() == trim(config('app.url'), '/'))
            @include('partial.banner')
        @else
            {{-- @include('include.innerpage-banner') --}}
        @endif
        <!--Home page services-->
        @include('include.services')
    </main>
@endsection
