<div class="col-12 px-5" id="office_id">
    <div class="service-list-title">সেবা সমূহ</div>
    <div id="service-lists" class="my-search">
        <input class="form-control form-control-sm my-search-input" type="text" placeholder="সেবা বাছাই করুন">
        <ul class="my-search-list">
            @php $i = 1; @endphp
            @if (isset($serviceByCat['g2c']) && $serviceByCat['g2c'])
                @foreach ($serviceByCat['g2c'] as $service)
                    @if ($loop->iteration == 1)
                        <li class="service-item service-item-citizen">নাগরিক সেবা</li>
                    @endif
                    <li>
                        <a target="_blank" href="{{$baseUrl}}services/info?id={{$service->id}}&office={{$officeId}}">
                            <span class="sl">{{ AppHelper::engToBngNum($i++) }}</span>
                            {{ $service->name }}
                        </a>
                    </li>
                @endforeach
            @endif
            @if (isset($serviceByCat['g2g']) && $serviceByCat['g2g'])
                @foreach ($serviceByCat['g2g'] as $service)
                    @if ($loop->iteration == 1)
                        <li class="service-item service-item-gov">অভ্যন্তরীণ সেবা</li>
                    @endif
                    <li>
                        <a target="_blank" href="{{$baseUrl}}services/info?id={{$service->id}}&office={{$officeId}}">
                            <span class="sl">{{ AppHelper::engToBngNum($i++) }}</span>
                            {{ $service->name }}
                        </a>
                    </li>
                @endforeach
            @endif
        </ul>
    </div>
</div>
