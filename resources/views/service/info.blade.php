@extends('layouts.app')

@section('content')
    {{-- @include('include.innerpage-banner') --}}
    <?php
        $helper = new \App\Helpers\AppHelper();
    ?>
    <div class="container innerpage-container py-5">
        <div class="row">
            <div class="col-12">
                <div class="alert alert-primary p-2"><?php echo ($lang=='en'?$service['name_en']:$service['name']) ?></div>
            </div>
            <div class="col-12 text-end mb-3">
                <?php

                    $dToken = '';
                    if($service['recipient']==3){
                        if(isset(session()->get('employee')->username)){
                            $doptorUser = array();
                            $doptorUser['gid'] = session()->get('employee')->username;
                            $doptorUser['employeeRecordId'] = session()->get('employee')->employee_id;
                            $doptorUser['id'] = session()->get('employee')->employee_id;
                            $doptorUser['officeId'] = session()->get('employee')->office_id;
                            $doptorUser['designation'] = session()->get('employee')->designation_bn;
                            $doptorUser['officeUnitId'] = session()->get('employee')->office_unit_id;
                            $doptorUser['InchargeLabel'] = session()->get('employee')->incharge_label;
                            $doptorUser['officeUnitOrganogramId'] = session()->get('employee')->office_unit_organogram_id;
                            $doptorUser['officeNameEng'] = session()->get('employee')->office_name_en;
                            $doptorUser['officeNameBng'] = session()->get('employee')->office_name_bn;
                            $doptorUser['unitNamEng'] = session()->get('employee')->unit_name_en;
                            $doptorUser['unitNameBng'] = session()->get('employee')->unit_name_bn;
                            $doptorUser['oisf_designation'] = $doptorUser['designation'];
                            $doptorUser['oisf_identification'] = $doptorUser['gid'];

                            $doptorUser = json_encode($doptorUser, JSON_UNESCAPED_UNICODE);
                            $dToken = '&dtoken='.urlencode($doptorUser);
                        }
                    }

                    $myGovUrl = config('services.mygov.mygov_url').'/';
                    $url = $myGovUrl.'service/?id='.$service['sid'];
                    //if(empty(session()->get('employee'))){
                        if($service['recipient'] == 3)
                            $url = $myGovUrl.'doptor/login?redirect='.urlencode($url).'%23step2'.$dToken;
                        else
                            //$url = 'https://www.training.mygov.bd/users/login?redirect=https%3A%2F%2Fwww.training.mygov.bd%2Fservice%2F%3Fid%3DBDGS-1638251650%23step2';
                            $url = $baseUrl.'login'.'?redirect='.urlencode($myGovUrl.'users/login?redirect='.urlencode($url).'#step2');
                    //}

                    if(!($service['office']== null)){
                        if($service['sid'] == 'BDGS-1639370655'){
                            if($cooperativeSocietyAppUrl == ''){
                                $url = 'http://beneficiary.rdcd.orangebd.com';
                            }else{
                                $url = $cooperativeSocietyAppUrl;
                            }
                        }
                ?>
                    <a href="<?php echo $url ?>" class="btn btn-success py-2 px-3 d-inline-block"><i class="far fa-paper-plane me-2"></i>আবেদন করুন</a>
                <?php } ?>
            </div>
            <div class="col-6">
                <div class="border rounded p-2">
                    <h5 class="border-bottom bg-light p-2">সেবা সংশ্লিষ্ট তথ্য</h5>
                    <div class="row">
                        <div class="col-12 mb-2 mt-2">
                            <div class="mb-2">
                                <span class="alert alert-primary p-1 px-2"><i class="fas fa-info-circle me-2"></i>সেবা প্রদানের পদ্ধতি</span>
                            </div>
                            <div class="border bg-light rounded p-2">
                                <?php echo $serviceCitizen['way_of_service']; ?>
                            </div>
                        </div>

                        <div class="col-12 mb-2 mt-2">
                            <div class="mb-2">
                                <span class="alert alert-success p-1 px-2"><i class="fas fa-wallet me-2"></i>সেবার মূল্য এবং পরিশোধ পদ্ধতি</span>
                            </div>
                            <div class="border bg-light rounded p-2">
                                <?php echo $serviceCitizen['payment_details']; ?>
                            </div>
                        </div>

                        <div class="col-12 mb-2 mt-2">
                            <div class="mb-2">
                                <span class="alert alert-warning p-1 px-2"><i class="far fa-clock me-2"></i>সেবা প্রদানের সময়সীমা</span>
                            </div>
                            <div class="border bg-light rounded p-2">
                                <?php echo $helper->engToBngNum($serviceCitizen['time_duration']); ?> কার্যদিবস
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-6">
                <div class="border rounded p-2">
                    <h5 class="border-bottom bg-light p-2">আবেদন ফরম পূরণের নিয়মাবলী</h5>
                    <div class="common-service-information">
                        <ul>
                            <li>১। আবেদন ফরমের লাল তারকা চিহ্নিত ঘরগুলো অবশ্যই পূরণ করুন। অন্যান্য ঘরগুলো পূরণ ঐচ্ছিক।</li>
                            <li>২। আবেদন প্রক্রিয়া সম্পন্ন হওয়ার পূর্বে প্রয়োজন হলে সংরক্ষণ করা যায় এবং পরবর্তীতে সেবা ব্যবস্থাপনা অপশন হতে ড্রাফট আবেদন পুনরায় শুরু করা যাবে।</li>
                            <li>৩। আবেদন দাখিলের পর প্রতিটি আবেদনের জন্য একটা স্বতন্ত্র ট্রাকিং নম্বর প্রদান করা হবে যেটা ব্যবহার করে সেবা ব্যবস্থাপনা অপশন হতে আবেদনের অগ্রগতি জানা যাবে।</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
