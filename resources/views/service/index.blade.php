@extends('layouts.app')

@section('content')
    {{-- @include('include.innerpage-banner') --}}

    <?php
        $helper = new \App\Helpers\AppHelper();
    ?>
    <div class="container innerpage-container py-5">
        <div class="row">
            {{-- <div class="breadcrumb col-12">মন্ত্রণালয় / দপ্তর অনুসারে সেবা সমূহ</div> --}}
            @php
                    //dd(session()->get('employee'));
                    //echo $officeId;
            @endphp
            <div class="col-4">
                <ul class="service-category-list">
                    <li class="active">
                        <div class="service-category">
                            <i class="fa fa-sitemap mx-2"></i>অফিস অনুসারে
                            <i id="expand-cat" class="fas fa-caret-down float-end mt-1"></i>
                        </div>
                        <ul class="office-list">
                            @foreach ($officeWiseServices as $office)
                                @if(session()->get('employee'))
                                    @if(($office->office_layer == 1 || $office->office_layer == 2) && (session()->get('employee')->office_id == 95 || session()->get('employee')->office_id == 148))
                                    <li>
                                        <a class="office {{ ($office->office_id == $officeId)?'active':'inactive' }}" data-office="{{$office->office_id}}" href="#">
                                        <i class="fa fa-angle-right"></i>{{ $office->doptorname }}
                                        </a>
                                    </li>
                                    @elseif(session()->get('employee')->office_id == $office->office_id && $office->office_layer == 3)
                                    <li>
                                        <a class="office {{ ($office->office_id == $officeId)?'active':'inactive' }}" data-office="{{$office->office_id}}" href="#">
                                        <i class="fa fa-angle-right"></i>{{ $office->doptorname }}
                                        </a>
                                    </li>
                                    @endif
                                @else
                                    <li>
                                        <a class="office {{ ($office->office_id == $officeId)?'active':'inactive' }}" data-office="{{$office->office_id}}" href="#">
                                        <i class="fa fa-angle-right"></i>{{ $office->doptorname }}
                                        </a>
                                    </li>
                                @endif
                            @endforeach
                        </ul>
                    </li>
                </ul>
            </div>
            <div class="col-8">
                <div class="row" id="officeContainer">
                    <div class="col-12 px-5">
                        <div class="service-list-title">সেবা সমূহ</div>
                        <div id="service-lists" class="my-search">
                            <input class="form-control form-control-sm my-search-input" type="text" placeholder="সেবা বাছাই করুন">
                            <ul class="my-search-list">
                                @php $i = 1; @endphp
                                @if (isset($serviceByCat['g2c']) && $serviceByCat['g2c'])
                                    @foreach ($serviceByCat['g2c'] as $service)
                                        @if ($loop->iteration == 1)
                                            <li class="service-item service-item-citizen">নাগরিক সেবা</li>
                                        @endif

                                        <?php /*@if ($service->id == 'BDGS-1638251650')*/ ?>
                                        <li class="service-item">
                                            <a target="_blank" href="{{$baseUrl}}services/info?id={{$service->id}}&office={{$officeId}}">
                                                <span class="sl">{{ $helper->engToBngNum($i++) }}</span>
                                                {{ $service->name }}
                                            </a>
                                        </li>
                                        <?php /*@else
                                        <li>
                                            <a target="_blank" href="https://training.mygov.bd/service/?id={{$service->id}}">
                                                <span class="sl">{{ $helper->engToBngNum($i++) }}</span>
                                                {{ $service->name }}
                                            </a>
                                        </li>
                                        @endif*/ ?>
                                    @endforeach
                               @endif

                               @if (isset($serviceByCat['g2g']) && $serviceByCat['g2g'])
                                    @foreach ($serviceByCat['g2g'] as $service)
                                        @if ($loop->iteration == 1)
                                            <li class="service-item service-item-gov">অভ্যন্তরীণ সেবা</li>
                                        @endif

                                        <?php /*@if ($service->id == 'BDGS-1638251650')*/ ?>
                                        <li class="service-item">
                                            <a target="_blank" href="{{$baseUrl}}services/info?id={{$service->id}}&office={{$officeId}}">
                                                <span class="sl">{{ $helper->engToBngNum($i++) }}</span>
                                                {{ $service->name }}
                                            </a>
                                        </li>
                                        <?php /*@else
                                        <li>
                                            <a target="_blank" href="https://training.mygov.bd/service/?id={{$service->id}}">
                                                <span class="sl">{{ $helper->engToBngNum($i++) }}</span>
                                                {{ $service->name }}
                                            </a>
                                        </li>
                                        @endif*/ ?>
                                    @endforeach
                               @endif
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @section('scripts')
    <script>
        $(document).ready(function() {
            $('.office').click(function() {

                if($(this).data('office') != ''){
                    $.ajax({
                        url: "{{url('/get-service-list')}}/"+$(this).data('office'),
                        dataType: 'JSON',
                        success: function(data) {
                            $('#officeContainer').html(data.serviceListHTML);
                        },
                        error: function (xhr, status, error){
                            if(status == 'error')
                                console.log(error);
                        }
                    });
                }
            });

        });
    </script>
	@stop
@endsection
