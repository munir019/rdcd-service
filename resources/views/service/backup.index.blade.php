@extends('layouts.app')

@section('content')
    {{-- @include('include.innerpage-banner') --}}

    <?php
        $helper = new \App\Helpers\AppHelper();
    ?>
    <div class="container pb-5">
        <div class="row">
            <div class="breadcrumb col-12">মন্ত্রণালয় / দপ্তর অনুসারে সেবা সমূহ</div>
            <div class="col-3">
                <ul class="service-category-list">
                    <li class="<?php echo ($category=='office'?'active':'inactive') ?>">
                        <div class="service-category">
                            <i class="fa fa-sitemap mx-2"></i>দপ্তর অনুসারে
                            <i id="expand-cat" class="fas fa-caret-<?php echo ($category=='office'?'down':'right') ?> float-end mt-1"></i>
                        </div>
                        <ul>
                            <?php
                                foreach($serviceCategory['officeLayer'] as $k=>$v){
                                    echo '<li>';
                                        echo '<a data-service="office-level-'.$k.'" href="'.$baseUrl.'services?category=office&data=office-level-'.$k.'">';
                                        echo '<i class="fa fa-angle-right"></i>';
                                        if($lang=='bn')
                                            echo $v['name'];
                                        else
                                            echo $v['name_en'];
                                        echo '</a>';
                                    echo '</li>';
                                }
                            ?>
                        </ul>
                    </li>
                    {{-- <li class="<?php echo ($category=='sector'?'active':'inactive') ?>">
                        <div class="service-category">
                            <i class="fas fa-cubes mx-2"></i>খাত অনুসারে
                            <i id="expand-cat" class="fas fa-caret-<?php echo ($category=='sector'?'down':'right') ?> float-end mt-1"></i>
                        </div>
                        <ul>
                            <?php
                            foreach($serviceCategory['sector'] as $k=>$v){
                                echo '<li>';
                                echo '<a data-service="office-level-'.$k.'" href="'.$baseUrl.'services?category=sector&data=service-sector-'.$k.'">';
                                echo '<i class="fa fa-angle-right"></i>';
                                if($lang=='bn')
                                    echo $v['name'];
                                else
                                    echo $v['name_en'];
                                echo '</a>';
                                echo '</li>';
                            }
                            ?>
                        </ul>
                    </li>
                    <li class="<?php echo ($category=='recipient'?'active':'inactive') ?>">
                        <div class="service-category">
                            <i class="fas fa-users mx-2"></i>সেবাগ্রহীতা অনুসারে
                            <i id="expand-cat" class="fas fa-caret-<?php echo ($category=='recipient'?'down':'right') ?> float-end mt-1"></i>
                        </div>
                        <ul>
                            <?php
                            foreach($serviceCategory['recipient'] as $k=>$v){
                                echo '<li>';
                                echo '<a data-service="office-level-'.$k.'" href="'.$baseUrl.'services?category=recipient&data=service-recipient-'.$k.'">';
                                echo '<i class="fa fa-angle-right"></i>';
                                if($lang=='bn')
                                    echo $v['name'];
                                else
                                    echo $v['name_en'];
                                echo '</a>';
                                echo '</li>';
                            }
                            ?>
                        </ul>
                    </li>
                    <li class="<?php echo ($category=='all'?'active':'inactive') ?>">
                        <div class="service-category">
                            <i class="fas fa-list mx-2"></i>সকল সেবা
                            <i id="expand-cat" class="fas fa-caret-<?php echo ($category=='recipient'?'down':'right') ?> float-end mt-1"></i>
                        </div>
                        <ul>
                            <?php
                            foreach($serviceCategory['recipient'] as $k=>$v){
                                echo '<li>';
                                echo '<a data-service="office-level-'.$k.'" href="'.$baseUrl.'services?category=all">';
                                echo '<i class="fa fa-angle-right"></i>';
                                if($lang=='bn')
                                    echo $v['name'];
                                else
                                    echo $v['name_en'];
                                echo '</a>';
                                echo '</li>';
                            }
                            ?>
                        </ul>
                    </li> --}}
                </ul>
            </div>
            <div class="col-9">
                <div class="row">
                <?php
                    if(!empty($officeLevel)){
                        echo '<div class="col-5 px-0">';
                            // echo '<div class="office-list-title">দপ্তর সমূহ</div>';
                            echo '<div class="my-search bg-light">';
                                if($data=='office-level-3'){
                                    echo '<select data-id="'.($level3!=''?$level3:'1').'" class="form-control form-control-sm office-level">';
                                    $i = 1;
                                    foreach($officeLevel as $k=>$v){
                                        echo '<option value="'.$i.'">'.$k.'</option>';
                                        $i = $i + 1;
                                    }
                                    echo '</select>';

                                    $j = 1;
                                    foreach($officeLevel as $val){
                                        echo '<div class="office-level3 office-level3-'.$j.' d-none">';
                                        echo '<input class="form-control form-control-sm my-search-input" type="text" placeholder="বাছাই করুন">';
                                        echo '<ul class="my-search-list origin-list">';
                                        $i = 1;
                                        foreach ($val as $k=>$v){
                                            echo '<li>';
                                            echo '<a href="'.$baseUrl.'services?category='.$category.($data!=''?'&data='.$data:'').($origin!=''?'&origin='.$k:'').'&level3='.$j.'" class="'.($k==$origin?'active':'').'">';
                                            echo ($lang=='en'?$v['name_en']:$v['name']);
                                            echo '</a>';
                                            echo '<i class="fa fa-angle-right me-2 float-end"></i>';
                                            echo '</li>';
                                            $i = $i + 1;
                                        }
                                        echo '</ul>';
                                        echo '</div>';
                                        $j = $j + 1;
                                    }
                                }else{
                                    echo '<input class="form-control form-control-sm my-search-input" type="text" placeholder="বাছাই করুন">';
                                    echo '<ul class="my-search-list origin-list">';
                                        $i = 1;
                                        foreach($officeLevel as $k=>$v){
                                            echo '<li>';
                                                echo '<a href="'.$baseUrl.'services?category='.$category.($data!=''?'&data='.$data:'').($v['id']!=''?'&origin='.$v['id']:'').'" class="'.($v['id']==$origin?'active':'').'">';
                                                    echo '<i class="fa fa-angle-right me-2 float-end"></i>';
                                                    echo ($lang=='en'?$v['name_en']:$v['name']);
                                                echo '</a>';
                                            echo '</li>';
                                            $i = $i + 1;
                                        }
                                    echo '</ul>';
                                }
                            echo '</div>';
                        echo '</div>';
                    }
                ?>
                    <div class="<?php echo (empty($officeLevel)?'col-12':'col-7') ?>  px-0">
                        {{-- <div class="service-list-title">সেবা সমূহ</div> --}}
                        <div id="service-lists" class="my-search">
                            <input class="form-control form-control-sm my-search-input" type="text" placeholder="সেবা বাছাই করুন">
                            <ul class="my-search-list">
                                <?php
                                    $i = 1;
                                    foreach($services as $v){
                                        echo '<li>';
                                            echo '<a href="'.$baseUrl.'services/info?id='.$v['sid'].'">';
                                                echo '<span class="sl">'.$helper->engToBngNum($i).'.</span>';
                                                echo ($lang=='en'?$v['name_en']:$v['name']);
                                            echo '</a>';
                                        echo '</li>';
                                        $i = $i + 1;
                                    }
                                ?>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
