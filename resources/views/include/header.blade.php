<?php
/**
 * Created by PhpStorm.
 * User: Md. Munir Hossain
 * Date: 06/06/2021
 * Time: 01:43 PM
 */
?>
<header class="homepage-header">
    <div class="row">
        <div class="col-lg-6 col-md-7 col-sm-7 col-6">
            <div class="header-left">
                <a href="/" class="home-page-logo"></a>
            </div>
        </div>
        <div class="col-lg-6 col-md-5 col-sm-5 col-6">
            <div class="header-right">
                <div class="nav-container">
                    <nav>
                        <ul class="top-nav-right">
                            @php
                                //dd(session()->get('user'));
                            @endphp
                                <li class="item4"><button class="helpline" data-bs-toggle="modal" data-bs-target="#helplineModal">হেল্প ডেস্ক</button></li>

                            @if (session()->has('citizen') || session()->has('employee'))
                                <li class="user-menu">
                                    <div class="dropdown">
                                        <span class="user-name">
                                            @if(session()->has('employee'))
                                                {{  session()->get('employee')->name_bn?? session()->get('employee')->username }}
                                            @else
                                                {{ session()->get('citizen')->name }}
                                            @endif
                                        </span>
                                        <button class="dropdown-toggle dd-user-menu" type="button" data-bs-toggle="dropdown">
                                            @if(session()->has('employee'))
                                                <img class="profile-img" src="{{ session()->get('employee')->photo??asset('img/user-profile-img.png')  }}" alt="{{ session()->get('employee')->name_bn }}">
                                            @else
                                            <img class="profile-img" src="{{ session()->get('citizen')->photo??asset('img/user-profile-img.png')  }}" alt="{{ session()->get('citizen')->name }}">
                                            @endif

                                        </button>
                                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="user-menu">
                                            @if(session()->has('employee') || session()->has('citizen'))
                                            <a class="dropdown-item" href="{{ config('services.mygov.mygov_url') }}/dashboard">
                                                <i class="fas fa-tachometer-alt"></i>সেবা ব্যবস্থাপনা
                                            </a>
                                            @endif
                                            @if(session()->has('citizen'))
                                            <a class="dropdown-item" href="{{$baseUrl}}logout">
                                                <i class="fas fa-sign-out-alt"></i>লগআউট
                                            </a>
                                            @endif
                                            @if(session()->has('employee'))
                                            <a class="dropdown-item" href="{{$baseUrl}}doptorlogout">
                                                <i class="fas fa-sign-out-alt"></i>লগআউট
                                            </a>
                                            @endif
                                        </div>
                                    </div>
                                </li>
                            @else
                                <li class="item5"><button class="registration"><a target="_blank" href="https://cdap.mygov.bd/registration">নিবন্ধন</a></button></li>
                                <li class="item6"><button class="login" data-bs-toggle="modal" data-bs-target="#loginModal">লগইন</button></li>
                            @endif
                        </ul>
                    </nav>
                </div>
            </div>
        </div>
    </div>
</header>
@include('include.login-modal')
@include('include.helpline')
