<!-- Inner page banner -->
<div class="innerpage-banner">
    <img src="{{ asset('img/banner/banner_brdb.png') }}" alt="BRDB">
    <div class="innerpage-logo-container">
        @if (request()->category == 'office' && (request()->data == 'office-level-1' || request()->data == 'office-level-2'))
            {{-- <img class="innerpage-logo" src="img/logo.png" alt="Logo Title"> --}}
        @elseif(request()->category == 'office' && request()->data == 'office-level-3' &&  !empty(request()->name))
            <img class="innerpage-logo" src="img/logo/{{request()->name}}_logo_large.png" alt="Logo Title">
        @endif
    </div>
    <div class="innerpage-search-container">
        <div class="innerpage-search">
            <!-- <img class="search-icon" src="images/icon/search-icon.png" >
            <span class="search-text">আপনার কাঙ্খিত সেবাটি খুঁজুন</span> -->
            <form class="innerpage-search-form" method="post" action="{{$baseUrl}}search">
                @csrf
                <input type="text" name="searchtext" class="search-key" id="service_search" placeholder="আপনার কাঙ্খিত সেবাটি খুঁজুন">
                <button class="button-innerpage-search">খুঁজুন</button>
            </form>
        </div>
    </div>
</div>
