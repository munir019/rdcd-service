<header class="inner-page-header">
    <div class="innerpage-nav-container">
        <div class="top-nav-left">
            <a href="/" class="home-link">
                <img src="{{ asset('img/logo_small.png') }}" alt="RDCD Logo">
            </a>
        </div>
        <nav>
            @include('partial.topnav')
            <ul class="top-nav-right">
                @if (empty(session()->get('user')))
                    <li class="item5"><button class="registration">{{ __('messages.register', [], session()->get('locale')) }}</button></li>
                    <li class="item6"><button class="login" data-bs-toggle="modal" data-bs-target="#loginModal">{{ __('messages.login', [], session()->get('locale')) }}</button></li>
                @else
                    <li class="user-menu">
                        <div class="dropdown">
                            <span class="user-name" title="{{ session()->get('user')['name'] }}">{{ session()->get('user')['name'] }}</span>
                            <button class="dropdown-toggle dd-user-menu" type="button" data-bs-toggle="dropdown">
                                <img class="profile-img" src="{{ session()->get('user')['photo']  }}" alt="{{ session()->get('user')['name'] }}">
                            </button>
                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="user-menu">
                                <a class="dropdown-item" href="{{$baseUrl}}dashboard">
                                    <i class="fas fa-tachometer-alt"></i>{{ __('messages.service_management', [], session()->get('locale')) }}
                                </a>
                                <a class="dropdown-item" href="{{$baseUrl}}logout">
                                    <i class="fas fa-sign-out-alt"></i>{{ __('messages.logout', [], session()->get('locale')) }}
                                </a>
                            </div>
                        </div>
                    </li>
                @endif
                <div class="dropdown">
                    <select class="form-select" id="dropdownMenuButton">
                        <option value="bn" @php if(session()->get('locale') == 'bn') echo 'selected';  @endphp>বাংলা</option>
                        <option value="en" @php if(session()->get('locale') == 'en') echo 'selected';  @endphp>English</option>
                    </select>
                </div>
            </ul>
        </nav>
    </div>
</header>
@include('include.login-modal')
