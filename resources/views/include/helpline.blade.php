<div class="modal fade" id="helplineModal" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="helplineModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content border-0">
            <div class="modal-header">
                <h6 class="modal-title" id="viewModalLabel">হেল্প ডেস্ক</h6>
                <button type="button" class="close border-0 rounded" data-bs-dismiss="modal" aria-label="Close">
                    <i class="fa fa-times-circle"></i>
                </button>
            </div>
            <div class="modal-body">
                <div class="sub-panel text-center">
                    <h5 class="helpline-header">প্ল্যাটফর্ম সংক্রান্ত যেকোনো সমস্যা / জিজ্ঞাসা</h5>
                </div>
                <p class="text-center mt-4">ফোন করুন: <span class="color-primary">+880 1713243659</span>
                    <br>
                    অথবা
                    <br>
                    ই-মেইল করুন: <a href="mailto:rdcd.idsdp@gmail.com"><span class="color-primary">rdcd.idsdp@gmail.com</span></a>
                </p>
            </div>
            <div class="modal-footer">
            </div>
        </div>
    </div>
</div>
