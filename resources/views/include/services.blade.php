<div class="page-container2">
    <div class="page-content">
        <div class="services-header">
            <h2 class="block-header2">সেবা তালিকা</h2>
        </div>

        <div class="service-block-container container">
            <div class="row">
                @foreach ($officeWiseServices as $office)
                    <div class="col-lg-4 col-md-6 col-sm-12 col-12">
                        <div class="service-block">
                            <div class="service-block-header">
                                <a href="{{ $baseUrl }}services?office={{$office->office_id}}&data=office-level-{{$office->office_layer}}">
                                    <img class="service-logo" src="{{ asset('img/logo/'.$office->logo) }}">
                                </a>
                                <h2 class="block-title">
                                    <a href="{{ $baseUrl }}services?category=office&office={{$office->office_id}}&data=office-level-{{$office->office_layer}}">{{ $office->doptorname }}</a>
                                </h2>
                            </div>

                            <ul class="service-list">
                                @foreach (array_slice($office->services, 0, 3) as $service)
                                    <?php /*@if ($service->id == 'BDGS-1638251650')*/ ?>
                                        <li class="service-item">
                                            <a target="_blank" href="{{$baseUrl}}services/info?id={{$service->id}}&office={{$office->office_id}}">{{ Str::limit($service->name, 50) }}</a>
                                        </li>
                                    <?php /*@else
                                        <li class="service-item">
                                            <a target="_blank" href="https://training.mygov.bd/service/?id={{$service->id}}">{{ Str::limit($service->name, 50) }}</a>
                                        </li>
                                    @endif*/ ?>
                                @endforeach
                            </ul>

                            @if (count($office->services) > 3)
                                <div class="readmore-container">
                                    <a class="readmore" href="{{ $baseUrl }}services?category=office&office={{$office->office_id}}&data=office-level-{{$office->office_layer}}">আরও</a>
                                </div>
                            @endif
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
</div>
<div class="category-wise-service-block-container">
    <div class="inportant-service-and-info-block">
        {{-- <h2 class="service-header"><img class="title-left" src="{{ asset('img/home/title_left_bg.png') }}">গুরুত্বপূর্ণ সেবা ও তথ্য সমূহ<img class="title-right" src="{{ asset('img/home/title_right_bg.png') }}"></h2> --}}
        <div class="service-list">
            <div class="service-item">
                <img class="service-item-icon" src="{{ asset('img/home/doptor_count_icon.png') }}">
                <span class="title">দপ্তরের সংখ্যা (১১টি)</span>
            </div>
            <div class="service-item">
                <img class="service-item-icon" src="{{ asset('img/home/service_count_icon.png') }}">
                <span class="title">সর্বমোট সেবার সংখ্যা</span>
            </div>
            <div class="service-item">
                <a href="http://forms.mygov.bd" target="_blank">
                    <img class="service-item-icon" src="{{ asset('img/home/form_download_icon.png') }}">
                </a>
                <a href="http://forms.mygov.bd" target="_blank">আবেদনফরম ডাউনলোড করুন</a>
            </div>
            <div class="service-item">
                <a href="#" target="_blank">
                    <img class="service-item-icon" src="{{ asset('img/home/service_tracking_icon.png') }}">
                </a>
                <a href="#" target="_blank">অনিবন্ধিত সদস্যদের নিবন্ধন</a>
            </div>
            <div class="service-item">
                <a href="{{$baseUrl}}application/status" target="_blank">
                    <img class="service-item-icon" src="{{ asset('img/home/service_wise_policy.png') }}">
                </a>
                <a href="{{$baseUrl}}application/status" target="_blank">সেবা ট্র্যাকিং করুন</a>
            </div>
        </div>
    </div>
</div>
