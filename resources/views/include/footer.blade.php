<?php
/**
 * Created by PhpStorm.
 * User: Md. Munir Hossain
 * Date: 06/06/2021
 * Time: 01:43 PM
 */
?>
<footer class="page-footer">
    <div class="row">
        <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 col-6">
            <div class="footer-left-block footer-block">
                <div class="text-label">{{ __('messages.planning_n_implementation', [], session()->get('locale')) }}</div>
                <div class="partner-logo">
                    <a href="https://a2i.pmo.gov.bd" target="_blank">
                        <img src="{{ asset('img/home/a2i_logo.png') }}" alt="A2I Logo">
                    </a>
                </div>
            </div>
        </div>
        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-0 col-0 d-none d-md-block">
            {{-- <div class="footer-middle-block footer-block">
                <nav>
                    <ul class="footer-nav">
                        <li class="item1"><a href="#">{{ __('messages.faq', [], session()->get('locale')) }}</a></li>
                        <li class="item2"><a href="#">{{ __('messages.privacy_policy', [], session()->get('locale')) }}</a></li>
                        <li class="item3"><a href="#">{{ __('messages.recent_news', [], session()->get('locale')) }}</a></li>
                        <li class="item4"><a href="#">{{ __('messages.help_desk', [], session()->get('locale')) }}</a></li>
                    </ul>
                </nav>
            </div> --}}
        </div>
        <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 col-6">
            <div class="footer-right-block footer-block">
                <div class="text-label">{{ __('messages.technical_assistance', [], session()->get('locale')) }}</div>
                <div class="partner-logo-right">
                    <a href="https://www.orangebd.com" target="_blank">
                        <img src="{{ asset('img/partner-logo.png') }}" alt="Partner Logo">
                    </a>
                </div>
            </div>
        </div>
    </div>
</footer>
