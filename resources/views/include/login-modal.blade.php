@if (!session()->has('citizen') || !session()->has('employee'))
    <div class="modal fade" id="loginModal" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="loginModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content border-0">
                <div class="modal-header">
                    <h6 class="modal-title" id="viewModalLabel">লগইন</h6>
                    <button type="button" class="close border-0 rounded" data-bs-dismiss="modal" aria-label="Close">
                        <i class="fa fa-times-circle"></i>
                    </button>
                </div>
                <div class="modal-body text-center p-4">
                    <a class="btn btn-login btn-beneficiary w-100 p-2" href="<?php echo $baseUrl ?>login">
                        <i class="fa fa-users"></i>&nbsp&nbspআবেদনকারী লগইন
                    </a>
                    <div class="p-2">অথবা</div>
                    <a class="btn btn-login btn-govt-employee w-100 p-2" href="<?php echo $baseUrl ?>doptorlogin">
                        <i class="fa fa-user"></i>&nbsp&nbspপ্রশাসনিক লগইন
                    </a>
                    {{-- <div class="p-2">অথবা</div>
                    <a class="btn btn-login btn-warning w-100 p-2" href="<?php echo getenv('ASSISTANT_DOMAIN'); ?>">
                        <i class="fa fa-users"></i>&nbsp&nbspসহায়ক লগইন
                    </a> --}}
                </div>
                <div class="modal-footer">
                </div>
            </div>
        </div>
    </div>
@endif
