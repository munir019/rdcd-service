<script>
    $(document).ready(function(){
        <?php if(isset($message['title'])) {?>
            Swal.fire({
                title: "<?php echo (isset($message['title'])?$message['title']:'') ?>",
                html: "<?php echo (isset($message['text'])?$message['text']:'') ?>",
                icon: "<?php echo (isset($message['icon'])?$message['icon']:'') ?>",
                button: "<?php echo (isset($message['button'])?$message['button']:'') ?>",
            });
        <?php } ?>
    });
</script>