<?php

/**
 * Created by PhpStorm.
 * User: Md. Munir Hossain
 * Date: 10/10/2021
 * Time: 12:45 PM
 */
namespace App\Helpers;

class AppHelper
{
    public function engToBngNum($num){
        $num =str_replace('0','০',$num);
        $num =str_replace('1','১',$num);
        $num =str_replace('2','২',$num);
        $num =str_replace('3','৩',$num);
        $num =str_replace('4','৪',$num);
        $num =str_replace('5','৫',$num);
        $num =str_replace('6','৬',$num);
        $num =str_replace('7','৭',$num);
        $num =str_replace('8','৮',$num);
        $num =str_replace('9','৯',$num);

        return $num;
    }

    public function bngDate($time){
        if(!is_numeric($time))
            return $this->engToBngNum(date('d-m-Y ইং H:i',strtotime($time)));
        else
            return $this->engToBngNum(date('d-m-Y ইং H:i',$time));
    }

    public function bngOnlyDate($time){
        if(!is_numeric($time))
            return $this->engToBngNum(date('d-m-Y ইং',strtotime($time)));
        else
            return $this->engToBngNum(date('d-m-Y ইং',$time));
    }
}