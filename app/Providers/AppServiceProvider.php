<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Orangebd\ServiceInfo;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any message services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any message services.
     *
     * @return void
     */
    public function boot()
    {
        $lang = config('app.locale');
        session(['locale' => $lang]);

        //Write Services in cache for searching
        $serviceInfo = new ServiceInfo();
        $serviceInfo->getRdcdServices();
    }
}
