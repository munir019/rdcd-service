<?php

namespace App\Orangebd;

use Illuminate\Support\Facades\Storage;

class GeoInfo
{
    public $division;
    public $district;
    public $upazila;
    public $union;
    public $citycorporation;
    public $municipality;
    public $thana;
    private $cachePath;

    public function __construct(){
        $this->cachePath = 'geo/';

        if(!Storage::disk('cache')->exists($this->cachePath . 'geo_division.json'))
            $this->cronGeo();

        if(Storage::disk('cache')->exists($this->cachePath . 'geo_division.json'))
            $this->division = json_decode(Storage::disk('cache')->get($this->cachePath . 'geo_division.json'),true);
        if(Storage::disk('cache')->exists($this->cachePath . 'geo_district.json'))
            $this->district = json_decode(Storage::disk('cache')->get($this->cachePath . 'geo_district.json'),true);
        if(Storage::disk('cache')->exists($this->cachePath . 'geo_upazila.json'))
            $this->upazila = json_decode(Storage::disk('cache')->get($this->cachePath . 'geo_upazila.json'),true);
        if(Storage::disk('cache')->exists($this->cachePath . 'geo_union.json'))
            $this->union = json_decode(Storage::disk('cache')->get($this->cachePath . 'geo_union.json'),true);
        if(Storage::disk('cache')->exists($this->cachePath . 'geo_citycorporation.json'))
            $this->citycorporation = json_decode(Storage::disk('cache')->get($this->cachePath . 'geo_citycorporation.json'),true);
        if(Storage::disk('cache')->exists($this->cachePath . 'geo_municipality.json'))
            $this->municipality = json_decode(Storage::disk('cache')->get($this->cachePath . 'geo_municipality.json'),true);
        if(Storage::disk('cache')->exists($this->cachePath . 'geo_thana.json'))
            $this->thana = json_decode(Storage::disk('cache')->get($this->cachePath . 'geo_thana.json'),true);
    }
    
    public function cronGeo(){
        $nDoptor = new nDoptor();

        $division = $nDoptor->data('division?xglimit=10000');
        $division = json_decode($division, true);
        if(!empty($division)) {
            $tmp = array();
            foreach ($division as $value) {
                $tmp[$value['id']] = $value;
            }
            $division = $tmp;

            Storage::disk('cache')->put($this->cachePath . 'geo_division.json', json_encode($division, JSON_UNESCAPED_UNICODE));
        }

        $district = $nDoptor->data('district?xglimit=10000');
        $district = json_decode($district, true);
        if(!empty($district)) {
            $tmp = array();
            foreach ($district as $value) {
                $tmp[$value['id']] = $value;
            }
            $district = $tmp;

            Storage::disk('cache')->put($this->cachePath . 'geo_district.json', json_encode($district, JSON_UNESCAPED_UNICODE));
        }

        $upazila = $nDoptor->data('upazilla?xglimit=10000');
        $upazila = json_decode($upazila, true);
        if(!empty($upazila)) {
            $tmp = array();
            foreach ($upazila as $value) {
                $tmp[$value['id']] = $value;
            }
            $upazila = $tmp;

            Storage::disk('cache')->put($this->cachePath . 'geo_upazila.json', json_encode($upazila, JSON_UNESCAPED_UNICODE));
        }

        $union = $nDoptor->data('union?xglimit=10000');
        $union = json_decode($union, true);
        if(!empty($union)) {
            $tmp = array();
            foreach ($union as $value) {
                $tmp[$value['id']] = $value;
            }
            $union = $tmp;

            Storage::disk('cache')->put($this->cachePath . 'geo_union.json', json_encode($union, JSON_UNESCAPED_UNICODE));
        }

        $citycorporation = $nDoptor->data('citycorporation?xglimit=10000');
        $citycorporation = json_decode($citycorporation, true);
        if(!empty($citycorporation)) {
            $tmp = array();
            foreach ($citycorporation as $value) {
                $tmp[$value['id']] = $value;
            }
            $citycorporation = $tmp;

            Storage::disk('cache')->put($this->cachePath . 'geo_citycorporation.json', json_encode($citycorporation, JSON_UNESCAPED_UNICODE));
        }

        $municipality = $nDoptor->data('municipality?xglimit=10000');
        $municipality = json_decode($municipality, true);

        if(!empty($municipality)) {
            $tmp = array();
            foreach ($municipality as $value) {
                $tmp[$value['id']] = $value;
            }
            $municipality = $tmp;

            Storage::disk('cache')->put($this->cachePath . 'geo_municipality.json', json_encode($municipality, JSON_UNESCAPED_UNICODE));
        }

        $thana = $nDoptor->data('thana?xglimit=10000');
        $thana = json_decode($thana, true);
        if(!empty($thana)) {
            $tmp = array();
            foreach ($thana as $value) {
                $tmp[$value['id']] = $value;
            }
            $thana = $tmp;

            Storage::disk('cache')->put($this->cachePath . 'geo_thana.json', json_encode($thana, JSON_UNESCAPED_UNICODE));
        }
    }
}