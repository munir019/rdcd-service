<?php
namespace App\Orangebd;

use App\Models\Service;
use App\Models\ServiceOffice;
use App\Models\ServiceRecipient;
use App\Models\ServiceSector;
use Illuminate\Support\Facades\Storage;

class ServiceInfo
{
    private $cachePath;
    private $cacheOfficePath;

    public function __construct()
    {
        $this->cachePath = 'service/';
        $this->cacheOfficePath = 'office/';
    }

    public function getServiceCategoryInfo(){

        if(!Storage::disk('cache')->exists($this->cachePath.'sector.json') ||
            !Storage::disk('cache')->exists($this->cachePath.'recipient.json') ||
            !Storage::disk('cache')->exists($this->cachePath.'officeLayer.json')) {

            $this->cronServiceList();
        }

        $sector = Storage::disk('cache')->get($this->cachePath.'sector.json');
        $sector = json_decode($sector, true);

        $recipient = Storage::disk('cache')->get($this->cachePath.'recipient.json');
        $recipient = json_decode($recipient, true);

        $officeLayer = Storage::disk('cache')->get($this->cachePath.'officeLayer.json');
        $officeLayer = json_decode($officeLayer, true);

        $result = array();
        $result['sector'] = $sector;
        $result['recipient'] = $recipient;
        $result['officeLayer'] = $officeLayer;

        return $result;
    }

    public function getServices($category, $data){

        if($category=='sector')
            $file = 'service_by_sector_'.str_replace('service-sector-','',$data).'.json';
        else if($category=='recipient')
            $file = 'service_by_recipient_'.str_replace('service-recipient-','',$data).'.json';
        else if($category=='ministry')
            $file = 'service_by_ministry_'.$data.'.json';
        else if($category=='origin')
            $file = 'service_by_origin_'.$data.'.json';

        if(!Storage::disk('cache')->exists($this->cachePath.$file)){
            $this->cronServiceList();
        }

        $service = array();
        if(Storage::disk('cache')->exists($this->cachePath.$file)) {
            $service = Storage::disk('cache')->get($this->cachePath . $file);
            $service = json_decode($service, true);
        }

        return $service;
    }

    public function cronServiceList(){

        $officeLayer = array(1 => array("name" => "মন্ত্রণালয়/বিভাগ", "name_en" => "Ministry / Division"),
            2 => array("name" => "অধিদপ্তর/পরিদপ্তর", "name_en" => "Directorate Other"),
            3 => array("name" => "দপ্তর/সংস্থা অন্যান্য", "name_en" => "Office / agency"),
            4 => array("name" => "বিভাগীয় কার্যালয়", "name_en" => "Departmental office"),
            5 => array("name" => "জেলা কার্যালয়", "name_en" => "District Office"),
            6 => array("name" => "উপজেলা কার্যালয়", "name_en" => "Upazila Office"),
            7 => array("name" => "আঞ্চলিক কার্যালয়", "name_en" => "Regional Office"));

        $service = Service::select(['recipient', 'sector'])->where('status', 1)->where('onlive', 1)->get()->toArray();
        $recipientId = array();
        $sectorId = array();
        foreach ($service as $v) {
            $recipientId[$v['recipient']] = $v['recipient'];
            $sectorId[$v['sector']] = $v['sector'];
        }

        $sector = array();
        $tmp = ServiceSector::select(['id', 'name', 'name_en', 'icon'])->whereIn('id', $sectorId)->orderBy('hierarchy')->get()->toArray();
        foreach ($tmp as $v) {
            $sector[$v['id']] = $v;
        }

        $recipient = array();
        $tmp = ServiceRecipient::select(['id', 'name', 'name_en', 'icon'])->whereIn('id', $recipientId)->orderBy('hierarchy')->get()->toArray();
        foreach ($tmp as $v) {
            $recipient[$v['id']] = $v;
        }

        Storage::disk('cache')->put($this->cachePath.'sector.json', json_encode($sector, JSON_UNESCAPED_UNICODE));
        Storage::disk('cache')->put($this->cachePath.'recipient.json', json_encode($recipient, JSON_UNESCAPED_UNICODE));
        Storage::disk('cache')->put($this->cachePath.'officeLayer.json', json_encode($officeLayer, JSON_UNESCAPED_UNICODE));

        $nDoptor = new nDoptor();

        $serviceCategory = array();
        /*All Service*/

        $data = Service::select(['id','sid','name','name_en', 'keyword','recipient','sector','application_recipient','payment_gateway','office','icon'])->where('status',1)->where('onlive',1)->where('office','>',0)->get()->toArray();

        Storage::disk('cache')->put($this->cachePath.'service_all.json', json_encode($data, JSON_UNESCAPED_UNICODE));
        /*All Service*/

        /*Search Service*/
        $dataSearch = array();
        foreach($data as $v){
            $dataSearch[] = array('name'=>$v['name'],'name_en'=>$v['name_en'],'keyword'=>$v['keyword']);
        }

        Storage::disk('cache')->put($this->cachePath.'search_service.json', json_encode($dataSearch, JSON_UNESCAPED_UNICODE));
        /*Search Service*/

        /*Service by Recipient*/
        $dataKeys = array();
        foreach($data as $v){
            $dataKeys[$v['recipient']][] = $v;
        }

        foreach ($dataKeys as $k=>$v)
            Storage::disk('cache')->put($this->cachePath.'service_by_recipient_'.$k.'.json', json_encode($v, JSON_UNESCAPED_UNICODE));
        /*Service by Recipient*/

        /*Service by Sector*/
        $dataKeys = array();
        foreach($data as $v){
            $dataKeys[$v['sector']][] = $v;
        }

        foreach ($dataKeys as $k=>$v)
            Storage::disk('cache')->put($this->cachePath.'service_by_sector_'.$k.'.json', json_encode($v, JSON_UNESCAPED_UNICODE));
        /*Service by Sector*/

        /*Service by Ministry*/
        $ministry = Storage::disk('cache')->get($this->cacheOfficePath.'ministry.json');
        $ministry = json_decode($ministry, true);

        $ministryWithService = array();
        $tmp = array();
        foreach($ministry as $val){
            $ministryId = $val['id'];
            $tmp = ServiceOffice::select('sid')->where('geo_ministry', $ministryId)->where('status', 1)->get()->toArray();
            $serviceId = array();
            foreach($tmp as $v)
                $serviceId[$v['sid']] = $v['sid'];

            $data = Service::select(['id','sid','name','name_en', 'keyword','recipient','sector','application_recipient','payment_gateway','office','icon'])->where('status',1)->where('onlive',1)->where('office','>',0)->whereIn('id',$serviceId)->get()->toArray();

            if(!empty($data)) {
                $ministryWithService[] = $val;
                Storage::disk('cache')->put($this->cachePath . 'service_by_ministry_' . $ministryId . '.json', json_encode($data, JSON_UNESCAPED_UNICODE));
            }
        }

        Storage::disk('cache')->put($this->cachePath . 'service_by_ministry.json', json_encode($ministryWithService, JSON_UNESCAPED_UNICODE));
        /*Service by Ministry*/

        /*Office by Origin*/
        $origin = Storage::disk('cache')->get($this->cacheOfficePath.'office_origin.json');
        $origin = json_decode($origin,true);

        $originWithService = array();
        foreach($origin as $val) {
            $originId = $val['id'];

            $tmp = ServiceOffice::select('sid')->where('office_origin',$originId)->where('status',1)->get()->toArray();
            $serviceId = array();
            foreach($tmp as $v)
                $serviceId[$v['sid']] = $v['sid'];

            $data = Service::select(['id','sid','name','name_en', 'keyword','recipient','sector','application_recipient','payment_gateway','office','icon'])->where('status',1)->where('onlive',1)->where('office','>',0)->whereIn('id',$serviceId)->get()->toArray();

            if(!empty($data)) {
                $originWithService[] = $val;
                Storage::disk('cache')->put($this->cachePath . 'service_by_origin_' . $originId . '.json', json_encode($data, JSON_UNESCAPED_UNICODE));
            }
        }

        Storage::disk('cache')->put($this->cachePath . 'service_by_origin.json', json_encode($originWithService, JSON_UNESCAPED_UNICODE));
    }


    public function getRdcdServices(){
        $data = $this->getRdcdRapidServiceList();
        $dataSearch = $this->getRapidUniqueServices($data);

        /**Rapid Service*/
        //if(!Storage::disk('cache')->exists($this->cachePath.'rapid_services.json')) {
            Storage::disk('cache')->put($this->cachePath.'rapid_services.json', json_encode($dataSearch, JSON_UNESCAPED_UNICODE));
        //}
    }

    public function getRapidUniqueServices($data){
        $dataSearch = []; $dataService = [];
        if(!empty($data)){
            foreach($data as $key=>$val){
                if($val->services){
                    foreach($val->services as $service){
                        $service = (array) $service;
                        if(!in_array($service['name'], $dataService)){
                            $dataService[] = $service['name'];
                            $dataSearch[] = array('name'=>$service['name'],'name_en'=>$service['name_en'],'keyword'=>$service['keyword'], 'sid'=>$service['sid'], 'type_name'=>$service['type_name'], 'sector_name'=>$service['sector_name']);
                        }
                    }
                }
            }
        }
        return $dataSearch;
    }

    public function getRdcdRapidServiceList(){

        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => 'https://admin.mygov.bd/rdcd.php',
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'GET',
        ));
        $response = curl_exec($curl);
        curl_close($curl);

        $response = json_decode($response);

		if(count($response) > 9){
			// $swap_service = array_shift($response[0]->services);
			// array_unshift($response[count($response)-1]->services, $swap_service);

            if(isset($response[9]->services) && $response[9]->office_layer == 3){
                $response[9]->services = $response[0]->services;
            }

            if(isset($response[10]->services) && $response[10]->office_layer == 3){
                $response[10]->services = $response[0]->services;
            }
		}
		return $response;

        $serviceList = [
            [
                'id' => 1,
                'office_layer' => 1,
                'doptorname' => 'পল্লী উন্নয়ন ও সমবায় বিভাগ',
                'logo' => 'ministry_logo.png',
                'services' => [
                    [
                        'id' => 'BDGS-1638251650',
                        'form_id' => '1882',
                        'service_type' => 'g2c',
                        'name' => 'তথ্য অধিকার আইনে তথ্য প্রাপ্তির আবেদনপত্র'
                    ],
                    [
                        'id' => 'BDGS-1605509762',
                        'form_id' => '770',
                        'service_type' => 'g2c',
                        'name' => 'তথ্য অধিকার আইনে আপিল আবেদন'
                    ],
                    [
                        'id' => 'BDGS-1628673117',
                        'form_id' => '1509',
                        'service_type' => 'g2g',
                        'name' => 'অর্জিত ছুটির (অভ্যন্তরীণ) মঞ্জুরের আবেদন'
                    ],
                    [
                        'id' => 'BDGS-1630901329',
                        'form_id' => '1519',
                        'service_type' => 'g2g',
                        'name' => 'অর্জিত ছুটি (বহিঃ বাংলাদেশ) মঞ্জুরের আবেদন'
                    ],
                    [
                        'id' => 'BDGS-1639030213',
                        'form_id' => '',
                        'service_type' => 'g2g',
                        'name' => 'অসাধারণ ছুটি'
                    ],
                    [
                        'id' => 'BDGS-1639030724',
                        'form_id' => '',
                        'service_type' => 'g2g',
                        'name' => 'অধ্যয়ন ছুটি'
                    ],
                    [
                        'id' => 'BDGS-1639031238',
                        'form_id' => '',
                        'service_type' => 'g2g',
                        'name' => 'সংগনিরোধ ছুটি'
                    ],
                    [
                        'id' => 'BDGS-1605511065',
                        'form_id' => '777',
                        'service_type' => 'g2g',
                        'name' => 'প্রসূতি ছুটির জন্য আবেদন'
                    ],
                    [
                        'id' => 'BDGS-1639031339',
                        'form_id' => '',
                        'service_type' => 'g2g',
                        'name' => 'প্রাপ্যতাবিহীন ছুটি'
                    ],
                    [
                        'id' => 'BDGS-1639031426',
                        'form_id' => '',
                        'service_type' => 'g2g',
                        'name' => 'অবসর-উত্তর ছুটি'
                    ],
                    [
                        'id' => 'BDGS-1531733932',
                        'form_id' => '11',
                        'service_type' => 'g2g',
                        'name' => 'নৈমিত্তিক ছুটির জন্য আবেদন'
                    ],
                    [
                        'id' => 'BDGS-1639031501',
                        'form_id' => '',
                        'service_type' => 'g2g',
                        'name' => 'অক্ষমতাজনিত বিশেষ ছুটি'
                    ],
                    [
                        'id' => 'BDGS-1638252679',
                        'form_id' => '1886',
                        'service_type' => 'g2g',
                        'name' => 'চিকিৎসা ছুটি মঞ্জুরের আবেদন'
                    ],
                    [
                        'id' => 'BDGS-1638257451',
                        'form_id' => '1889',
                        'service_type' => 'g2g',
                        'name' => 'চিকিৎসা ছুটি মঞ্জুর (বহিঃবাংলাদেশ)'
                    ],
                    [
                        'id' => 'BDGS-1639031640',
                        'form_id' => '',
                        'service_type' => 'g2g',
                        'name' => 'বিশেষ অসুস্থ্যতাজনিত ছুটি'
                    ],
                    [
                        'id' => 'BDGS-1545908174',
                        'form_id' => '188',
                        'service_type' => 'g2g',
                        'name' => 'শ্রান্তি-বিনোদন ভাতা ও ছুটি মঞ্জুরীর আবেদন'
                    ],
                    [
                        'id' => 'BDGS-1630918419',
                        'form_id' => '1556',
                        'service_type' => 'g2g',
                        'name' => 'সাধারণ ভবিষ্যৎ তহবিল হতে অগ্রিম মঞ্জুরি'
                    ],
                    [
                        'id' => 'BDGS-1638260689',
                        'form_id' => '1890',
                        'service_type' => 'g2g',
                        'name' => 'চাকরি হতে অব্যাহতির জন্য আবেদন'
                    ],
                    [
                        'id' => 'BDGS-1583900979',
                        'form_id' => '407',
                        'service_type' => 'g2g',
                        'name' => 'চাকুরী নিয়মিতকরনের জন্য আবেদন'
                    ],
                    [
                        'id' => 'BDGS-1630906012',
                        'form_id' => '1536',
                        'service_type' => 'g2g',
                        'name' => 'চাকুরী স্থায়ীকরণের আবেদন'
                    ],
                    [
                        'id' => 'BDGS-1583818332',
                        'form_id' => '396',
                        'service_type' => 'g2g',
                        'name' => 'পাসপোর্টের অনাপত্তির জন্য আবেদন'
                    ],
                    [
                        'id' => 'BDGS-1566468976',
                        'form_id' => '229',
                        'service_type' => 'g2g',
                        'name' => 'গাড়ী রিকুইজিশনের জন্য আবেদন'
                    ],
                    [
                        'id' => 'BDGS-1638696827',
                        'form_id' => '',
                        'service_type' => 'g2g',
                        'name' => 'অভ্যন্তরীণ আইসিটি সাপোর্ট সিস্টেম'
                    ],
                ]
            ],
            [
                'id' => 2,
                'office_layer' => 2,
                'doptorname' => 'সমবায় অধিদপ্তর',
                'logo' => 'ministry_logo.png',
                'services' => [
                    [
                        'id' => '#',
                        'form_id' => '',
                        'service_type' => 'g2c',
                        'name' => 'প্রাথমিক সমবায় সমিতি নিবন্ধন'
                    ],
                    [
                        'id' => '#',
                        'form_id' => '',
                        'service_type' => 'g2c',
                        'name' => 'নির্বাচন কমিটি নিয়োগের মাধ্যমে ব্যবস্থাপনা কমিটি গঠন'
                    ],
                    [
                        'id' => '#',
                        'form_id' => '',
                        'service_type' => 'g2c',
                        'name' => 'অন্তর্বর্তী ব্যবস্থাপনা কমিটি গঠন'
                    ],
                    [
                        'id' => 'BDGS-1638251650',
                        'form_id' => '1882',
                        'service_type' => 'g2c',
                        'name' => 'তথ্য অধিকার আইনে তথ্য প্রাপ্তির আবেদনপত্র'
                    ],
                    [
                        'id' => 'BDGS-1605509762',
                        'form_id' => '770',
                        'service_type' => 'g2c',
                        'name' => 'তথ্য অধিকার আইনে আপিল আবেদন'
                    ],
                    [
                        'id' => 'BDGS-1628673117',
                        'form_id' => '1509',
                        'service_type' => 'g2g',
                        'name' => 'অর্জিত ছুটির (অভ্যন্তরীণ) মঞ্জুরের আবেদন'
                    ],
                    [
                        'id' => 'BDGS-1630901329',
                        'form_id' => '1519',
                        'service_type' => 'g2g',
                        'name' => 'অর্জিত ছুটি (বহিঃ বাংলাদেশ) মঞ্জুরের আবেদন'
                    ],
                    [
                        'id' => 'BDGS-1639030213',
                        'form_id' => '',
                        'service_type' => 'g2g',
                        'name' => 'অসাধারণ ছুটি'
                    ],
                    [
                        'id' => 'BDGS-1639030724',
                        'form_id' => '',
                        'service_type' => 'g2g',
                        'name' => 'অধ্যয়ন ছুটি'
                    ],
                    [
                        'id' => 'BDGS-1639031238',
                        'form_id' => '',
                        'service_type' => 'g2g',
                        'name' => 'সংগনিরোধ ছুটি'
                    ],
                    [
                        'id' => 'BDGS-1605511065',
                        'form_id' => '777',
                        'service_type' => 'g2g',
                        'name' => 'প্রসূতি ছুটির জন্য আবেদন'
                    ],
                    [
                        'id' => 'BDGS-1639031339',
                        'form_id' => '',
                        'service_type' => 'g2g',
                        'name' => 'প্রাপ্যতাবিহীন ছুটি'
                    ],
                    [
                        'id' => 'BDGS-1639031426',
                        'form_id' => '',
                        'service_type' => 'g2g',
                        'name' => 'অবসর-উত্তর ছুটি'
                    ],
                    [
                        'id' => 'BDGS-1531733932',
                        'form_id' => '11',
                        'service_type' => 'g2g',
                        'name' => 'নৈমিত্তিক ছুটির জন্য আবেদন'
                    ],
                    [
                        'id' => 'BDGS-1639031501',
                        'form_id' => '',
                        'service_type' => 'g2g',
                        'name' => 'অক্ষমতাজনিত বিশেষ ছুটি'
                    ],
                    [
                        'id' => 'BDGS-1638252679',
                        'form_id' => '1886',
                        'service_type' => 'g2g',
                        'name' => 'চিকিৎসা ছুটি মঞ্জুরের আবেদন'
                    ],
                    [
                        'id' => 'BDGS-1638257451',
                        'form_id' => '1889',
                        'service_type' => 'g2g',
                        'name' => 'চিকিৎসা ছুটি মঞ্জুর (বহিঃবাংলাদেশ)'
                    ],
                    [
                        'id' => 'BDGS-1639031640',
                        'form_id' => '',
                        'service_type' => 'g2g',
                        'name' => 'বিশেষ অসুস্থ্যতাজনিত ছুটি'
                    ],
                    [
                        'id' => 'BDGS-1545908174',
                        'form_id' => '188',
                        'service_type' => 'g2g',
                        'name' => 'শ্রান্তি-বিনোদন ছুটি ও ভাতা মঞ্জুরীর আবেদন'
                    ],
                    [
                        'id' => 'BDGS-1630918419',
                        'form_id' => '1556',
                        'service_type' => 'g2g',
                        'name' => 'সাধারণ ভবিষ্যৎ তহবিল হতে অগ্রিম মঞ্জুরি'
                    ],
                    [
                        'id' => 'BDGS-1638260689',
                        'form_id' => '1890',
                        'service_type' => 'g2g',
                        'name' => 'চাকরি হতে অব্যাহতির জন্য আবেদন'
                    ],
                    [
                        'id' => 'BDGS-1583900979',
                        'form_id' => '407',
                        'service_type' => 'g2g',
                        'name' => 'চাকুরী নিয়মিতকরনের জন্য আবেদন'
                    ],
                    [
                        'id' => 'BDGS-1630906012',
                        'form_id' => '1536',
                        'service_type' => 'g2g',
                        'name' => 'চাকুরী স্থায়ীকরণের আবেদন'
                    ],
                    [
                        'id' => 'BDGS-1583818332',
                        'form_id' => '396',
                        'service_type' => 'g2g',
                        'name' => 'পাসপোর্টের অনাপত্তির জন্য আবেদন'
                    ],
                    [
                        'id' => 'BDGS-1566468976',
                        'form_id' => '229',
                        'service_type' => 'g2g',
                        'name' => 'গাড়ী রিকুইজিশনের জন্য আবেদন'
                    ]
                ]
            ],
            [
                'id' =>3,
                'office_layer' => 3,
                'doptorname' => 'বাংলাদেশ পল্লী উন্নয়ন বোর্ড (বিআরডিবি)',
                'logo' => 'brdb.png',
                'services' => [
                    [
                        'id' => 'BDGS-1638251514',
                        'form_id' => '1881',
                        'service_type' => 'g2g',
                        'name' => 'ভ্রমণ সূচি অনুমোদন'
                    ],
                    [
                        'id' => 'BDGS-1638251786',
                        'form_id' => '1883',
                        'service_type' => 'g2g',
                        'name' => 'ভ্রমণ বিল অনুমোদন'
                    ],
                    [
                        'id' => 'BDGS-1638252044',
                        'form_id' => '1885',
                        'service_type' => 'g2c',
                        'name' => 'সুফলভোগীদের প্রশিক্ষণের আবেদন '
                    ],
                    [
                        'id' => 'BDGS-1638251841',
                        'form_id' => '1884',
                        'service_type' => 'g2c',
                        'name' => 'সদস্য ভর্তি আবেদন'
                    ],
                    [
                        'id' => 'BDGS-1638251650',
                        'form_id' => '1882',
                        'service_type' => 'g2c',
                        'name' => 'তথ্য অধিকার আইনে তথ্য প্রাপ্তির আবেদনপত্র'
                    ],
                    [
                        'id' => 'BDGS-1605509762',
                        'form_id' => '770',
                        'service_type' => 'g2c',
                        'name' => 'তথ্য অধিকার আইনে আপিল আবেদন'
                    ],
                    [
                        'id' => 'BDGS-1628673117',
                        'form_id' => '1509',
                        'service_type' => 'g2g',
                        'name' => 'অর্জিত ছুটির (অভ্যন্তরীণ) মঞ্জুরের আবেদন'
                    ],
                    [
                        'id' => 'BDGS-1630901329',
                        'form_id' => '1519',
                        'service_type' => 'g2g',
                        'name' => 'অর্জিত ছুটি (বহিঃ বাংলাদেশ) মঞ্জুরের আবেদন'
                    ],
                    [
                        'id' => 'BDGS-1639030213',
                        'form_id' => '',
                        'service_type' => 'g2g',
                        'name' => 'অসাধারণ ছুটি'
                    ],
                    [
                        'id' => 'BDGS-1639030724',
                        'form_id' => '',
                        'service_type' => 'g2g',
                        'name' => 'অধ্যয়ন ছুটি'
                    ],
                    [
                        'id' => 'BDGS-1639031238',
                        'form_id' => '',
                        'service_type' => 'g2g',
                        'name' => 'সংগনিরোধ ছুটি'
                    ],
                    [
                        'id' => 'BDGS-1605511065',
                        'form_id' => '777',
                        'service_type' => 'g2g',
                        'name' => 'প্রসূতি ছুটির জন্য আবেদন'
                    ],
                    [
                        'id' => 'BDGS-1639031339',
                        'form_id' => '',
                        'service_type' => 'g2g',
                        'name' => 'প্রাপ্যতাবিহীন ছুটি'
                    ],
                    [
                        'id' => 'BDGS-1639031426',
                        'form_id' => '',
                        'service_type' => 'g2g',
                        'name' => 'অবসর-উত্তর ছুটি'
                    ],
                    [
                        'id' => 'BDGS-1531733932',
                        'form_id' => '11',
                        'service_type' => 'g2g',
                        'name' => 'নৈমিত্তিক ছুটির জন্য আবেদন'
                    ],
                    [
                        'id' => 'BDGS-1639031501',
                        'form_id' => '',
                        'service_type' => 'g2g',
                        'name' => 'অক্ষমতাজনিত বিশেষ ছুটি'
                    ],
                    [
                        'id' => 'BDGS-1638252679',
                        'form_id' => '1886',
                        'service_type' => 'g2g',
                        'name' => 'চিকিৎসা ছুটি মঞ্জুরের আবেদন'
                    ],
                    [
                        'id' => 'BDGS-1638257451',
                        'form_id' => '1889',
                        'service_type' => 'g2g',
                        'name' => 'চিকিৎসা ছুটি মঞ্জুর (বহিঃবাংলাদেশ)'
                    ],
                    [
                        'id' => 'BDGS-1639031640',
                        'form_id' => '',
                        'service_type' => 'g2g',
                        'name' => 'বিশেষ অসুস্থ্যতাজনিত ছুটি'
                    ],
                    [
                        'id' => 'BDGS-1545908174',
                        'form_id' => '188',
                        'service_type' => 'g2g',
                        'name' => 'শ্রান্তি-বিনোদন ছুটি ও ভাতা মঞ্জুরীর আবেদন'
                    ],
                    [
                        'id' => 'BDGS-1630918419',
                        'form_id' => '1556',
                        'service_type' => 'g2g',
                        'name' => 'সাধারণ ভবিষ্যৎ তহবিল হতে অগ্রিম মঞ্জুরি'
                    ],
                    [
                        'id' => 'BDGS-1638260689',
                        'form_id' => '1890',
                        'service_type' => 'g2g',
                        'name' => 'চাকরি হতে অব্যাহতির জন্য আবেদন'
                    ],
                    [
                        'id' => 'BDGS-1583900979',
                        'form_id' => '407',
                        'service_type' => 'g2g',
                        'name' => 'চাকুরী নিয়মিতকরনের জন্য আবেদন'
                    ],
                    [
                        'id' => 'BDGS-1630906012',
                        'form_id' => '1536',
                        'service_type' => 'g2g',
                        'name' => 'চাকুরী স্থায়ীকরণের আবেদন'
                    ],
                    [
                        'id' => 'BDGS-1583818332',
                        'form_id' => '396',
                        'service_type' => 'g2g',
                        'name' => 'পাসপোর্টের অনাপত্তির জন্য আবেদন'
                    ],
                    [
                        'id' => 'BDGS-1566468976',
                        'form_id' => '229',
                        'service_type' => 'g2g',
                        'name' => 'গাড়ী রিকুইজিশনের জন্য আবেদন'
                    ]
                ]
            ],
            [
                'id' => 4,
                'office_layer' => 3,
                'doptorname' => 'বাংলাদেশ পল্লী উন্নয়ন একাডেমী (বার্ড), কুমিল্লা',
                'logo' => 'bard.png',
                'services' => [
                    [
                        'id' => 'BDGS-1638247800',
                        'form_id' => '1869',
                        'service_type' => 'g2c',
                        'name' => 'দর্শনার্থী পরিদর্শন ও সেবা প্রাপ্তির আবেদন'
                    ],
                    [
                        'id' => 'BDGS-1638248236',
                        'form_id' => '1871',
                        'service_type' => 'g2c',
                        'name' => 'ফিজিকাল ফ্যাসিলিটি সার্ভিস (ক্যাফেটেরিয়া ও হোস্টেল) ব্যবহারের জন্য আবেদন'
                    ],
                    [
                        'id' => 'BDGS-1638251650',
                        'form_id' => '1882',
                        'service_type' => 'g2c',
                        'name' => 'তথ্য অধিকার আইনে তথ্য প্রাপ্তির আবেদনপত্র'
                    ],
                    [
                        'id' => 'BDGS-1605509762',
                        'form_id' => '770',
                        'service_type' => 'g2c',
                        'name' => 'তথ্য অধিকার আইনে আপিল আবেদন'
                    ],
                    [
                        'id' => 'BDGS-1628673117',
                        'form_id' => '1509',
                        'service_type' => 'g2g',
                        'name' => 'অর্জিত ছুটির (অভ্যন্তরীণ) মঞ্জুরের আবেদন'
                    ],
                    [
                        'id' => 'BDGS-1630901329',
                        'form_id' => '1519',
                        'service_type' => 'g2g',
                        'name' => 'অর্জিত ছুটি (বহিঃ বাংলাদেশ) মঞ্জুরের আবেদন'
                    ],
                    [
                        'id' => 'BDGS-1639030213',
                        'form_id' => '',
                        'service_type' => 'g2g',
                        'name' => 'অসাধারণ ছুটি'
                    ],
                    [
                        'id' => 'BDGS-1639030724',
                        'form_id' => '',
                        'service_type' => 'g2g',
                        'name' => 'অধ্যয়ন ছুটি'
                    ],
                    [
                        'id' => 'BDGS-1639031238',
                        'form_id' => '',
                        'service_type' => 'g2g',
                        'name' => 'সংগনিরোধ ছুটি'
                    ],
                    [
                        'id' => 'BDGS-1605511065',
                        'form_id' => '777',
                        'service_type' => 'g2g',
                        'name' => 'প্রসূতি ছুটির জন্য আবেদন'
                    ],
                    [
                        'id' => 'BDGS-1639031339',
                        'form_id' => '',
                        'service_type' => 'g2g',
                        'name' => 'প্রাপ্যতাবিহীন ছুটি'
                    ],
                    [
                        'id' => 'BDGS-1639031426',
                        'form_id' => '',
                        'service_type' => 'g2g',
                        'name' => 'অবসর-উত্তর ছুটি'
                    ],
                    [
                        'id' => 'BDGS-1531733932',
                        'form_id' => '11',
                        'service_type' => 'g2g',
                        'name' => 'নৈমিত্তিক ছুটির জন্য আবেদন'
                    ],
                    [
                        'id' => 'BDGS-1639031501',
                        'form_id' => '',
                        'service_type' => 'g2g',
                        'name' => 'অক্ষমতাজনিত বিশেষ ছুটি'
                    ],
                    [
                        'id' => 'BDGS-1638252679',
                        'form_id' => '1886',
                        'service_type' => 'g2g',
                        'name' => 'চিকিৎসা ছুটি মঞ্জুরের আবেদন'
                    ],
                    [
                        'id' => 'BDGS-1638257451',
                        'form_id' => '1889',
                        'service_type' => 'g2g',
                        'name' => 'চিকিৎসা ছুটি মঞ্জুর (বহিঃবাংলাদেশ)'
                    ],
                    [
                        'id' => 'BDGS-1639031640',
                        'form_id' => '',
                        'service_type' => 'g2g',
                        'name' => 'বিশেষ অসুস্থ্যতাজনিত ছুটি'
                    ],
                    [
                        'id' => 'BDGS-1545908174',
                        'form_id' => '188',
                        'service_type' => 'g2g',
                        'name' => 'শ্রান্তি-বিনোদন ছুটি ও ভাতা মঞ্জুরীর আবেদন'
                    ],
                    [
                        'id' => 'BDGS-1630918419',
                        'form_id' => '1556',
                        'service_type' => 'g2g',
                        'name' => 'সাধারণ ভবিষ্যৎ তহবিল হতে অগ্রিম মঞ্জুরি'
                    ],
                    [
                        'id' => 'BDGS-1638260689',
                        'form_id' => '1890',
                        'service_type' => 'g2g',
                        'name' => 'চাকরি হতে অব্যাহতির জন্য আবেদন'
                    ],
                    [
                        'id' => 'BDGS-1583900979',
                        'form_id' => '407',
                        'service_type' => 'g2g',
                        'name' => 'চাকুরী নিয়মিতকরনের জন্য আবেদন'
                    ],
                    [
                        'id' => 'BDGS-1630906012',
                        'form_id' => '1536',
                        'service_type' => 'g2g',
                        'name' => 'চাকুরী স্থায়ীকরণের আবেদন'
                    ],
                    [
                        'id' => 'BDGS-1583818332',
                        'form_id' => '396',
                        'service_type' => 'g2g',
                        'name' => 'পাসপোর্টের অনাপত্তির জন্য আবেদন'
                    ],
                    [
                        'id' => 'BDGS-1566468976',
                        'form_id' => '229',
                        'service_type' => 'g2g',
                        'name' => 'গাড়ী রিকুইজিশনের জন্য আবেদন'
                    ]
                ]
            ],
            [
                'id' => 5,
                'office_layer' => 3,
                'doptorname' => 'পল্লী উন্নয়ন একাডেমী (আরডিএ), বগুড়া',
                'logo' => 'rda.png',
                'services' => [
                    [
                        'id' => 'BDGS-1638248866',
                        'form_id' => '1873',
                        'service_type' => 'g2c',
                        'name' => 'একাডেমীর প্রায়োগিক গবেষণায় উৎপাদিত খাদ্যপণ্য (দই, মিষ্টি ,আচার, মধু ইত্যাদি)'
                    ],
                    [
                        'id' => 'BDGS-1638249142',
                        'form_id' => '1874',
                        'service_type' => 'g2c',
                        'name' => 'মৎস্য হ্যাচারি থেকে উন্নত মানের মাছের রেনু, পোনা, বড় মাছ ও মা/ব্রুড মাছ সরবরাহ'
                    ],
                    [
                        'id' => 'BDGS-1638249371',
                        'form_id' => '1875',
                        'service_type' => 'g2c',
                        'name' => 'একাডেমীর নার্সারিতে উৎপাদিত গুণগত মানসম্পন্ন ফল-মূল, শাক-সবজির চারা সরবরাহ'
                    ],
                    [
                        'id' => 'BDGS-1638249723',
                        'form_id' => '1877',
                        'service_type' => 'g2c',
                        'name' => 'ফসল ইউনিটের উৎপাদিত কৃষি পণ্য (ফসল,শাক-সবজি,মাঠফলন) ক্রয়ের জন্য আবেদন'
                    ],
                    [
                        'id' => 'BDGS-1638250085',
                        'form_id' => '1878',
                        'service_type' => 'g2c',
                        'name' => 'একাডেমি থেকে উৎপাদিত নিরাপদ মুরগি (দেশি, সোনালী বয়লার), একদিন বয়সী বাচ্চা (দেশি মুরগি) এবং মুরগির ডিম সরবরাহ'
                    ],
                    [
                        'id' => 'BDGS-1638251226',
                        'form_id' => '1879',
                        'service_type' => 'g2c',
                        'name' => 'একাডেমীর প্রদর্শনী খামার হতে দুগ্ধ ও গবাদি প্রাণী ক্রয়ের জন্য আবেদন'
                    ],
                    [
                        'id' => 'BDGS-1638251467',
                        'form_id' => '1880',
                        'service_type' => 'g2c',
                        'name' => 'টিস্যু কালচার পদ্ধতিতে উৎপাদিত রোগমুক্ত বীজ (আলু) ও চারা(আলু, স্ট্রবেরি, স্টেভিয়া, কলা) ক্রয়ের জন্য আবেদন।'
                    ],
                    [
                        'id' => 'BDGS-1638246067',
                        'form_id' => '1866',
                        'service_type' => 'g2c',
                        'name' => 'একাডেমীর প্রদর্শিত খামার হতে পল্লী জৈব সার সরবরাহ'
                    ],
                    [
                        'id' => 'BDGS-1638251650',
                        'form_id' => '1882',
                        'service_type' => 'g2c',
                        'name' => 'তথ্য অধিকার আইনে তথ্য প্রাপ্তির আবেদনপত্র'
                    ],
                    [
                        'id' => 'BDGS-1605509762',
                        'form_id' => '770',
                        'service_type' => 'g2c',
                        'name' => 'তথ্য অধিকার আইনে আপিল আবেদন'
                    ],
                    [
                        'id' => 'BDGS-1628673117',
                        'form_id' => '1509',
                        'service_type' => 'g2g',
                        'name' => 'অর্জিত ছুটির (অভ্যন্তরীণ) মঞ্জুরের আবেদন'
                    ],
                    [
                        'id' => 'BDGS-1630901329',
                        'form_id' => '1519',
                        'service_type' => 'g2g',
                        'name' => 'অর্জিত ছুটি (বহিঃ বাংলাদেশ) মঞ্জুরের আবেদন'
                    ],
                    [
                        'id' => 'BDGS-1639030213',
                        'form_id' => '',
                        'service_type' => 'g2g',
                        'name' => 'অসাধারণ ছুটি'
                    ],
                    [
                        'id' => 'BDGS-1639030724',
                        'form_id' => '',
                        'service_type' => 'g2g',
                        'name' => 'অধ্যয়ন ছুটি'
                    ],
                    [
                        'id' => 'BDGS-1639031238',
                        'form_id' => '',
                        'service_type' => 'g2g',
                        'name' => 'সংগনিরোধ ছুটি'
                    ],
                    [
                        'id' => 'BDGS-1605511065',
                        'form_id' => '777',
                        'service_type' => 'g2g',
                        'name' => 'প্রসূতি ছুটির জন্য আবেদন'
                    ],
                    [
                        'id' => 'BDGS-1639031339',
                        'form_id' => '',
                        'service_type' => 'g2g',
                        'name' => 'প্রাপ্যতাবিহীন ছুটি'
                    ],
                    [
                        'id' => 'BDGS-1639031426',
                        'form_id' => '',
                        'service_type' => 'g2g',
                        'name' => 'অবসর-উত্তর ছুটি'
                    ],
                    [
                        'id' => 'BDGS-1531733932',
                        'form_id' => '11',
                        'service_type' => 'g2g',
                        'name' => 'নৈমিত্তিক ছুটির জন্য আবেদন'
                    ],
                    [
                        'id' => 'BDGS-1639031501',
                        'form_id' => '',
                        'service_type' => 'g2g',
                        'name' => 'অক্ষমতাজনিত বিশেষ ছুটি'
                    ],
                    [
                        'id' => 'BDGS-1638252679',
                        'form_id' => '1886',
                        'service_type' => 'g2g',
                        'name' => 'চিকিৎসা ছুটি মঞ্জুরের আবেদন'
                    ],
                    [
                        'id' => 'BDGS-1638257451',
                        'form_id' => '1889',
                        'service_type' => 'g2g',
                        'name' => 'চিকিৎসা ছুটি মঞ্জুর (বহিঃবাংলাদেশ)'
                    ],
                    [
                        'id' => 'BDGS-1639031640',
                        'form_id' => '',
                        'service_type' => 'g2g',
                        'name' => 'বিশেষ অসুস্থ্যতাজনিত ছুটি'
                    ],
                    [
                        'id' => 'BDGS-1545908174',
                        'form_id' => '188',
                        'service_type' => 'g2g',
                        'name' => 'শ্রান্তি-বিনোদন ছুটি ও ভাতা মঞ্জুরীর আবেদন'
                    ],
                    [
                        'id' => 'BDGS-1630918419',
                        'form_id' => '1556',
                        'service_type' => 'g2g',
                        'name' => 'সাধারণ ভবিষ্যৎ তহবিল হতে অগ্রিম মঞ্জুরি'
                    ],
                    [
                        'id' => 'BDGS-1638260689',
                        'form_id' => '1890',
                        'service_type' => 'g2g',
                        'name' => 'চাকরি হতে অব্যাহতির জন্য আবেদন'
                    ],
                    [
                        'id' => 'BDGS-1583900979',
                        'form_id' => '407',
                        'service_type' => 'g2g',
                        'name' => 'চাকুরী নিয়মিতকরনের জন্য আবেদন'
                    ],
                    [
                        'id' => 'BDGS-1630906012',
                        'form_id' => '1536',
                        'service_type' => 'g2g',
                        'name' => 'চাকুরী স্থায়ীকরণের আবেদন'
                    ],
                    [
                        'id' => 'BDGS-1583818332',
                        'form_id' => '396',
                        'service_type' => 'g2g',
                        'name' => 'পাসপোর্টের অনাপত্তির জন্য আবেদন'
                    ],
                    [
                        'id' => 'BDGS-1566468976',
                        'form_id' => '229',
                        'service_type' => 'g2g',
                        'name' => 'গাড়ী রিকুইজিশনের জন্য আবেদন'
                    ]
                ],
            ],
            [
                'id' => 6,
                'office_layer' => 3,
                'doptorname' => 'বঙ্গবন্ধু দারিদ্র্য বিমোচন ও পল্লী উন্নয়ন একাডেমি (বাপার্ড)',
                'logo' => 'bapard.png',
                'services' =>
                    [
                        [
                            'id' => 'BDGS-1638246631',
                            'form_id' => '1867',
                            'service_type' => 'g2c',
                            'name' => 'হোস্টেল সুবিধা প্রদান'
                        ],
                        [
                            'id' => 'BDGS-1638248203',
                            'form_id' => '1870',
                            'service_type' => 'g2c',
                            'name' => 'কৃষি বিষয়ক পরামর্শের জন্য আবেদন'
                        ],
                        [
                            'id' => 'BDGS-1638248790',
                            'form_id' => '1872',
                            'service_type' => 'g2c',
                            'name' => 'ইনকিউবেটরের মাধ্যমে ডিম হতে বাচ্চা উৎপাদন করার জন্য আবেদন'
                        ],
                        [
                            'id' => 'BDGS-1638251650',
                            'form_id' => '1882',
                            'service_type' => 'g2c',
                            'name' => 'তথ্য অধিকার আইনে তথ্য প্রাপ্তির আবেদনপত্র'
                        ],
                        [
                            'id' => 'BDGS-1605509762',
                            'form_id' => '770',
                            'service_type' => 'g2c',
                            'name' => 'তথ্য অধিকার আইনে আপিল আবেদন'
                        ],
                        [
                            'id' => 'BDGS-1628673117',
                            'form_id' => '1509',
                            'service_type' => 'g2g',
                            'name' => 'অর্জিত ছুটির (অভ্যন্তরীণ) মঞ্জুরের আবেদন'
                        ],
                        [
                            'id' => 'BDGS-1630901329',
                            'form_id' => '1519',
                            'service_type' => 'g2g',
                            'name' => 'অর্জিত ছুটি (বহিঃ বাংলাদেশ) মঞ্জুরের আবেদন'
                        ],
                        [
                            'id' => 'BDGS-1639030213',
                            'form_id' => '',
                            'service_type' => 'g2g',
                            'name' => 'অসাধারণ ছুটি'
                        ],
                        [
                            'id' => 'BDGS-1639030724',
                            'form_id' => '',
                            'service_type' => 'g2g',
                            'name' => 'অধ্যয়ন ছুটি'
                        ],
                        [
                            'id' => 'BDGS-1639031238',
                            'form_id' => '',
                            'service_type' => 'g2g',
                            'name' => 'সংগনিরোধ ছুটি'
                        ],
                        [
                            'id' => 'BDGS-1605511065',
                            'form_id' => '777',
                            'service_type' => 'g2g',
                            'name' => 'প্রসূতি ছুটির জন্য আবেদন'
                        ],
                        [
                            'id' => 'BDGS-1639031339',
                            'form_id' => '',
                            'service_type' => 'g2g',
                            'name' => 'প্রাপ্যতাবিহীন ছুটি'
                        ],
                        [
                            'id' => 'BDGS-1639031426',
                            'form_id' => '',
                            'service_type' => 'g2g',
                            'name' => 'অবসর-উত্তর ছুটি'
                        ],
                        [
                            'id' => 'BDGS-1531733932',
                            'form_id' => '11',
                            'service_type' => 'g2g',
                            'name' => 'নৈমিত্তিক ছুটির জন্য আবেদন'
                        ],
                        [
                            'id' => 'BDGS-1639031501',
                            'form_id' => '',
                            'service_type' => 'g2g',
                            'name' => 'অক্ষমতাজনিত বিশেষ ছুটি'
                        ],
                        [
                            'id' => 'BDGS-1638252679',
                            'form_id' => '1886',
                            'service_type' => 'g2g',
                            'name' => 'চিকিৎসা ছুটি মঞ্জুরের আবেদন'
                        ],
                        [
                            'id' => 'BDGS-1638257451',
                            'form_id' => '1889',
                            'service_type' => 'g2g',
                            'name' => 'চিকিৎসা ছুটি মঞ্জুর (বহিঃবাংলাদেশ)'
                        ],
                        [
                            'id' => 'BDGS-1639031640',
                            'form_id' => '',
                            'service_type' => 'g2g',
                            'name' => 'বিশেষ অসুস্থ্যতাজনিত ছুটি'
                        ],
                        [
                            'id' => 'BDGS-1545908174',
                            'form_id' => '188',
                            'service_type' => 'g2g',
                            'name' => 'শ্রান্তি-বিনোদন ছুটি ও ভাতা মঞ্জুরীর আবেদন'
                        ],
                        [
                            'id' => 'BDGS-1630918419',
                            'form_id' => '1556',
                            'service_type' => 'g2g',
                            'name' => 'সাধারণ ভবিষ্যৎ তহবিল হতে অগ্রিম মঞ্জুরি'
                        ],
                        [
                            'id' => 'BDGS-1638260689',
                            'form_id' => '1890',
                            'service_type' => 'g2g',
                            'name' => 'চাকরি হতে অব্যাহতির জন্য আবেদন'
                        ],
                        [
                            'id' => 'BDGS-1583900979',
                            'form_id' => '407',
                            'service_type' => 'g2g',
                            'name' => 'চাকুরী নিয়মিতকরনের জন্য আবেদন'
                        ],
                        [
                            'id' => 'BDGS-1630906012',
                            'form_id' => '1536',
                            'service_type' => 'g2g',
                            'name' => 'চাকুরী স্থায়ীকরণের আবেদন'
                        ],
                        [
                            'id' => 'BDGS-1583818332',
                            'form_id' => '396',
                            'service_type' => 'g2g',
                            'name' => 'পাসপোর্টের অনাপত্তির জন্য আবেদন'
                        ],
                        [
                            'id' => 'BDGS-1566468976',
                            'form_id' => '229',
                            'service_type' => 'g2g',
                            'name' => 'গাড়ী রিকুইজিশনের জন্য আবেদন'
                        ]
                    ]
            ],
            [
                'id' => 7,
                'office_layer' => 3,
                'doptorname' => 'বাংলাদেশ দুগ্ধ উৎপাদনকারী সমবায় ইউনিয়ন লিমিটেড (মিল্কভিটা)',
                'logo' => 'milkvita.png',
                'services' => [
                    [
                        'id' => 'BDGS-1638249648',
                        'form_id' => '1876',
                        'service_type' => 'g2c',
                        'name' => 'প্রস্তাবিত সমিতির প্রাথমিক দুগ্ধ উৎপাদনকারী সমিতি গঠনের আবেদন'
                    ],
                    [
                        'id' => 'BDGS-1638251650',
                        'form_id' => '1882',
                        'service_type' => 'g2c',
                        'name' => 'তথ্য অধিকার আইনে তথ্য প্রাপ্তির আবেদনপত্র'
                    ],
                    [
                        'id' => 'BDGS-1605509762',
                        'form_id' => '770',
                        'service_type' => 'g2c',
                        'name' => 'তথ্য অধিকার আইনে আপিল আবেদন'
                    ],
                    [
                        'id' => 'BDGS-1628673117',
                        'form_id' => '1509',
                        'service_type' => 'g2g',
                        'name' => 'অর্জিত ছুটির (অভ্যন্তরীণ) মঞ্জুরের আবেদন'
                    ],
                    [
                        'id' => 'BDGS-1630901329',
                        'form_id' => '1519',
                        'service_type' => 'g2g',
                        'name' => 'অর্জিত ছুটি (বহিঃ বাংলাদেশ) মঞ্জুরের আবেদন'
                    ],
                    [
                        'id' => 'BDGS-1639030213',
                        'form_id' => '',
                        'service_type' => 'g2g',
                        'name' => 'অসাধারণ ছুটি'
                    ],
                    [
                        'id' => 'BDGS-1639030724',
                        'form_id' => '',
                        'service_type' => 'g2g',
                        'name' => 'অধ্যয়ন ছুটি'
                    ],
                    [
                        'id' => 'BDGS-1639031238',
                        'form_id' => '',
                        'service_type' => 'g2g',
                        'name' => 'সংগনিরোধ ছুটি'
                    ],
                    [
                        'id' => 'BDGS-1605511065',
                        'form_id' => '777',
                        'service_type' => 'g2g',
                        'name' => 'প্রসূতি ছুটির জন্য আবেদন'
                    ],
                    [
                        'id' => 'BDGS-1639031339',
                        'form_id' => '',
                        'service_type' => 'g2g',
                        'name' => 'প্রাপ্যতাবিহীন ছুটি'
                    ],
                    [
                        'id' => 'BDGS-1639031426',
                        'form_id' => '',
                        'service_type' => 'g2g',
                        'name' => 'অবসর-উত্তর ছুটি'
                    ],
                    [
                        'id' => 'BDGS-1531733932',
                        'form_id' => '11',
                        'service_type' => 'g2g',
                        'name' => 'নৈমিত্তিক ছুটির জন্য আবেদন'
                    ],
                    [
                        'id' => 'BDGS-1639031501',
                        'form_id' => '',
                        'service_type' => 'g2g',
                        'name' => 'অক্ষমতাজনিত বিশেষ ছুটি'
                    ],
                    [
                        'id' => 'BDGS-1638252679',
                        'form_id' => '1886',
                        'service_type' => 'g2g',
                        'name' => 'চিকিৎসা ছুটি মঞ্জুরের আবেদন'
                    ],
                    [
                        'id' => 'BDGS-1638257451',
                        'form_id' => '1889',
                        'service_type' => 'g2g',
                        'name' => 'চিকিৎসা ছুটি মঞ্জুর (বহিঃবাংলাদেশ)'
                    ],
                    [
                        'id' => 'BDGS-1639031640',
                        'form_id' => '',
                        'service_type' => 'g2g',
                        'name' => 'বিশেষ অসুস্থ্যতাজনিত ছুটি'
                    ],
                    [
                        'id' => 'BDGS-1545908174',
                        'form_id' => '188',
                        'service_type' => 'g2g',
                        'name' => 'শ্রান্তি-বিনোদন ছুটি ও ভাতা মঞ্জুরীর আবেদন'
                    ],
                    [
                        'id' => 'BDGS-1630918419',
                        'form_id' => '1556',
                        'service_type' => 'g2g',
                        'name' => 'সাধারণ ভবিষ্যৎ তহবিল হতে অগ্রিম মঞ্জুরি'
                    ],
                    [
                        'id' => 'BDGS-1638260689',
                        'form_id' => '1890',
                        'service_type' => 'g2g',
                        'name' => 'চাকরি হতে অব্যাহতির জন্য আবেদন'
                    ],
                    [
                        'id' => 'BDGS-1583900979',
                        'form_id' => '407',
                        'service_type' => 'g2g',
                        'name' => 'চাকুরী নিয়মিতকরনের জন্য আবেদন'
                    ],
                    [
                        'id' => 'BDGS-1630906012',
                        'form_id' => '1536',
                        'service_type' => 'g2g',
                        'name' => 'চাকুরী স্থায়ীকরণের আবেদন'
                    ],
                    [
                        'id' => 'BDGS-1583818332',
                        'form_id' => '396',
                        'service_type' => 'g2g',
                        'name' => 'পাসপোর্টের অনাপত্তির জন্য আবেদন'
                    ],
                    [
                        'id' => 'BDGS-1566468976',
                        'form_id' => '229',
                        'service_type' => 'g2g',
                        'name' => 'গাড়ী রিকুইজিশনের জন্য আবেদন'
                    ]
                ]
            ],
            [
                'id' => 8,
                'office_layer' => 3,
                'doptorname' => 'পল্লী দারিদ্র্য বিমোচন ফাউন্ডেশন (পিডিবিএফ)',
                'logo' => 'pdbf.png',
                'services' => [
                    [
                        'id' => 'BDGS-1638246054',
                        'form_id' => '1865',
                        'service_type' => 'g2c',
                        'name' => 'নারী উদ্যোক্তা উদ্দীপন ও উদ্যোক্তা সৃষ্টির লক্ষ্যে ঋণ প্রাপ্তিতে সহায়তা প্রদান'
                    ],
                    [
                        'id' => 'BDGS-1638246865',
                        'form_id' => '1868',
                        'service_type' => 'g2c',
                        'name' => 'বীমা সুবিধা প্রাপ্তিতে সহায়তা প্রদান'
                    ],
                    [
                        'id' => 'BDGS-1638251650',
                        'form_id' => '1882',
                        'service_type' => 'g2c',
                        'name' => 'তথ্য অধিকার আইনে তথ্য প্রাপ্তির আবেদনপত্র'
                    ],
                    [
                        'id' => 'BDGS-1605509762',
                        'form_id' => '770',
                        'service_type' => 'g2c',
                        'name' => 'তথ্য অধিকার আইনে আপিল আবেদন'
                    ],
                    [
                        'id' => 'BDGS-1628673117',
                        'form_id' => '1509',
                        'service_type' => 'g2g',
                        'name' => 'অর্জিত ছুটির (অভ্যন্তরীণ) মঞ্জুরের আবেদন'
                    ],
                    [
                        'id' => 'BDGS-1630901329',
                        'form_id' => '1519',
                        'service_type' => 'g2g',
                        'name' => 'অর্জিত ছুটি (বহিঃ বাংলাদেশ) মঞ্জুরের আবেদন'
                    ],
                    [
                        'id' => 'BDGS-1639030213',
                        'form_id' => '',
                        'service_type' => 'g2g',
                        'name' => 'অসাধারণ ছুটি'
                    ],
                    [
                        'id' => 'BDGS-1639030724',
                        'form_id' => '',
                        'service_type' => 'g2g',
                        'name' => 'অধ্যয়ন ছুটি'
                    ],
                    [
                        'id' => 'BDGS-1639031238',
                        'form_id' => '',
                        'service_type' => 'g2g',
                        'name' => 'সংগনিরোধ ছুটি'
                    ],
                    [
                        'id' => 'BDGS-1605511065',
                        'form_id' => '777',
                        'service_type' => 'g2g',
                        'name' => 'প্রসূতি ছুটির জন্য আবেদন'
                    ],
                    [
                        'id' => 'BDGS-1639031339',
                        'form_id' => '',
                        'service_type' => 'g2g',
                        'name' => 'প্রাপ্যতাবিহীন ছুটি'
                    ],
                    [
                        'id' => 'BDGS-1639031426',
                        'form_id' => '',
                        'service_type' => 'g2g',
                        'name' => 'অবসর-উত্তর ছুটি'
                    ],
                    [
                        'id' => 'BDGS-1531733932',
                        'form_id' => '11',
                        'service_type' => 'g2g',
                        'name' => 'নৈমিত্তিক ছুটির জন্য আবেদন'
                    ],
                    [
                        'id' => 'BDGS-1639031501',
                        'form_id' => '',
                        'service_type' => 'g2g',
                        'name' => 'অক্ষমতাজনিত বিশেষ ছুটি'
                    ],
                    [
                        'id' => 'BDGS-1638252679',
                        'form_id' => '1886',
                        'service_type' => 'g2g',
                        'name' => 'চিকিৎসা ছুটি মঞ্জুরের আবেদন'
                    ],
                    [
                        'id' => 'BDGS-1638257451',
                        'form_id' => '1889',
                        'service_type' => 'g2g',
                        'name' => 'চিকিৎসা ছুটি মঞ্জুর (বহিঃবাংলাদেশ)'
                    ],
                    [
                        'id' => 'BDGS-1639031640',
                        'form_id' => '',
                        'service_type' => 'g2g',
                        'name' => 'বিশেষ অসুস্থ্যতাজনিত ছুটি'
                    ],
                    [
                        'id' => 'BDGS-1545908174',
                        'form_id' => '188',
                        'service_type' => 'g2g',
                        'name' => 'শ্রান্তি-বিনোদন ছুটি ও ভাতা মঞ্জুরীর আবেদন'
                    ],
                    [
                        'id' => 'BDGS-1630918419',
                        'form_id' => '1556',
                        'service_type' => 'g2g',
                        'name' => 'সাধারণ ভবিষ্যৎ তহবিল হতে অগ্রিম মঞ্জুরি'
                    ],
                    [
                        'id' => 'BDGS-1638260689',
                        'form_id' => '1890',
                        'service_type' => 'g2g',
                        'name' => 'চাকরি হতে অব্যাহতির জন্য আবেদন'
                    ],
                    [
                        'id' => 'BDGS-1583900979',
                        'form_id' => '407',
                        'service_type' => 'g2g',
                        'name' => 'চাকুরী নিয়মিতকরনের জন্য আবেদন'
                    ],
                    [
                        'id' => 'BDGS-1630906012',
                        'form_id' => '1536',
                        'service_type' => 'g2g',
                        'name' => 'চাকুরী স্থায়ীকরণের আবেদন'
                    ],
                    [
                        'id' => 'BDGS-1583818332',
                        'form_id' => '396',
                        'service_type' => 'g2g',
                        'name' => 'পাসপোর্টের অনাপত্তির জন্য আবেদন'
                    ],
                    [
                        'id' => 'BDGS-1566468976',
                        'form_id' => '229',
                        'service_type' => 'g2g',
                        'name' => 'গাড়ী রিকুইজিশনের জন্য আবেদন'
                    ]
                ]
            ],
            [
                'id' => 9,
                'office_layer' => 3,
                'doptorname' => 'ক্ষুদ্র কৃষক উন্নয়ন ফাউন্ডেশন (এসএফডিএফ)',
                'logo' => 'sfdf.png',
                'services' => [
                    [
                        'id' => 'BDGS-1638251841',
                        'form_id' => '1884',
                        'name' => 'সদস্য ভর্তি আবেদন'
                    ],
                    [
                        'id' => 'BDGS-1638251650',
                        'form_id' => '1882',
                        'service_type' => 'g2c',
                        'name' => 'তথ্য অধিকার আইনে তথ্য প্রাপ্তির আবেদনপত্র'
                    ],
                    [
                        'id' => 'BDGS-1605509762',
                        'form_id' => '770',
                        'service_type' => 'g2c',
                        'name' => 'তথ্য অধিকার আইনে আপিল আবেদন'
                    ],
                    [
                        'id' => 'BDGS-1628673117',
                        'form_id' => '1509',
                        'service_type' => 'g2g',
                        'name' => 'অর্জিত ছুটির (অভ্যন্তরীণ) মঞ্জুরের আবেদন'
                    ],
                    [
                        'id' => 'BDGS-1630901329',
                        'form_id' => '1519',
                        'service_type' => 'g2g',
                        'name' => 'অর্জিত ছুটি (বহিঃ বাংলাদেশ) মঞ্জুরের আবেদন'
                    ],
                    [
                        'id' => 'BDGS-1639030213',
                        'form_id' => '',
                        'service_type' => 'g2g',
                        'name' => 'অসাধারণ ছুটি'
                    ],
                    [
                        'id' => 'BDGS-1639030724',
                        'form_id' => '',
                        'service_type' => 'g2g',
                        'name' => 'অধ্যয়ন ছুটি'
                    ],
                    [
                        'id' => 'BDGS-1639031238',
                        'form_id' => '',
                        'service_type' => 'g2g',
                        'name' => 'সংগনিরোধ ছুটি'
                    ],
                    [
                        'id' => 'BDGS-1605511065',
                        'form_id' => '777',
                        'service_type' => 'g2g',
                        'name' => 'প্রসূতি ছুটির জন্য আবেদন'
                    ],
                    [
                        'id' => 'BDGS-1639031339',
                        'form_id' => '',
                        'service_type' => 'g2g',
                        'name' => 'প্রাপ্যতাবিহীন ছুটি'
                    ],
                    [
                        'id' => 'BDGS-1639031426',
                        'form_id' => '',
                        'service_type' => 'g2g',
                        'name' => 'অবসর-উত্তর ছুটি'
                    ],
                    [
                        'id' => 'BDGS-1531733932',
                        'form_id' => '11',
                        'service_type' => 'g2g',
                        'name' => 'নৈমিত্তিক ছুটির জন্য আবেদন'
                    ],
                    [
                        'id' => 'BDGS-1639031501',
                        'form_id' => '',
                        'service_type' => 'g2g',
                        'name' => 'অক্ষমতাজনিত বিশেষ ছুটি'
                    ],
                    [
                        'id' => 'BDGS-1638252679',
                        'form_id' => '1886',
                        'service_type' => 'g2g',
                        'name' => 'চিকিৎসা ছুটি মঞ্জুরের আবেদন'
                    ],
                    [
                        'id' => 'BDGS-1638257451',
                        'form_id' => '1889',
                        'service_type' => 'g2g',
                        'name' => 'চিকিৎসা ছুটি মঞ্জুর (বহিঃবাংলাদেশ)'
                    ],
                    [
                        'id' => 'BDGS-1639031640',
                        'form_id' => '',
                        'service_type' => 'g2g',
                        'name' => 'বিশেষ অসুস্থ্যতাজনিত ছুটি'
                    ],
                    [
                        'id' => 'BDGS-1545908174',
                        'form_id' => '188',
                        'service_type' => 'g2g',
                        'name' => 'শ্রান্তি-বিনোদন ছুটি ও ভাতা মঞ্জুরীর আবেদন'
                    ],
                    [
                        'id' => 'BDGS-1630918419',
                        'form_id' => '1556',
                        'service_type' => 'g2g',
                        'name' => 'সাধারণ ভবিষ্যৎ তহবিল হতে অগ্রিম মঞ্জুরি'
                    ],
                    [
                        'id' => 'BDGS-1638260689',
                        'form_id' => '1890',
                        'service_type' => 'g2g',
                        'name' => 'চাকরি হতে অব্যাহতির জন্য আবেদন'
                    ],
                    [
                        'id' => 'BDGS-1583900979',
                        'form_id' => '407',
                        'service_type' => 'g2g',
                        'name' => 'চাকুরী নিয়মিতকরনের জন্য আবেদন'
                    ],
                    [
                        'id' => 'BDGS-1630906012',
                        'form_id' => '1536',
                        'service_type' => 'g2g',
                        'name' => 'চাকুরী স্থায়ীকরণের আবেদন'
                    ],
                    [
                        'id' => 'BDGS-1583818332',
                        'form_id' => '396',
                        'service_type' => 'g2g',
                        'name' => 'পাসপোর্টের অনাপত্তির জন্য আবেদন'
                    ],
                    [
                        'id' => 'BDGS-1566468976',
                        'form_id' => '229',
                        'service_type' => 'g2g',
                        'name' => 'গাড়ী রিকুইজিশনের জন্য আবেদন'
                    ]
                ]
            ],
            [
                'id' => 10,
                'office_layer' => 3,
                'doptorname' => 'বাংলাদেশ সমবায় ব্যাংক লিঃ (বিএসবিএল)',
                'logo' => 'bsbl.png',
                'services' => [
                    [
                        'id' => 'BDGS-1638251650',
                        'form_id' => '1882',
                        'service_type' => 'g2c',
                        'name' => 'তথ্য অধিকার আইনে তথ্য প্রাপ্তির আবেদনপত্র'
                    ],
                    [
                        'id' => 'BDGS-1605509762',
                        'form_id' => '770',
                        'service_type' => 'g2c',
                        'name' => 'তথ্য অধিকার আইনে আপিল আবেদন'
                    ],
                    [
                        'id' => 'BDGS-1628673117',
                        'form_id' => '1509',
                        'service_type' => 'g2g',
                        'name' => 'অর্জিত ছুটির (অভ্যন্তরীণ) মঞ্জুরের আবেদন'
                    ],
                    [
                        'id' => 'BDGS-1630901329',
                        'form_id' => '1519',
                        'service_type' => 'g2g',
                        'name' => 'অর্জিত ছুটি (বহিঃ বাংলাদেশ) মঞ্জুরের আবেদন'
                    ],
                    [
                        'id' => 'BDGS-1639030213',
                        'form_id' => '',
                        'service_type' => 'g2g',
                        'name' => 'অসাধারণ ছুটি'
                    ],
                    [
                        'id' => 'BDGS-1639030724',
                        'form_id' => '',
                        'service_type' => 'g2g',
                        'name' => 'অধ্যয়ন ছুটি'
                    ],
                    [
                        'id' => 'BDGS-1639031238',
                        'form_id' => '',
                        'service_type' => 'g2g',
                        'name' => 'সংগনিরোধ ছুটি'
                    ],
                    [
                        'id' => 'BDGS-1605511065',
                        'form_id' => '777',
                        'service_type' => 'g2g',
                        'name' => 'প্রসূতি ছুটির জন্য আবেদন'
                    ],
                    [
                        'id' => 'BDGS-1639031339',
                        'form_id' => '',
                        'service_type' => 'g2g',
                        'name' => 'প্রাপ্যতাবিহীন ছুটি'
                    ],
                    [
                        'id' => 'BDGS-1639031426',
                        'form_id' => '',
                        'service_type' => 'g2g',
                        'name' => 'অবসর-উত্তর ছুটি'
                    ],
                    [
                        'id' => 'BDGS-1531733932',
                        'form_id' => '11',
                        'service_type' => 'g2g',
                        'name' => 'নৈমিত্তিক ছুটির জন্য আবেদন'
                    ],
                    [
                        'id' => 'BDGS-1639031501',
                        'form_id' => '',
                        'service_type' => 'g2g',
                        'name' => 'অক্ষমতাজনিত বিশেষ ছুটি'
                    ],
                    [
                        'id' => 'BDGS-1638252679',
                        'form_id' => '1886',
                        'service_type' => 'g2g',
                        'name' => 'চিকিৎসা ছুটি মঞ্জুরের আবেদন'
                    ],
                    [
                        'id' => 'BDGS-1638257451',
                        'form_id' => '1889',
                        'service_type' => 'g2g',
                        'name' => 'চিকিৎসা ছুটি মঞ্জুর (বহিঃবাংলাদেশ)'
                    ],
                    [
                        'id' => 'BDGS-1639031640',
                        'form_id' => '',
                        'service_type' => 'g2g',
                        'name' => 'বিশেষ অসুস্থ্যতাজনিত ছুটি'
                    ],
                    [
                        'id' => 'BDGS-1545908174',
                        'form_id' => '188',
                        'service_type' => 'g2g',
                        'name' => 'শ্রান্তি-বিনোদন ছুটি ও ভাতা মঞ্জুরীর আবেদন'
                    ],
                    [
                        'id' => 'BDGS-1630918419',
                        'form_id' => '1556',
                        'service_type' => 'g2g',
                        'name' => 'সাধারণ ভবিষ্যৎ তহবিল হতে অগ্রিম মঞ্জুরি'
                    ],
                    [
                        'id' => 'BDGS-1638260689',
                        'form_id' => '1890',
                        'service_type' => 'g2g',
                        'name' => 'চাকরি হতে অব্যাহতির জন্য আবেদন'
                    ],
                    [
                        'id' => 'BDGS-1583900979',
                        'form_id' => '407',
                        'service_type' => 'g2g',
                        'name' => 'চাকুরী নিয়মিতকরনের জন্য আবেদন'
                    ],
                    [
                        'id' => 'BDGS-1630906012',
                        'form_id' => '1536',
                        'service_type' => 'g2g',
                        'name' => 'চাকুরী স্থায়ীকরণের আবেদন'
                    ],
                    [
                        'id' => 'BDGS-1583818332',
                        'form_id' => '396',
                        'service_type' => 'g2g',
                        'name' => 'পাসপোর্টের অনাপত্তির জন্য আবেদন'
                    ],
                    [
                        'id' => 'BDGS-1566468976',
                        'form_id' => '229',
                        'service_type' => 'g2g',
                        'name' => 'গাড়ী রিকুইজিশনের জন্য আবেদন'
                    ]
                ]
            ],
            [
                'id' => 11,
                'office_layer' => 3,
                'doptorname' => 'বাংলাদেশ জাতীয় পল্লী উন্নয়ন সমবায় ফেডারেশন (বিএনসিএফআরডি)',
                'logo' => 'ncf.png',
                'services' => [
                    [
                        'id' => 'BDGS-1638252764',
                        'form_id' => '1887',
                        'service_type' => 'g2c',
                        'name' => 'অভিযোগ গ্রহণের মাধ্যমে উপজেলা কেন্দ্রীয় সমবায় সমিতি সমুহের সমস্যা সমাধান'
                    ],
                    [
                        'id' => 'BDGS-1638251650',
                        'form_id' => '1882',
                        'service_type' => 'g2c',
                        'name' => 'তথ্য অধিকার আইনে তথ্য প্রাপ্তির আবেদনপত্র'
                    ],
                    [
                        'id' => 'BDGS-1605509762',
                        'form_id' => '770',
                        'service_type' => 'g2c',
                        'name' => 'তথ্য প্রাপ্তির আপিল অভিযোগ আবেদন'
                    ],
                    [
                        'id' => 'BDGS-1628673117',
                        'form_id' => '1509',
                        'service_type' => 'g2g',
                        'name' => 'অর্জিত ছুটির (অভ্যন্তরীণ) মঞ্জুরের আবেদন'
                    ],
                    [
                        'id' => 'BDGS-1630901329',
                        'form_id' => '1519',
                        'service_type' => 'g2g',
                        'name' => 'অর্জিত ছুটি (বহিঃ বাংলাদেশ) মঞ্জুরের আবেদন'
                    ],
                    [
                        'id' => 'BDGS-1639030213',
                        'form_id' => '',
                        'service_type' => 'g2g',
                        'name' => 'অসাধারণ ছুটি'
                    ],
                    [
                        'id' => 'BDGS-1639030724',
                        'form_id' => '',
                        'service_type' => 'g2g',
                        'name' => 'অধ্যয়ন ছুটি'
                    ],
                    [
                        'id' => 'BDGS-1639031238',
                        'form_id' => '',
                        'service_type' => 'g2g',
                        'name' => 'সংগনিরোধ ছুটি'
                    ],
                    [
                        'id' => 'BDGS-1605511065',
                        'form_id' => '777',
                        'service_type' => 'g2g',
                        'name' => 'প্রসূতি ছুটির জন্য আবেদন'
                    ],
                    [
                        'id' => 'BDGS-1639031339',
                        'form_id' => '',
                        'service_type' => 'g2g',
                        'name' => 'প্রাপ্যতাবিহীন ছুটি'
                    ],
                    [
                        'id' => 'BDGS-1639031426',
                        'form_id' => '',
                        'service_type' => 'g2g',
                        'name' => 'অবসর-উত্তর ছুটি'
                    ],
                    [
                        'id' => 'BDGS-1531733932',
                        'form_id' => '11',
                        'service_type' => 'g2g',
                        'name' => 'নৈমিত্তিক ছুটির জন্য আবেদন'
                    ],
                    [
                        'id' => 'BDGS-1639031501',
                        'form_id' => '',
                        'service_type' => 'g2g',
                        'name' => 'অক্ষমতাজনিত বিশেষ ছুটি'
                    ],
                    [
                        'id' => 'BDGS-1638252679',
                        'form_id' => '1886',
                        'service_type' => 'g2g',
                        'name' => 'চিকিৎসা ছুটি মঞ্জুরের আবেদন'
                    ],
                    [
                        'id' => 'BDGS-1638257451',
                        'form_id' => '1889',
                        'service_type' => 'g2g',
                        'name' => 'চিকিৎসা ছুটি মঞ্জুর (বহিঃবাংলাদেশ)'
                    ],
                    [
                        'id' => 'BDGS-1639031640',
                        'form_id' => '',
                        'service_type' => 'g2g',
                        'name' => 'বিশেষ অসুস্থ্যতাজনিত ছুটি'
                    ],
                    [
                        'id' => 'BDGS-1545908174',
                        'form_id' => '188',
                        'service_type' => 'g2g',
                        'name' => 'শ্রান্তি-বিনোদন ছুটি ও ভাতা মঞ্জুরীর আবেদন'
                    ],
                    [
                        'id' => 'BDGS-1630918419',
                        'form_id' => '1556',
                        'service_type' => 'g2g',
                        'name' => 'সাধারণ ভবিষ্যৎ তহবিল হতে অগ্রিম মঞ্জুরি'
                    ],
                    [
                        'id' => 'BDGS-1638260689',
                        'form_id' => '1890',
                        'service_type' => 'g2g',
                        'name' => 'চাকরি হতে অব্যাহতির জন্য আবেদন'
                    ],
                    [
                        'id' => 'BDGS-1583900979',
                        'form_id' => '407',
                        'service_type' => 'g2g',
                        'name' => 'চাকুরী নিয়মিতকরনের জন্য আবেদন'
                    ],
                    [
                        'id' => 'BDGS-1630906012',
                        'form_id' => '1536',
                        'service_type' => 'g2g',
                        'name' => 'চাকুরী স্থায়ীকরণের আবেদন'
                    ],
                    [
                        'id' => 'BDGS-1583818332',
                        'form_id' => '396',
                        'service_type' => 'g2g',
                        'name' => 'পাসপোর্টের অনাপত্তির জন্য আবেদন'
                    ],
                    [
                        'id' => 'BDGS-1566468976',
                        'form_id' => '229',
                        'service_type' => 'g2g',
                        'name' => 'গাড়ী রিকুইজিশনের জন্য আবেদন'
                    ]
                ]
            ],
        ];

        return $serviceList;
    }
}
