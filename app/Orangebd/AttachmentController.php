<?php
namespace App\Controller;

use Cake\Event\Event;
use Cake\Datasource\ConnectionManager;
use App\Orangebd;

class AttachmentController extends AppController
{

    public function beforeFilter(Event $event)
    {

    }

    public function index(){
        $this->set('title','Bangladesh Service Application Platform');
        $session = $this->request->session();
        $user = $session->read('Auth.User');

        if(isset($this->request->query['fid'])) {
            $encrypt = new Orangebd\Encrypt();

            $fid = $this->request->query['fid'];

            $citizenDb = ConnectionManager::get('citizen_db');
            $this->loadModel('UsersStorage');
            $this->UsersStorage->connection($citizenDb);
            $this->UsersStorage->setCustomTable();
            $storage = $this->UsersStorage->getStorageByFid($fid,$user['id'],$user['user_type']);

            if(($user['id']==$storage['uid'] && $user['user_type']==$storage['user_type']) || $storage['user_type']==3) {

                if($storage['user_type']==3)
                    $filePath = 'storage/assistant';
                else {
                    if ($user['user_type'] == 1)
                        $filePath = 'storage/citizen';
                    else if ($user['user_type'] == 2)
                        $filePath = 'storage/institution';
                    else if ($user['user_type'] == 3)
                        $filePath = 'storage/assistant';
                }

                if($storage['user_type']==3)
                    $filePath = $filePath.'/'.$encrypt->encodePath($storage['uid']).'/'.$storage['file_name'];
                else
                    $filePath = $filePath.'/'.$encrypt->encodePath($user['id']).'/'.$storage['file_name'];

                $ext = explode('.',$storage['file_name']);
                $ext = end($ext);

                if(file_exists($filePath)) {

                    header('Content-Description: File Transfer');

                    header('Content-Type: message/octet-stream');

                    header('Content-Disposition: attachment; filename="'.basename(time().'.'.$ext).'"');

                    header('Expires: 0');

                    header('Cache-Control: must-revalidate');

                    header('Pragma: public');

                    header('Content-Length: ' . filesize($filePath));

                    flush(); // Flush system output buffer

                    readfile($filePath);

                    exit;

                }

            }
        }
        else if(isset($this->request->query['pid'])){
            $encrypt = new Orangebd\Encrypt();

            $pid = $this->request->query['pid'];
            $pid = $pid.'.pdf';

            $session = $this->request->session();
            $user = $session->read('Auth.User');

            $citizenDb = ConnectionManager::get('citizen_db');
            $this->loadModel('UsersStorage');
            $this->UsersStorage->connection($citizenDb);
            $this->UsersStorage->setCustomTable();


            //if($user['user_type']==1)
                $filePath = 'storage/citizen';
            //else if($user['user_type']==2)
            //    $filePath = 'storage/institution';
            if($user['user_type']==1)
                $filePath = 'storage/citizen';
            else if($user['user_type']==2)
                $filePath = 'storage/institution';
            else if($user['user_type']==3)
                $filePath = 'storage/assistant';

            $filePath = $filePath.'/'.$encrypt->encodePath($user['id']).'/'.$pid;
            $ext = explode('.',$pid);
            $ext = end($ext);

            if(file_exists($filePath)) {

                header('Content-Description: File Transfer');

                header('Content-Type: message/octet-stream');

                header('Content-Disposition: attachment; filename="'.basename(time().'.'.$ext).'"');

                header('Expires: 0');

                header('Cache-Control: must-revalidate');

                header('Pragma: public');

                header('Content-Length: ' . filesize($filePath));

                flush(); // Flush system output buffer

                readfile($filePath);

                exit;

            }
        }
        die();
    }

    public function view(){
        $this->set('title','Bangladesh Service Application Platform');

        if(isset($this->request->query['fid'])) {
            $encrypt = new Orangebd\Encrypt();

            $fid = $this->request->query['fid'];

            $session = $this->request->session();
            $user = $session->read('Auth.User');

            $citizenDb = ConnectionManager::get('citizen_db');
            $this->loadModel('UsersStorage');
            $this->UsersStorage->connection($citizenDb);
            $this->UsersStorage->setCustomTable();
            $storage = $this->UsersStorage->getStorageByFid($fid,$user['id'],$user['user_type']);

            if($user['id']==$storage['uid'] && $user['user_type']==$storage['user_type']) {

                if($user['user_type']==1)
                    $filePath = 'storage/citizen';
                else if($user['user_type']==2)
                    $filePath = 'storage/institution';

                $filePath = $filePath.'/'.$encrypt->encodePath($user['id']).'/'.$storage['file_name'];

                $ext = explode('.',$storage['file_name']);
                $ext = end($ext);

                if(file_exists($filePath)) {

                    //header('Content-Description: File Transfer');

                    header('Content-Type: '.mime_content_type($filePath));

                    /*header('Content-Disposition: attachment; filename="'.basename(time().'.'.$ext).'"');

                    header('Expires: 0');

                    header('Cache-Control: must-revalidate');

                    header('Pragma: public');

                    header('Content-Length: ' . filesize($filePath));

                    flush();*/

                    readfile($filePath);

                    exit;

                }

            }
        }
        die();
    }

    public function rename(){
        if ($this->request->is('post')){
            $fid = $this->request->data['fid'];
            $session = $this->request->session();
            $user = $session->read('Auth.User');

            $citizenDb = ConnectionManager::get('citizen_db');
            $this->loadModel('UsersStorage');
            $this->UsersStorage->connection($citizenDb);
            $this->UsersStorage->setCustomTable();
            $storage = $this->UsersStorage->getStorageByFid($fid,$user['id'],$user['user_type']);

            $data['display_name'] = $this->request->data['name'];
            $storage = $this->UsersStorage->get($storage['id']);
            $storage = $this->UsersStorage->patchEntity($storage, $data);
            $this->UsersStorage->save($storage);

            return $this->redirect('/profile#3');
        }

        die();
    }

    public function upload($id){

        $this->set('title','Bangladesh Service Application Platform');
        if(isset($id)){
            $this->loadModel('ServiceDocs');
            $docs = $this->ServiceDocs->getActiveDocsById($id);
            $this->set('docs',$docs);

            $this->set('key',$id);

            if(isset($this->request->query['error']))
                $this->set('error',$this->request->query['error']);
        }
    }

    public function uploaded($id){
        $this->set('title','Bangladesh Service Application Platform');
        $this->set('id',$id);
    }

    public function load($id){
        $session = $this->request->session();
        $user = $session->read('Auth.User');

        $citizenDb = ConnectionManager::get('citizen_db');
        $this->loadModel('UsersStorage');
        $this->UsersStorage->connection($citizenDb);
        $this->UsersStorage->setCustomTable();
        $storage = $this->UsersStorage->getStorageByUidAndDocid($user['id'],$user['user_type'],$id);

        echo json_encode($storage);

        die();
    }
}
?>
