<?php
namespace App\Orangebd;

class DBSwitch
{
    public function changeByService($sid){

        $dbConfig = DBSwitch::where('sid',$sid)->limit('1')->get()->toArray();
        $dbConfig = $dbConfig[0];
        Config::set('database.connections.applicationDB', [
            'driver' => 'mysql',
            'host' => $dbConfig['db_host'],
            'port' => $dbConfig['db_port'],
            'database' => $dbConfig['db_name'],
            'username' => $dbConfig['db_user'],
            'password' => $dbConfig['db_password']
        ]);
    }
}
