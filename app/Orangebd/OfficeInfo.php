<?php

namespace App\Orangebd;

use Illuminate\Support\Facades\Storage;

class OfficeInfo
{
    private $cachePath;
    private $cacheGeoPath;

    public function __construct()
    {
        $this->cachePath = 'office/';
        $this->cacheGeoPath = 'geo/';
    }

    public function getOfficeOriginByLevel($data){

        $file = 'office_level_'.str_replace('office-level-','',$data).'.json';

        if(!Storage::disk('cache')->exists($this->cachePath.$file)){
            $this->cronOffice();
        }

        $officeLevel = Storage::disk('cache')->get($this->cachePath.$file);
        $officeLevel = json_decode($officeLevel, true);

        return $officeLevel;
    }

    public function getFullOfficeLayer(){
        $file = 'full_office_layer.json';
        if(!Storage::disk('cache')->exists($this->cachePath.$file)){
            $this->cronOffice();
        }
        $fullOfficeLayer = Storage::disk('cache')->get($this->cachePath.$file);
        $fullOfficeLayer = json_decode($fullOfficeLayer, true);

        return $fullOfficeLayer;
    }

    public function cronOffice()
    {
        $nDoptor = new nDoptor();

        $ministry = $nDoptor->data('officeministry?xglimit=10000&id=46');
        $ministry = json_decode($ministry, true);
        if(!empty($ministry)) {
            $tmp = array();
            foreach ($ministry as $value) {
                $tmp[$value['id']] = $value;
            }
            $ministry = $tmp;

            Storage::disk('cache')->put($this->cachePath . 'ministry.json', json_encode($ministry, JSON_UNESCAPED_UNICODE));
        }

        $officeOrigin = $nDoptor->data('officeorigin?xglimit=10000&ministry=46');
        $officeOrigin = json_decode($officeOrigin, true);
        //dd($officeOrigin);

        if(!empty($officeOrigin))
            Storage::disk('cache')->put($this->cachePath . 'office_origin.json',json_encode($officeOrigin, JSON_UNESCAPED_UNICODE));

        $offices = $nDoptor->data('office?xgselect=id+nameBn+name+ministry+layer+origin+division+district+upazila+union+website&xglimit=10000&ministry=46');
        $offices = json_decode($offices, true);
        //dd($offices);

        if(!empty($offices))
            Storage::disk('cache')->put($this->cachePath . 'offices.json',json_encode($offices, JSON_UNESCAPED_UNICODE));

        $tmp = $offices;
        $offices = array();
        foreach ($tmp as $v){
            $offices[] = array('name'=>strip_tags(preg_replace('/(\t)+/', '', $v['nameBn'])));
        }

        if(!empty($offices))
            Storage::disk('cache')->put($this->cachePath . 'offices_info.json',json_encode($offices, JSON_UNESCAPED_UNICODE));

        $officeLayer = $nDoptor->data('officelayer?xglimit=10000');
        $tmp = json_decode($officeLayer, true);
        $officeLayer = array();
        foreach ($tmp as $v){
            $officeLayer[] = array('id'=>$v['id'],'custom_layer_id'=>$v['customlayerid']);
        }

        if(!empty($officeLayer))
            Storage::disk('cache')->put($this->cachePath . 'office_layer.json',json_encode($officeLayer, JSON_UNESCAPED_UNICODE));


        /*Build Office by layer and others*/
        $newLayer = array();
        if(Storage::disk('cache')->exists($this->cachePath . 'custom_layer.json'))
            $newLayer = json_decode(Storage::disk('cache')->get($this->cachePath . 'custom_layer.json'),true);

        $officeLayer = array();
        if(Storage::disk('cache')->exists($this->cachePath . 'office_layer.json'))
            $officeLayer = json_decode(Storage::disk('cache')->get($this->cachePath . 'office_layer.json'),true);

        $officeOrigin = array();
        if(Storage::disk('cache')->exists($this->cachePath . 'office_origin.json'))
            $officeOrigin = json_decode(Storage::disk('cache')->get($this->cachePath . 'office_origin.json'),true);

        $offices = array();
        if(Storage::disk('cache')->exists($this->cachePath . 'offices.json'))
            $offices = json_decode(Storage::disk('cache')->get($this->cachePath . 'offices.json'),true);


        $districts = array();
        if(Storage::disk('cache')->exists($this->cacheGeoPath . 'geo_district.json'))
            $districts = json_decode(Storage::disk('cache')->get($this->cacheGeoPath . 'geo_district.json'),true);

        $result = array();
        $layerName = array('1' => 'মন্ত্রণালয়/বিভাগ', '2' => 'অধিদপ্তর', '3' => 'অন্যান্য দপ্তর/সংস্থা', '4' => 'বিভাগীয় পর্যায়ের কার্যালয়', '5' => 'জেলা পর্যায়ের কার্যালয়', '6' => 'উপজেলা পর্যায়ের কার্যালয়', '7' => 'আঞ্চলিক কার্যালয়');
        $layerFinal = array();

        /*Sort Layer*/
        $layerTemp = array();
        foreach ($newLayer as $key => $value) {
            $layerTemp[$value['level']][] = $value;
        }
        ksort($layerTemp);

        $tmp = array();
        foreach ($layerTemp[3] as $key => $value) {
            $tmp[$value['id']] = $value;
        }
        //unset($layerTemp[3]);
        $layerTemp[3] = $tmp;
        /*Sort Level*/

        /*Sort Layer Ay Layer Declare*/
        foreach ($layerTemp as $key => $value) {
            $tmp = array();
            foreach ($value as $k => $v) {
                $tmp[] = $v['id'];
            }
            $layerFinal[$key]['name'] = $layerName[$key];
            $layerFinal[$key]['ids'] = $tmp;
        }

        $tmp = array();
        foreach ($officeLayer as $key => $value) {
            $tmp[$value['custom_layer_id']][] = $value['id'];
        }
        $officeLayer = $tmp;

        $officeByLayer = array();
        $officeByOrigin = array();
        $officeById = array();
        foreach ($offices as $val) {
            $val['nameBn'] = strip_tags(preg_replace('/(\t)+/', '', $val['nameBn']));

            $officeByLayer[$val['layer']][] = $val;
            $officeByOrigin[$val['origin']][] = $val;
            $officeById[$val['id']] = $val;
        }

        $tmpOfficeId = $officeById;

        $officeOriginByLayerAndLevel = array();
        foreach ($officeOrigin as $key => $value) {
            $officeOriginByLayerAndLevel[$value['layer']][$value['level']][] = $value;
        }

        /*Get Ministry*/
        $tmp = array();
        foreach ($layerFinal[1]['ids'] as $key => $value) {
            if (!empty($officeLayer[$value]))
                $tmp = array_merge($tmp, $officeLayer[$value]);
        }

        $ministry = array();
        foreach ($tmp as $key => $value) {
            if (!empty($officeByLayer[$value])) {
                foreach ($officeByLayer[$value] as $k => $v) {
                    $ministry[] = $v;
                    unset($officeById[$v['id']]);
                }
            }
        }
        $result['ministryOffices'] = $ministry;
        /*Get Ministry*/

        /*Get Doptopr*/
        $tmp = array();
        foreach ($layerFinal[2]['ids'] as $key => $value) {
            if (!empty($officeLayer[$value]))
                $tmp = array_merge($tmp, $officeLayer[$value]);
        }
        $doptor = array();
        foreach ($tmp as $key => $value) {
            if (!empty($officeByLayer[$value])) {
                foreach ($officeByLayer[$value] as $k => $v) {
                    $doptor[] = $v;
                    unset($officeById[$v['id']]);
                }
            }
        }
        $result['doptorOfffices'] = $doptor;
        /*Get Doptopr*/

        /*Get Others*/
        $others = array();
        foreach ($layerFinal[3]['ids'] as $key => $value) {
            $tmp = array();
            if (!empty($officeLayer[$value])) {
                $tmp = array_merge($tmp, $officeLayer[$value]);

                $others[$value]['name'] = $layerTemp[3][$value]['name'];
                foreach ($tmp as $ke => $va) {
                    if (!empty($officeByLayer[$va])) {
                        foreach ($officeByLayer[$va] as $k => $v) {
                            $others[$value]['offices'][] = $v;
                            unset($officeById[$v['id']]);
                        }
                    }
                }
            }
        }
        $result['otherOffices'] = $others;
        /*Get Others*/

        /*Get Divisional Office*/
        $divisionalOffice = array();
        foreach ($layerFinal[4]['ids'] as $key => $value) {
            $tmp = array();
            if (!empty($officeLayer[$value])) {
                $tmp = array_merge($tmp, $officeLayer[$value]);
                foreach ($tmp as $ke => $va) {
                    if (!empty($officeOriginByLayerAndLevel[$va])) {
                        foreach ($officeOriginByLayerAndLevel[$va] as $kl => $vl) {
                            if (!empty($officeOriginByLayerAndLevel[$va][$kl])) {
                                foreach ($officeOriginByLayerAndLevel[$va][$kl] as $k => $v) {
                                    /*Set Origin Name*/
                                    $divisionalOffice[$v['id']] = array('name' => strip_tags(preg_replace('/(\t)+/', '', $v['nameBn'])), 'name_en' => strip_tags(preg_replace('/(\t)+/', '', $v['name'])));
                                }
                            }
                        }
                    }
                }
            }
        }

        foreach ($divisionalOffice as $key => $value) {
            if (!empty($officeByOrigin[$key])) {
                $divisionalOffice[$key]['offices'] = $officeByOrigin[$key];

                if (is_array($officeByOrigin[$key])) {
                    foreach ($officeByOrigin[$key] as $v) {
                        unset($officeById[$v['id']]);
                    }
                }
            }
        }
        $result['divisionOffices'] = $divisionalOffice;
        /*Get Divisional Office*/

        /*Get District Office*/
        $districtOffices = array();
        foreach ($layerFinal[5]['ids'] as $key => $value) {
            $tmp = array();
            if (!empty($officeLayer[$value])) {
                //$tmp = array_merge($tmp, $officeLayer[$value]);
                $tmp = $officeLayer[$value];

                foreach ($tmp as $ke => $va) {
                    if (!empty($officeOriginByLayerAndLevel[$va])) {
                        foreach ($officeOriginByLayerAndLevel[$va] as $kl => $vl) {
                            if (!empty($officeOriginByLayerAndLevel[$va][$kl])) {
                                foreach ($officeOriginByLayerAndLevel[$va][$kl] as $k => $v) {
                                    /*Set Origin Name*/
                                    $districtOffices[$v['id']] = array('name' => strip_tags(preg_replace('/(\t)+/', '', $v['nameBn'])), 'name_en' => strip_tags(preg_replace('/(\t)+/', '', $v['name'])));
                                }
                            }
                        }
                    }
                }
            }
        }
        foreach ($districtOffices as $key => $value) {
            if (!empty($officeByOrigin[$key])) {
                $districtOffices[$key]['offices'] = $officeByOrigin[$key];

                if (is_array($officeByOrigin[$key])) {
                    foreach ($officeByOrigin[$key] as $v) {
                        unset($officeById[$v['id']]);
                    }
                }
            }
        }
        $result['districtOffices'] = $districtOffices;
        /*Get District Office*/

        /*Get Upazila Office*/
        $upazilaOffices = array();
        foreach ($layerFinal[6]['ids'] as $key => $value) {
            $tmp = array();
            if (!empty($officeLayer[$value])) {
                //$tmp = array_merge($tmp, $officeLayer[$value]);
                $tmp = $officeLayer[$value];
                foreach ($tmp as $ke => $va) {
                    if (!empty($officeOriginByLayerAndLevel[$va])) {
                        foreach ($officeOriginByLayerAndLevel[$va] as $kl => $vl) {
                            if (!empty($officeOriginByLayerAndLevel[$va][$kl])) {
                                foreach ($officeOriginByLayerAndLevel[$va][$kl] as $k => $v) {
                                    /*Set Origin Name*/
                                    $upazilaOffices[$v['id']] = array('name' => strip_tags(preg_replace('/(\t)+/', '', $v['nameBn'])), 'name_en' => strip_tags(preg_replace('/(\t)+/', '', $v['name'])));
                                }
                            }
                        }
                    }
                }
            }
        }
        foreach ($upazilaOffices as $key => $value) {
            if (!empty($officeByOrigin[$key])) {
                $tmpDistrict = array();
                foreach ($officeByOrigin[$key] as $k => $v) {
                    if ($v['district'] > 0) {
                        //echo $districts[$v['district']]['nameBn'];
                        $tmpDistrict[$v['district']]['name'] = @$districts[$v['district']]['nameBn'];
                        $tmpDistrict[$v['district']]['offices'][] = array('nameBn' => strip_tags(preg_replace('/(\t)+/', '', $v['nameBn'])), 'id' => $v['id']);
                        unset($officeById[$v['id']]);
                    }
                }
                $upazilaOffices[$key]['district'] = $tmpDistrict;
            }
        }
        $result['upazilaOffices'] = $upazilaOffices;
        /*Get Upazila Office*/

        /*Get Local Office*/
        $localOffice = array();
        foreach ($layerFinal[7]['ids'] as $key => $value) {
            $tmp = array();
            if (!empty($officeLayer[$value])) {
                //$tmp = array_merge($tmp, $officeLayer[$value]);
                $tmp = $officeLayer[$value];
                foreach ($tmp as $ke => $va) {
                    if (!empty($officeOriginByLayerAndLevel[$va])) {
                        foreach ($officeOriginByLayerAndLevel[$va] as $kl => $vl) {
                            if (!empty($officeOriginByLayerAndLevel[$va][$kl])) {
                                foreach ($officeOriginByLayerAndLevel[$va][$kl] as $k => $v) {
                                    /*Set Origin Name*/
                                    $localOffice[$v['id']] = array('name' => strip_tags(preg_replace('/(\t)+/', '', $v['nameBn'])), 'name_en' => strip_tags(preg_replace('/(\t)+/', '', $v['name'])));
                                }
                            }
                        }
                    }
                }
            }
        }
        foreach ($localOffice as $key => $value) {
            if (!empty($officeByOrigin[$key])) {
                $localOffice[$key]['offices'] = $officeByOrigin[$key];

                if (is_array($officeByOrigin[$key])) {
                    foreach ($officeByOrigin[$key] as $v) {
                        unset($officeById[$v['id']]);
                    }
                }
            }
        }
        $result['localOffices'] = $localOffice;
        /*Get Local Office*/

        if(!empty($result))
            Storage::disk('cache')->put($this->cachePath . 'full_office_layer.json',json_encode($result,JSON_UNESCAPED_UNICODE));

        if(!empty($officeById))
            Storage::disk('cache')->put($this->cachePath . 'empty_office.json',json_encode($officeById,JSON_UNESCAPED_UNICODE));

        //
        /*Office Origin*/
        //
        $offices = json_decode(Storage::disk('cache')->get($this->cachePath . 'full_office_layer.json'),true);

        /*Layer Ministry*/
        $data = array();
        foreach ($offices['ministryOffices'] as $v){
            $data[] = array('name'=>$v['nameBn'],'name_en'=>$v['name'],'id'=>$v['ministry']);
        }

        Storage::disk('cache')->put($this->cachePath . 'office_level_1.json',json_encode($data, JSON_UNESCAPED_UNICODE));
        /*Layer Ministry*/

        /*Layer Doptor*/
        $data = array();
        foreach ($offices['doptorOfffices'] as $v){
            $data[] = array('name'=>$v['nameBn'],'name_en'=>$v['name'],'id'=>$v['origin']);
        }

        Storage::disk('cache')->put($this->cachePath . 'office_level_2.json',json_encode($data, JSON_UNESCAPED_UNICODE));
        /*Layer Doptor*/

        /*Layer Other*/
        $data = array();
        foreach ($offices['otherOffices'] as $val){
            $tempArray = array();
            if(!empty($val['offices'])) {
                foreach ($val['offices'] as $v) {
                    $tempArray[$v['origin']] = array('name' => $v['nameBn'], 'name_en' => $v['name']);
                }
                $data[$val['name']] = $tempArray;
            }
        }

        Storage::disk('cache')->put($this->cachePath . 'office_level_3.json',json_encode($data, JSON_UNESCAPED_UNICODE));
        /*Layer Other*/

        /*Layer Divisional*/
        $data = array();
        foreach ($offices['divisionOffices'] as $k=>$v){
            $data[] = array('name' => $v['name'], 'name_en' => $v['name_en'],'id' => $k);
        }

        Storage::disk('cache')->put($this->cachePath . 'office_level_4.json',json_encode($data, JSON_UNESCAPED_UNICODE));
        /*Layer Divisional*/

        /*Layer District*/
        $data = array();
        foreach ($offices['districtOffices'] as $k=>$v){
            $data[] = array('name' => $v['name'], 'name_en' => $v['name_en'],'id' => $k);
        }

        Storage::disk('cache')->put($this->cachePath . 'office_level_5.json',json_encode($data, JSON_UNESCAPED_UNICODE));
        /*Layer District*/

        /*Layer Upazila*/
        $data = array();
        foreach ($offices['upazilaOffices'] as $k=>$v){
            $data[] = array('name' => $v['name'], 'name_en' => $v['name_en'],'id' => $k);
        }

        Storage::disk('cache')->put($this->cachePath . 'office_level_6.json',json_encode($data, JSON_UNESCAPED_UNICODE));
        /*Layer Upazila*/

        /*Layer Local*/
        $data = array();
        foreach ($offices['localOffices'] as $k=>$v){
            $data[] = array('name' => $v['name'], 'name_en' => $v['name_en'],'id' => $k);
        }

        Storage::disk('cache')->put($this->cachePath . 'office_level_7.json',json_encode($data, JSON_UNESCAPED_UNICODE));
        /*Layer Local*/
    }
}
