<?php

namespace App\Orangebd;

class ApplicationId
{
    public function parseApplicationId($aid){

        if(strpos($aid,'.')!==false){
            $tmp = explode('.',$aid);
            $sid = (int)$tmp[0];
            $oid = (int)$tmp[1];
        }else {
            $sid = substr($aid, 0, 5);
            $oid = substr($aid, 5, 7);

            $sid = rtrim($sid, '0');
            $oid = rtrim($oid, '0');

            $sid = substr($sid, 0, strlen($sid) - 1);
            $oid = substr($oid, 0, strlen($oid) - 1);
        }

        return array($sid,$oid);
    }

    public function getServiceId($aid){
        $result = $this->parseApplicationId($aid);
        return $result[0];
    }
}