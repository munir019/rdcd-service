<?php

namespace App\Orangebd;


class Workflow
{
    public function send($data, $sid, $user){

        $data = json_decode($data, true);

        $ownData = array();
        $ownData['client'] = 'mygov';
        $ownData['token'] = '123';
        $ownData['dataType'] = 'html';
        $ownData['data'] = $data['data'];
        $ownData['title'] = $data['application_subject'];
        $ownData['callback_url'] = $data['feedback_url'];
        $ownData['api_key'] = $data['api_key'];

        $metadata = array();
        $metadata['app']['appId'] = $sid;
        $metadata['app']['appTrackingId'] = $data['tracking_id'];
        $metadata['app']['appStatus'] = 1;
        $metadata['app']['date'] = date('Y-m-d');
        $metadata['app']['expDate'] = str_replace('T00:00:00+00:00','',$this->bngToEngNum($data['approximate_solve_date']));
        /*if(empty($metadata['app']['expDate']))
            unset($metadata['app']['expDate']);*/

        $metadata['user']['userName'] = $data['applicant_name'];
        $metadata['user']['userMobile'] = $data['mobile_no'];
        
        if(!empty($user['email'])){
            $metadata['user']['userEmail'] = $user['email'];
        }  
        else if(isset($data['email']))
            $metadata['user']['userEmail'] = $data['email'];
        else $metadata['user']['userEmail'] = "";


        $metadata['decision'] = $data['decision'];
        $metadata['workflow'] = '';

        $ownData['metadata'] = json_encode($metadata, JSON_UNESCAPED_UNICODE);
        $ownData['attachments'] = json_encode($data['attachments'], JSON_UNESCAPED_UNICODE);

        $ownData['stackHolderId'] = $data['office_id'];
        $ownData['currentStateId'] = $data['desks'];

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => 'https://workflow.training.mygov.bd/data/store',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => $ownData,
            CURLOPT_HTTPHEADER => array(
                'Authorization: Bearer zCLuiQPozCtpt'
            ),
        ));

        $response = curl_exec($curl);

        curl_close($curl);
        $response = json_decode($response, true);

        return $response;
    }

    private function bngToEngNum($num){
        $num =str_replace('০','0',$num);
        $num =str_replace('১','1',$num);
        $num =str_replace('২','2',$num);
        $num =str_replace('৩','3',$num);
        $num =str_replace('৪','4',$num);
        $num =str_replace('৫','5',$num);
        $num =str_replace('৬','6',$num);
        $num =str_replace('৭','7',$num);
        $num =str_replace('৮','8',$num);
        $num =str_replace('৯','9',$num);

        return $num;
    }
}