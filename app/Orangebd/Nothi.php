<?php

namespace App\Orangebd;


use App\Models\NothiStatus;

class Nothi
{
    private $nothiUrl = 'https://nothi.gov.bd';
    //private $nothiUrl = 'http://training.nothi.gov.bd';
    //private $nothiUrl = 'https://nothi.tappware.com';
    private $postData;

    public function sendToNothi($data,$trackingInfo=NULL){

        $direct_api_url = $this->nothiUrl . "/apimyGovApplicationRequest";

        $data = json_decode($data,true);
        $data['token']=$this->createToken();
        $data['api_client']='bsap';

        $trackingInfoNothi = '';
        if(!empty($trackingInfo))
            $trackingInfoNothi = json_decode($trackingInfo['tracking_info'],true);
        if(is_array($trackingInfoNothi)) {
            $trackingInfoNothi = json_decode($trackingInfo['tracking_info'],true);
            if(isset($trackingInfoNothi['potro_id'])) {
                $data['potro_id'] = $trackingInfoNothi['potro_id'];
                $data['nothi_id'] = $trackingInfoNothi['nothi_id'];
                $data['note_id'] = $trackingInfoNothi['note_id'];
            }

            if(isset($trackingInfo['action_log'])) {
                $tmp = array_values(json_decode($trackingInfo['action_log'], true));
                $data['decision_id'] = $tmp[0]['action'];
                $data['decision_note'] = $tmp[0]['note'];
            }
        }
        $this->http_build_query_for_curl($data,$this->postData);
        $post_data = $this->postData;

        return $this->curlRequest($direct_api_url,$post_data);
    }

    public function jsonSendToNothi($data,$trackingInfo=NULL){

        $direct_api_url = $this->nothiUrl . "/apimyGovApplicationRequest";

        $data = json_decode($data,true);
        $data['token']=$this->createToken();
        $data['api_client']='bsap';

        $trackingInfoNothi = '';
        if(!empty($trackingInfo))
            $trackingInfoNothi = json_decode($trackingInfo['tracking_info'],true);
        if(is_array($trackingInfoNothi)) {
            $trackingInfoNothi = json_decode($trackingInfo['tracking_info'],true);
            if(isset($trackingInfoNothi['potro_id'])) {
                $data['potro_id'] = $trackingInfoNothi['potro_id'];
                $data['nothi_id'] = $trackingInfoNothi['nothi_id'];
                $data['note_id'] = $trackingInfoNothi['note_id'];
            }

            if(isset($trackingInfo['action_log'])) {
                $tmp = array_values(json_decode($trackingInfo['action_log'], true));
                $data['decision_id'] = $tmp[0]['action'];
                $data['decision_note'] = $tmp[0]['note'];
            }
        }
        $this->http_build_query_for_curl($data,$this->postData);
        $post_data = $this->postData;

        return json_encode($post_data,JSON_UNESCAPED_UNICODE);
    }

    private function createToken(){
        $url = $this->nothiUrl.'/apiAccess';

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS,"username=orange-bsep&password=Ixgy95g4618M]9o");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_URL,$url);
        $result=curl_exec($ch);
        curl_close($ch);

        $result = json_decode($result,true);

        return $result['token'];
    }

    private function http_build_query_for_curl( $arrays, &$new = array(), $prefix = null ) {

        if ( is_object( $arrays ) ) {
            $arrays = get_object_vars( $arrays );
        }

        foreach ( $arrays AS $key => $value ) {
            $k = isset( $prefix ) ? $prefix . '[' . $key . ']' : $key;
            if ( is_array( $value ) OR is_object( $value )  ) {
                $this->http_build_query_for_curl( $value, $new, $k );
            } else {
                $new[$k] = $value;
            }
        }
    }

    public function checkOnNothi($tracking_id){
        $url = $this->nothiUrl . "/DakNagoriks/GetTracking";
        $data['token']=$this->createToken();
        $data['receive_no']=$tracking_id;

        $handle = curl_init();
        curl_setopt($handle, CURLOPT_URL, $url );
        curl_setopt($handle, CURLOPT_TIMEOUT, 30);
        curl_setopt($handle, CURLOPT_CONNECTTIMEOUT, 30);
        curl_setopt($handle, CURLOPT_HTTPHEADER, array("Content-type: multipart/form-data"));
        curl_setopt($handle, CURLOPT_POST, 1 );
        curl_setopt($handle, CURLOPT_POSTFIELDS, $data);
        curl_setopt($handle, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($handle, CURLOPT_SSL_VERIFYPEER, FALSE);

        $content = curl_exec($handle);

        $code = curl_getinfo($handle, CURLINFO_HTTP_CODE);

        return json_decode($content,true);
    }

    private function curlRequest($url,$data){

        /*$fp = fopen('storage/send.json', 'w');
        fwrite($fp, json_encode($data,JSON_UNESCAPED_UNICODE));
        fclose($fp);*/

        $handle = curl_init();
        curl_setopt($handle, CURLOPT_URL, $url );
        curl_setopt($handle, CURLOPT_TIMEOUT, 0);
        curl_setopt($handle, CURLOPT_CONNECTTIMEOUT, 30);
        curl_setopt($handle, CURLOPT_HTTPHEADER, array("Content-type: multipart/form-data"));
        curl_setopt($handle, CURLOPT_POST, 1 );
        curl_setopt($handle, CURLOPT_POSTFIELDS, $data);
        curl_setopt($handle, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($handle, CURLOPT_SSL_VERIFYPEER, FALSE);

        $content = curl_exec($handle);

        $code = curl_getinfo($handle, CURLINFO_HTTP_CODE);

        if($code == 200 && !( curl_errno($handle))) {
            curl_close( $handle);
        } else {
            curl_close( $handle);
            echo "FAILED TO CONNECT WITH eNOTHI";
            //exit;
        }
        //print_r($content);
        # PARSE THE JSON RESPONSE
        
        return json_decode($content, true );//$content;//
    }

    public function checkNothiStatus($nothiStatus, $application, $data){
        if (isset($nothiStatus['status']) && $nothiStatus['status'] == 'success') {
            $applicationSubmitted = true;
        } else if (isset($nothiStatus['status']) && $nothiStatus['status'] == 'error' && ($nothiStatus['message'] == 'অনিষ্পন্ন আবেদন, একই বিষয় দিয়ে পুনরায় আবেদন করায় এই আবেদনটি গ্রহণ করা হল না' || strpos($nothiStatus['message'], 'একই বিষয় দিয়ে পুনরায় আবেদন করায় এই আবেদনটি গ্রহণ করা হল না।') !== false)) {
            $applicationSubmitted = true;
        }else if (isset($nothiStatus['status']) && $nothiStatus['status'] == 'error' && (strpos($nothiStatus['message'], 'Invalid request! No Office found')!==false)) {
            $applicationSubmitted = false;
            $errorSkip = true;
        }else if (isset($nothiStatus['status']) && $nothiStatus['status'] == 'error' && (strpos($nothiStatus['message'], 'দুঃখিত! অফিসের ফ্রন্ট ডেস্ক এর তথ্য পাওয়া যায়নি') !== false)) {
            $applicationSubmitted = false;
            $errorSkip = true;
        }else if (isset($nothiStatus['status']) && $nothiStatus['status'] == 'error' && (strpos($nothiStatus['message'], 'অফিস ডাটাবেজ পাওয়া যায় নি! সাপোর্ট টিমের সাথে যোগাযোগ করুন। অফিস') !== false)) {
            $applicationSubmitted = false;
            $errorSkip = true;
        }else if (isset($nothiStatus['status']) && $nothiStatus['status'] == 'error' && (strpos($nothiStatus['message'], 'অফিস ডাটাবেজে সংযোগ করা যাচ্ছে না।') !== false)) {
            $applicationSubmitted = false;
            $errorSkip = true;
        } else {
            $applicationSubmitted = false;

            $nothiStatusLog = new NothiStatus();
            $nothiStatusLog->aid = $application->aid;
            $nothiStatusLog->data = json_encode($data, JSON_UNESCAPED_UNICODE);
            $nothiStatusLog->nothi_data = json_encode($nothiStatus, JSON_UNESCAPED_UNICODE);
            $nothiStatusLog->save();
        }
    }
}