<?php
namespace App\Orangebd;

class SiteInfo
{
    public function getInfo(){

        $logoPath = config('app.url').'img/logo.png';
        $imageType = pathinfo($logoPath, PATHINFO_EXTENSION);
        $tmpData = file_get_contents($logoPath);
        $imageData = 'data:image/' . $imageType . ';base64,' . base64_encode($tmpData);

        $bannerPath = config('app.url').'img/banner/banner.png';
        $imageType = pathinfo($bannerPath, PATHINFO_EXTENSION);
        $tmpData = file_get_contents($bannerPath);
        $imageBanner = 'data:image/' . $imageType . ';base64,' . base64_encode($tmpData);

        $siteInfo = array('name' => 'পল্লী উন্নয়ন ও সমবায় বিভাগ সেবা',
            'name_en' => 'Rural Development & Cooperative Division Services',
            'slogan' => '',
            'slogan_en' => '',
            'title' => 'পল্লী উন্নয়ন ও সমবায় বিভাগ সেবা | RDCD Services',
            'logo' => $imageData,
            'logo_banner' => $imageBanner,
            'theme_color'=>'#0d9146');

        return $siteInfo;
    }
}
