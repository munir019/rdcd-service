<?php

namespace App\Orangebd;

use App\Models\Application;
use App\Models\CitizenProfile;
use App\Models\Forms;
use App\Models\Service;
use App\Models\ServiceDecision;
use App\Models\ServiceIntegration;
use App\Models\WorkflowDecision;
use App\Models\WorkflowStep;
use App\Orangebd;

class DataProcess
{
    private $parentThis;
    private $urlPath;
    private $photo;
    private $signature;
    private $aid;

    public function getApplicationData($aid, $officers = null, $appData = null){

        $this->urlPath = config('app.url');
        $this->aid = $aid;

        $application = Application::where('aid',$aid)->get()->toArray();
        if(isset($application[0]))
            $application = $application[0];

        $sid = $application['sid'];
        $oid = $application['oid'];
        $fid = $application['fid'];

        //$aid = $id;
        //$applicationId = new Orangebd\ApplicationId();
        //$sid = $applicationId->getServiceId($aid);

        //$this->parentThis = $parentThis;

        //$this->parentThis->loadModel('ServiceLive');
        //$service = $this->parentThis->ServiceLive->getServiceById($sid);
        $service = Service::where('id',$sid)->get()->toArray();
        if(isset($service[0]))
            $service = $service[0];

        /*$this->parentThis->loadModel('ServiceDbConfig');
        $serviceDb = $this->parentThis->ServiceDbConfig->getDbByServiceId($sid);

        $appDb = 'mysql://'.$serviceDb['user'].':'.$serviceDb['password'].'@'.$serviceDb['host'].':'.$serviceDb['port'].'/'.$serviceDb['db'];
        $time = time();
        ConnectionManager::setConfig('app_db'.$time, ['url' => $appDb]);
        $appDb = ConnectionManager::get('app_db'.$time);
        $this->parentThis->loadModel('Application');
        $this->parentThis->Application->connection($appDb);

        $this->parentThis->Application->setCustomTable($sid);
        $message = $this->parentThis->Application->getApplicationByAid($aid);

        $this->parentThis->loadModel('ServiceLive');
        $service = $this->parentThis->ServiceLive->getServiceById($sid);*/

        $data = json_decode($application['data'],true);
        $attachment = json_decode($application['attachment'],true);


        //$this->parentThis->loadModel('ServiceForm');
        //$from = $this->parentThis->ServiceForm->getFormById($message['fid']);
        $from = Forms::where('id',$fid)->get()->toArray();
        if(isset($from[0]))
            $from = $from[0];

        //$this->parentThis->loadModel('ServiceIntegration');
        //$serviceIntegrationType = $this->parentThis->ServiceIntegration->getIntegrationTypeBySid($sid);

        $serviceIntegrationType = ServiceIntegration::where('sid',$sid)->get()->toArray();
        if(isset($serviceIntegrationType[0]))
            $serviceIntegrationType = $serviceIntegrationType[0];

        if(!empty($from['html_form']))
            $html = $this->removeGarbage($from['html_form']);
        else
            $html = '';

        $appData = json_decode($appData,true);

        $result = array();
        $result['tracking_id'] = $application['aid'];
        if(isset($appData['application_subject']))
            $result['application_subject']  = $service['name'].' ('.$appData['application_subject'].')'.' - '.$application['aid'];
        else
            $result['application_subject']  = $service['name'].' - '.$application['aid'];
        $result['office_id'] = /*'65';*/$application['oid'];
        $result['user_id'] = $application['uid'];
        if($application['user_type']==1){
            //$citizenDb = ConnectionManager::get('citizen_db');
            //$this->parentThis->loadModel('UsersProfile');
            //$this->parentThis->UsersProfile->connection($citizenDb);
            //$this->parentThis->UsersProfile->setCustomTable();
            //$u = $this->parentThis->UsersProfile->citizenProfileById($message['uid']);
            $u = CitizenProfile::where('id',$application['uid'])->get()->toArray();
            if(isset($u[0]))
                $u = $u[0];

            $result['applicant_name'] = $u['name'];
            $result['mobile_no'] = $u['code'].(int)$u['mobile'];
            $result['national_identity_no'] = $u['national_id_no'];
        }else if($application['user_type']==2){
            if(isset($data['name']))
                $result['applicant_name'] = $data['name'];
            else if(isset($data['name']))
                $result['applicant_name'] = $data['applicant_name'];
            else
                $result['applicant_name'] = 'Guest';
            $result['mobile_no'] = $data['mobile'];
            $result['national_identity_no'] = '';
        }else{
            if(isset($data['name']))
                $result['applicant_name'] = $data['name'];
            else if(isset($data['name']))
                $result['applicant_name'] = $data['applicant_name'];
            else
                $result['applicant_name'] = 'Guest';
            $result['mobile_no'] = $data['mobile'];
            $result['national_identity_no'] = '';
        }

        $application['submitted_date'] = date('d-m-Y H:i:s',strtotime($application['submitted_date']));

        $result['data'] = $this->processData($html,$data, $application);
        if(is_array($attachment)) {
            $attachment = array_values(array_filter($attachment));
            $result['attachments'] = $this->processAttachment($application['uid'], $application['user_type'], $attachment, $application['app_from'], $serviceIntegrationType);
        }else
            $result['attachments'] = '';

        //$this->parentThis->loadModel('ServiceGuide');
        //$temp = $this->parentThis->ServiceGuide->getGuideByServiceId($sid);
//echo $message['submitted_to'];
        $result['approximate_solve_date'] = $application['approximate_solve_date'];
        if(empty($officers)) {
            $result['desks'] = $application['submitted_to'];
            $result['fdesk'] = $application['submitted_to'];
            /*'213499';*/
        }else{
            $result['desks'] = $officers;
            $result['fdesk'] = $officers;
        }

        $result['feedback_url'] = $this->urlPath.'api/gas';
        $result['feedback_api_key'] = md5('orangebd'.$aid.'enothi');//'123ora#';
        $result['api_key'] = $result['feedback_api_key'];

        $decision = $this->processDecision($sid);
        unset($decision[1]);
        unset($decision[2]['decision'][2]);
        unset($decision[5]['decision'][13]);
        unset($decision[5]['decision'][14]);
        unset($decision[5]['decision'][15]);
        unset($decision[5]['decision'][16]);

        $result['decision'] = $decision;//json_encode($decision,JSON_UNESCAPED_UNICODE);
        /*$result['check_list'] = $temp['check_list'];
        $decision = $this->processDecision($sid);

        $result['redirectUrl'] = $this->urlPath.'api/receive_app_status';
        $result['param'] = array(
            'aid'=>'Application Id',
            'action'=>array(
                '1'=>'Application forward',
                '2'=>'Make the service editable to applicant',
                '3'=>'Ask for payment',
                '4'=>'Complete',
                '5'=>'archive'),
            'payment_amount'=>'Value in Integer, if only action is 2',
            'decision'=>$decision,
            'decision_note'=>'Optional'
        );
        $result['result'] = array(
            '1'=>'Message received and updated',
            '2'=>'Invalid message id',
            '3'=>'Invalid action',
            '4'=>'Invalid amount',
            '5'=>'Invalid decision',
            '6'=>'Database connection failed');*/

        if($this->signature!='')
            $result['data'] = str_replace('<div class="applicant_name"></div>','<img class="signature" src="'.$this->signature.'" />',$result['data']);
        //<div class="applicant_name">'.(isset($data['name'])?$data['name']:'').'</div>

        if($this->photo!='')
            $result['data'] = '<img class="photo" src="'.$this->photo.'"/>'.$result['data'];

        $result['data'] = '<div class="bsap relative">'.nl2br($result['data']).'</div>';
        return json_encode($result, JSON_UNESCAPED_UNICODE);
    }

    private function removeGarbage($html){
        $html = str_replace("\n","",$html);
        $html = str_replace("\r","",$html);
        return $html;
    }

    public function getApplicationView($application){

        $this->urlPath = config('app.url');

        $aid = $application->aid;

        /*$this->parentThis->loadModel('ServiceLive');
        $service = $this->parentThis->ServiceLive->getServiceById($sid);*/

        $service = Service::where('id',$application->sid)->get()->toArray();
        if(isset($service[0]))
            $service = $service[0];

        /*$this->parentThis->loadModel('ServiceDbConfig');
        $serviceDb = $this->parentThis->ServiceDbConfig->getDbByServiceId($sid);

        $appDb = 'mysql://'.$serviceDb['user'].':'.$serviceDb['password'].'@'.$serviceDb['host'].':'.$serviceDb['port'].'/'.$serviceDb['db'];
        $time = time();
        ConnectionManager::setConfig('app_db'.$time, ['url' => $appDb]);
        $appDb = ConnectionManager::get('app_db'.$time);
        $this->parentThis->loadModel('Application');
        $this->parentThis->Application->connection($appDb);

        $this->parentThis->Application->setCustomTable($sid);
        $application = $this->parentThis->Application->getApplicationByAid($aid);*/
        $data = json_decode($application->data,true);
        $attachment = json_decode($application->attachment);

        //$this->parentThis->loadModel('ServiceForm');
        //$from = $this->parentThis->ServiceForm->getFormById($application['fid']);

        $from = Forms::where('id',$application->fid)->get()->toArray();
        if(isset($from[0]))
            $from = $from[0];

        if(isset($from['html_form']))
            $html = $this->removeGarbage($from['html_form']);
        else
            $html = '';

        $result = array();
        $application->submitted_date = date('d-m-Y',strtotime($application->submitted_date));

        $result = $this->processData($html,$data, $application->toArray());

        return $result;
    }

    public function processDecision($sid){
        //$this->parentThis->loadModel('ServiceSteps');
        //$tmp = $this->parentThis->ServiceSteps->getStepBySid($sid);
        $tmp = ServiceDecision::where('sid',$sid)->get()->toArray();
        if(isset($tmp[0]))
            $tmp = $tmp[0];
        $step = json_decode($tmp['step'],true);

        //$this->parentThis->loadModel('ServiceStep');
        //$allStep = $this->parentThis->ServiceStep->getAllStep();
        $allStep = WorkflowStep::where('status',1)->get()->toArray();
        $tmp = array();
        foreach ($allStep as $v){
            $tmp[$v['id']] = $v;
        }
        $allStep = $tmp;

        //$this->parentThis->loadModel('ServiceDecisionSetting');
        //$decisions = $this->parentThis->ServiceDecisionSetting->getAllDecision();
        $decisions = WorkflowDecision::where('status',1)->get()->toArray();
        $tmp = array();
        foreach ($decisions as $v){
            $tmp[$v['id']] = $v;
        }
        $decisions = $tmp;

        $result = array();
        $dec = array();
        foreach ($step as $s){
            $decision = json_decode($allStep[$s]['decision'],true);
            $addStep = 0;
            foreach ($decision as $v){
                if(isset($decisions[$v]['name'])){
                    $dec[$s][$v] = $decisions[$v]['name'];
                    $addStep = 1;
                }
            }
            if($addStep == 1) {
                $result[$s]['step'] = $allStep[$s]['name'];
                $result[$s]['decision'] = $dec[$s];
            }
        }
        return $result;
    }

    public function processAttachment($uid,$user_type,$data,$appType, $serviceIntegrationType = null){
        $this->parentThis->loadModel('ServiceDocs');
        $docs = $this->parentThis->ServiceDocs->getAllDocs();

        $citizenDb = ConnectionManager::get('citizen_db');
        $this->parentThis->loadModel('UsersStorage');
        $this->parentThis->UsersStorage->connection($citizenDb);
        $this->parentThis->UsersStorage->setCustomTable();

        $att = array();
        $encrypt = new Orangebd\Encrypt();
        if($appType=='BSAP-DAK'){
            $i = 0;
            foreach ($data as $val) {
                $att[$i]['name'] = $val;
                $att[$i]['url'] = $this->urlPath . 'storage/' . $val;
                $i = $i + 1;
            }
        } else if($serviceIntegrationType['name'] == ServiceIntegrationType::JMSHOSTEL){
            $i = 0;
            foreach ($data as $val) {
                $tmp = $this->parentThis->UsersStorage->getStorageByFid($val,$uid,$user_type);
                $att[$i] = GetResizeImageActions::getImageArr($docs, $tmp, $this->urlPath);
                $urlPath = $this->urlPath . 'storage/institution/' . $encrypt->encodePath($tmp['uid']) . '/' . $tmp['file_name'];
                if($tmp['doc_id']==2)
                    $this->photo = $urlPath;
                if($tmp['doc_id']==7)
                    $this->signature = $urlPath;
                $i += 1;
            }
            
        } else {
            
            $i = 0;
            foreach ($data as $val) {
                $tmp = $this->parentThis->UsersStorage->getStorageByFid($val,$uid,$user_type);
                if(isset($tmp['display_name'])) {
                    $att[$i]['name'] = $docs[$tmp['doc_id']]['name'].'-'.$tmp['display_name'];
                    if ($tmp['user_type'] == 1) {
                        $att[$i]['url'] = $this->urlPath . 'storage/citizen/' . $encrypt->encodePath($tmp['uid']) . '/' . $tmp['file_name'];
                        if($tmp['doc_id']==2)
                            $this->photo = $att[$i]['url'];
                        if($tmp['doc_id']==7)
                            $this->signature = $att[$i]['url'];
                    }else
                        $att[$i]['url'] = $this->urlPath . 'storage/institution/' . $encrypt->encodePath($tmp['uid']) . '/' . $tmp['file_name'];
                    $i = $i + 1;
                }
            }
        }
        return $att;
    }

    public function innerHTML($node) {
        return implode(array_map([$node->ownerDocument,"saveHTML"],
            iterator_to_array($node->childNodes)));
    }

    function outerHTML($e) {
        $doc = new \DOMDocument();
        $doc->appendChild($doc->importNode($e, true));
        return $doc->saveHTML();
    }

    public function processData($html,$data,$applicationData=null){
        $applyDate = $applicationData['submitted_date'];

        if (empty($html))
            return 'No data';

        $nDoptor = new nDoptor();
        $doc = new \DOMDocument();
        libxml_use_internal_errors(true);

        $doc->loadHTML(mb_convert_encoding($html, 'HTML-ENTITIES', 'UTF-8'));
        $doc->preserveWhiteSpace = false;

        $items = $doc->getElementsByTagName('div');
        foreach ($items as $tag) {
            $grouId = $tag->getAttribute('data-group-id');
            if($grouId!=''){

                if(isset($data[$grouId])) {
                    $groupValue = $data[$grouId];
                    $groupValue = explode(',',$groupValue);
                    $groupValue = array_values(array_filter($groupValue));

                    $groupDiv = $this->nodeContent($tag);
                    $groupDiv = str_replace(array("\n", "\r"), '', $groupDiv);
                    $groupNewHtml = $groupDiv;

                    foreach($groupValue as $v){
                        $temp = $groupDiv;
                        $temp = str_replace('name="','name="'.$v,$temp);
                        $groupNewHtml .= $temp;
                    }
                    $html = str_replace($groupDiv,$groupNewHtml,$html);
                }
            }
        }

        $doc = new \DOMDocument();

        $doc->loadHTML(mb_convert_encoding($html, 'HTML-ENTITIES', 'UTF-8'));
        $doc->preserveWhiteSpace = false;

        $items = $doc->getElementsByTagName('input');

        if (count($items) > 0) {
            foreach ($items as $tag) {
                if ($tag->getAttribute('type') == 'hidden') {
                    $name = $tag->getAttribute('name');
                    if(strpos($name,'dsignature')!==false){
                        $value = (isset($data[$name])?$data[$name]:$tag->getAttribute('value'));

                        $html = preg_replace('#<input(.*?)name="' . $name . '"(.*?)>#', '<img src="'.$value.'"/>', $html);
                        $html = preg_replace('#<canvas(.*?)id="' . $name . '"(.*?)>#', '', $html);
                    }
                }else if ($tag->getAttribute('type') == 'radio') {
                    $name = $tag->getAttribute('name');

                    $value = $tag->getAttribute('value');
                    if ((isset($data[$name])) && $data[$name] == $value) {
                        $html = preg_replace('#<input(.*?)name="' . $name . '" type="radio" value="' . $value . '"(.*?)>#', '<i class="fa fa-circle"></i>', $html);
                    }
                    else
                        $html = preg_replace('#<input(.*?)name="' . $name . '" type="radio" value="'.$value.'"(.*?)>#', '<i class="fa fa-circle-o"></i>', $html);

                } else if ($tag->getAttribute('type') == 'checkbox') {
                    $name = $tag->getAttribute('name');
                    $value = $tag->getAttribute('value');

                    $name = str_replace('[', '', $name);
                    $name = str_replace(']', '', $name);

                    if ((isset($data[$name])) && (@in_array($value, $data[$name]) || $value == $data[$name])) {
                        $html = preg_replace('#<input(.*?)name="' . $name . '\[\]" type="checkbox" value="' . $value . '"(.*?)>#', '<i class="fa fa-check-square-o"></i>', $html);
                        $html = preg_replace('#<input(.*?)name="' . $name . '" type="checkbox" value="' . $value . '"(.*?)>#', '<i class="fa fa-check-square-o"></i>', $html);
                    } else {
                        $html = preg_replace('#<input(.*?)name="' . $name . '\[\]" type="checkbox" value="' . $value . '"(.*?)>#', '<i class="fa fa-square-o"></i>', $html);
                        $html = preg_replace('#<input(.*?)name="' . $name . '" type="checkbox" value="' . $value . '"(.*?)>#', '<i class="fa fa-square-o"></i>', $html);
                    }
                } else {
                    $name = $tag->getAttribute('name');

                    preg_match_all('/<input(((?!>).)*?)name="' . $name . '"(.*?)>/',$html,$content);
                    if(isset($content[0])) {
                        $content = $content[0];
                        foreach ($content as $k => $v) {
                            $html = str_replace($v, (isset($data[$name]) ? '<label>' . $data[$name] . '</label>' : ''), $html);
                        }
                    }
                    //$html = preg_replace('#<input(.*?)name="' . $name . '"(.*?)>#', (isset($data[$name]) ? '<label>' . $data[$name] . '</label>' : ''), $html);
                }
            }
        }

        $address = array('district', 'upazila', 'union', 'thana');
        $items = $doc->getElementsByTagName('select');
        if (count($items) > 0) {
            foreach ($items as $tag) {
                $static = 0;
                $name = $tag->getAttribute('name');
                $child = $tag->childNodes;
                if (isset($data[$name])) {
                    foreach ($address as $v) {
                        if (strpos($name, $v) > 0) {
                            if ($v == 'district')
                                $d = $nDoptor->data('district?id=' . $data[$name]);
                            //$d = implode(file('http://www.doptor.gov.bd:8090/gen/district?id='.$data[$name]));
                            else if (($v == 'upazila' && strpos($data[$name], 'U') > -1))
                                $d = $nDoptor->data('upazila?id=' . str_replace('U', '', $data[$name]));
                            //$d = implode(file('http://www.doptor.gov.bd:8090/gen/upazila?id='.str_replace('U','',$data[$name])));
                            else if ($v == 'upazila' && strpos($data[$name], 'C') > -1)
                                $d = $nDoptor->data('citycorporation?id=' . str_replace('C', '', $data[$name]));
                            //$d = implode(file('http://www.doptor.gov.bd:8090/gen/citycorporation?id='.str_replace('C','',$data[$name])));
                            else if ($v == 'union' && strpos($data[$name], 'U') > -1)
                                $d = $nDoptor->data('union?id=' . str_replace('U', '', $data[$name]));
                            //$d = implode(file('http://www.doptor.gov.bd:8090/gen/union?id='.str_replace('U','',$data[$name])));
                            else if ($v == 'union' && strpos($data[$name], 'M') > -1)
                                $d = $nDoptor->data('municipality?id=' . str_replace('M', '', $data[$name]));
                            //$d = implode(file('http://www.doptor.gov.bd:8090/gen/municipality?id='.str_replace('M','',$data[$name])));
                            else if ($v == 'thana')
                                $d = $nDoptor->data('thana?id=' . $data[$name]);
                            //$d = implode(file('http://www.doptor.gov.bd:8090/gen/thana?id='.$data[$name]));

                            $d = @json_decode($d,true);
                            if (is_array($d) && !empty($d)) {
                                $static = 1;
                                $d = $d[0];
                                $html = preg_replace('#<select(.*?)name="' . $name . '"(.*?)>(.*?)<\/select>#', '<label>' . $d['nameBn'] . '</label>', $html);
                            }
                        }
                    }

                    if (strpos($name, 'upazila-') > -1) {
                        $d = $nDoptor->data('upazila?id=' . $data[$name]);
                        $d = @json_decode($d,true);
                        if (is_array($d) && !empty($d)) {
                            $static = 1;
                            $d = $d[0];
                            $html = preg_replace('#<select(.*?)name="' . $name . '"(.*?)>(.*?)<\/select>#', '<label>' . $d['nameBn'] . '</label>', $html);
                        }
                    }

                    if($static == 0 && $name=='office_attention_desk'){
                        $d = $nDoptor->data('officeoriginunitorganogram?id=' . $data[$name]);
                        $d = @json_decode($d, true);
                        if (is_array($d) && !empty($d)) {
                            $static = 1;
                            $d = $d[0];
                            $html = preg_replace('#<select(.*?)name="' . $name . '"(.*?)>(.*?)<\/select>#', '<label>' . $d['nameBn'] . '</label>', $html);
                        }
                    }
                    if ($static == 0) {
                        if(isset($data[$name]))
                            $val = $data[$name];
                        foreach ($child as $item) {
                            if ($item->getAttribute('value') == $data[$name])
                                $val = $this->nodeContent($item);
                        }
                        if (isset($val)) {
                            $html = preg_replace('#<select(.*?)name="' . $name . '"(.*?)>(.*?)<\/select>#', (isset($data[$name]) ? '<label>' . $val . '</label>' : ''), $html);
                            $static = 1;
                        }
                    }
                    if ($static == 0) {
                        $html = preg_replace('#<select(.*?)name="' . $name . '"(.*?)>(.*?)<\/select>#', '', $html);
                    }
                } else {
                    foreach ($address as $v) {
                        if (strpos($name, $v) > 0) {
                            $par = $tag->parentNode;
                            $par = $par->parentNode;
                            $r = $this->nodeContent($par, true);
                            $r = str_replace(array("\r", "\n", '\r', '\n'), '', $r);
                            $html = str_replace($r, '', $html);
                        }
                    }
                }
            }
        }

        $items = $doc->getElementsByTagName('textarea');
        if (count($items) > 0) {
            foreach ($items as $tag) {
                $name = $tag->getAttribute('name');
                $html = preg_replace('#<textarea(.*?)name="' . $name . '"(.*?)>(.*?)<\/textarea>#', '<label>' . $data[$name] . '</label>', $html);
            }
        }

        if(isset($data['auto_to_whom']) && $data['auto_to_whom']!='')
            $html = preg_replace('#<label class="to_whom"(.*?)data-auto="true"(.*?)>(.*?)<\/label>#', '<label class="to_whom" data-auto="true">' . $data['auto_to_whom'] . '</label>', $html);
        if(isset($data['auto_office_name']) && $data['auto_office_name']!='') {
            $html = preg_replace('#<label class="office_name"(.*?)data-auto="true"(.*?)>(.*?)<\/label>#', '<label class="office_name" data-auto="true">' . $data['auto_office_name'] . '</label>', $html);
        }if(isset($data['auto_office_address']) && $data['auto_office_address']!='')
            $html = preg_replace('#<label class="office_address"(.*?)data-auto="true"(.*?)>(.*?)<\/label>#', '<label class="office_address" data-auto="true">'.$data['auto_office_address'].'</label>', $html);
        if(isset($data['office_attention_desk_unit']) && $data['office_attention_desk_unit']!='')
            $html = preg_replace('#<label class="office_attention_desk_unit"(.*?)>(.*?)<\/label>#', '<label class="office_attention_desk_unit" data-auto="true">'.$data['office_attention_desk_unit'].'</label>', $html);

        if ($applyDate != '') {
            $html = str_replace('<div>তারিখ : </div>', 'তারিখ : ' . $this->bngOnlyDate(strtotime($applyDate)), $html);
        }

        $html = preg_replace('#<span class="select2 select2-container select2-container--default"(.*?)>(.*?)</span>#', '', $html);
        $html = preg_replace('#<span class="select2-selection__arrow"(.*?)>(.*?)</span>#', '', $html);
        $html = preg_replace('#<span class="dropdown-wrapper"(.*?)>(.*?)</span>#', '', $html);

        $html = str_replace('fb-label','',$html);
        $html = str_replace('col-form-label-sm','',$html);
        $html = preg_replace('#<small(.*?)>(.*?)<\/small>#', '', $html);

        $html = str_replace('col-6','col-6 col-md-6',$html);
        $html = str_replace('col-4','col-4 col-md-4',$html);
        $html = str_replace('col-3','col-3 col-md-3',$html);

        $html = str_replace('<span obd="clear-dsignature">Clear</span>','',$html);
        //$html = $html.'<style>.bsap .relative{position: relative}.bsap .inline{ display:inline-block}.bsap .photo{ float:right;max-width: 100px;max-height: 100px;}.bsap .signature{ max-width: 200px; max-height: 50px}.bsap .submitted_by{ text-align: center}</style>';
        //$html = $html.'<style>@import "http://localhost/bsap-portal/css/external.bsap.css";</style>';
        $extraData = '';
        if(isset($this->aid) && $this->aid!='') {
            $extraData = '<div class="tracking-num">ট্রাকিং নং: ' . $this->aid . '</div>';
            if (isset($applyDate) && $applyDate != '')
                $extraData .= '<div class="message-date">তারিখ : ' . $this->bngDate(strtotime($applyDate)) . '</div>';
        }
        $tranData = '';
        if(isset($applicationData['tid']) && $applicationData['tid']!=''){
            $transactionDb = ConnectionManager::get('transaction');
            $this->parentThis->loadModel('Transaction');
            $this->parentThis->Transaction->connection($transactionDb);
            $this->parentThis->Transaction->setCustomTable();

            $transaction = $this->parentThis->Transaction->getInfoByTid($applicationData['tid']);
            $tranData = '<div>লেনদেন নাম্বার : ' . $applicationData['tid'] . '</div>';
            $transaction = json_decode($transaction,true);
            if(isset($transaction['CHALLANURL']))
                $tranData .= '<div>চালান দেখতে <a href="'.urldecode($transaction['CHALLANURL']).'" target="_blank">এখানে ক্লিক</a> করুন</div>';
        }

        //$html = '<link rel="stylesheet" href="https://'.$_SERVER['HTTP_HOST'].'/css/external.bsap.css?v=1">'.$extraData.$html.$tranData;
        $html = str_replace('col-sm-','col-md-',$html);
        $html = str_replace('col-md-2','col-md-2 col-xs-2 col-sm-2',$html);
        $html = str_replace('col-md-3','col-md-3 col-xs-3 col-sm-3',$html);
        $html = str_replace('col-md-4','col-md-4 col-xs-4 col-sm-4',$html);
        $html = str_replace('col-md-5','col-md-5 col-xs-5 col-sm-5',$html);
        $html = str_replace('col-md-6','col-md-6 col-xs-6 col-sm-6',$html);
        $html = str_replace('col-md-7','col-md-7 col-xs-7 col-sm-7',$html);
        $html = str_replace('col-md-8','col-md-8 col-xs-8 col-sm-8',$html);
        $html = str_replace('col-md-9','col-md-9 col-xs-9 col-sm-9',$html);
        $html = str_replace('col-md-10','col-md-10 col-xs-10 col-sm-10',$html);
        $html = str_replace('col-md-11','col-md-11 col-xs-11 col-sm-11',$html);
        $html = str_replace('col-md-12','col-md-12 col-xs-12 col-sm-12',$html);
        $html = str_replace('form-group','',$html);

        $html = $html.'<style>.bsap label{ display:block}.bsap .inline { display: inline-block; width: auto}.bsap .form-check-inline i { margin-right: 10px}.bsap .photo{ float:right;max-width: 100px;max-height: 100px}.bsap .signature{ max-width: 200px; max-height: 50px}.bsap .submitted_by{ text-align: center !important;}.bsap .submitted_date,.bsap .submitted_by{ padding-top:30px} table.table-III tr th,table.table-III tr td{ width: 33.3%} table.table-IV tr th,table.table-IV tr td{ width: 25%} table.table-V tr th, table.table-V tr td{ width: 20%} table.table-VI tr th, table.table-VI tr td{ width: 16.5%} table.table-VII tr th, table.table-VII tr td{ width: 14.1%} .fb-table-add-row, .fb-table-count-row, .fb-table-remove-row{ display: none}</style>';

        return $html;
    }

    public function nodeContent($n, $outer=false) {
        /*$d = new \DOMDocument();
        $b = $d->importNode($n->cloneNode(true),true);
        $d->appendChild($b); $h = $d->saveHTML();
        // remove outter tags
        if (!$outer) $h = substr($h,strpos($h,'>')+1,-(strlen($n->nodeName)+4));*/

        $innerHTML= '';
        $children = $n->childNodes;
        foreach ($children as $child) {
            $innerHTML .= $child->ownerDocument->saveHTML( $child );
        }
        $h = $innerHTML;
        //return $innerHTML;*/
        $h = $this->unicodeToUTF8($h);
        //return $h;
        return $h;
    }

    public function unicodeToUTF8($str){
        $unicodeToUtf8 = array(
            "&#2404;"=>"।",
            "&#2433;"=>"ঁ",
            "&#2434;"=>"ং",
            "&#2435;"=>"ঃ",
            "&#2437;"=>"অ",
            "&#2438;"=>"আ",
            "&#2439;"=>"ই",
            "&#2440;"=>"ঈ",
            "&#2441;"=>"উ",
            "&#2442;"=>"ঊ",
            "&#2443;"=>"ঋ",
            "&#2444;"=>"ঌ",
            "&#2447;"=>"এ",
            "&#2448;"=>"ঐ",
            "&#2451;"=>"ও",
            "&#2452;"=>"ঔ",
            "&#2453;"=>"ক",
            "&#2454;"=>"খ",
            "&#2455;"=>"গ",
            "&#2456;"=>"ঘ",
            "&#2457;"=>"ঙ",
            "&#2458;"=>"চ",
            "&#2459;"=>"ছ",
            "&#2460;"=>"জ",
            "&#2461;"=>"ঝ",
            "&#2462;"=>"ঞ",
            "&#2463;"=>"ট",
            "&#2464;"=>"ঠ",
            "&#2465;"=>"ড",
            "&#2466;"=>"ঢ",
            "&#2467;"=>"ণ",
            "&#2468;"=>"ত",
            "&#2469;"=>"থ",
            "&#2470;"=>"দ",
            "&#2471;"=>"ধ",
            "&#2472;"=>"ন",
            "&#2474;"=>"প",
            "&#2475;"=>"ফ",
            "&#2476;"=>"ব",
            "&#2477;"=>"ভ",
            "&#2478;"=>"ম",
            "&#2479;"=>"য",
            "&#2480;"=>"র",
            "&#2482;"=>"ল",
            "&#2486;"=>"শ",
            "&#2487;"=>"ষ",
            "&#2488;"=>"স",
            "&#2489;"=>"হ",
            "&#2492;"=>"়",
            "&#2493;"=>"ঽ",
            "&#2494;"=>"া",
            "&#2495;"=>"ি",
            "&#2496;"=>"ী",
            "&#2497;"=>"ু",
            "&#2498;"=>"ূ",
            "&#2499;"=>"ৃ",
            "&#2500;"=>"ৄ",
            "&#2503;"=>"ে",
            "&#2504;"=>"ৈ",
            "&#2507;"=>"ো",
            "&#2508;"=>"ৌ",
            "&#2509;"=>"্",
            "&#2510;"=>"ৎ",
            "&#2519;"=>"ৗ",
            "&#2524;"=>"ড়",
            "&#2525;"=>"ঢ়",
            "&#2527;"=>"য়",
            "&#2528;"=>"ৠ",
            "&#2529;"=>"ৡ",
            "&#2530;"=>"ৢ",
            "&#2531;"=>"ৣ",
            "&#2534;"=>"০",
            "&#2535;"=>"১",
            "&#2536;"=>"২",
            "&#2537;"=>"৩",
            "&#2538;"=>"৪",
            "&#2539;"=>"৫",
            "&#2540;"=>"৬",
            "&#2541;"=>"৭",
            "&#2542;"=>"৮",
            "&#2543;"=>"৯",
            "&#2544;"=>"ৰ",
            "&#2545;"=>"ৱ",
            "&#2546;"=>"৲",
            "&#2547;"=>"৳",
            "&#2548;"=>"৴",
            "&#2549;"=>"৵",
            "&#2550;"=>"৶",
            "&#2551;"=>"৷",
            "&#2552;"=>"৸",
            "&#2553;"=>"৹",
            "&#2554;"=>"৺",
            "&#2555;"=>"৻"
        );

        foreach ($unicodeToUtf8 as $k=>$v)
            $str = str_replace($k,$v,$str);

        return $str;
    }

    public function engToBngNum($num){
        $num =str_replace('0','০',$num);
        $num =str_replace('1','১',$num);
        $num =str_replace('2','২',$num);
        $num =str_replace('3','৩',$num);
        $num =str_replace('4','৪',$num);
        $num =str_replace('5','৫',$num);
        $num =str_replace('6','৬',$num);
        $num =str_replace('7','৭',$num);
        $num =str_replace('8','৮',$num);
        $num =str_replace('9','৯',$num);

        return $num;
    }

    public function bngDate($time){
        if(!is_numeric($time))
            return $this->engToBngNum(date('d-m-Y ইং H:i',strtotime($time)));
        else
            return $this->engToBngNum(date('d-m-Y ইং H:i',$time));
    }

    public function bngOnlyDate($time){
        if(!is_numeric($time))
            return $this->engToBngNum(date('d-m-Y ইং',strtotime($time)));
        else
            return $this->engToBngNum(date('d-m-Y ইং',$time));
    }
}