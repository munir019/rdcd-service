<?php

namespace App\Orangebd;

use App\Models\CalHoliday;
use App\Models\CalWeeklyHoliday;
use Cake\Core\Configure;
use Cake\Datasource\ConnectionManager;

class Calendar
{
    public function nextWorkingDay($days){

        //$this->parentThis->loadModel('CalendarWeekly');
        $weeklyHoliday = CalWeeklyHoliday::get()->toArray();// $this->parentThis->CalendarWeekly->getData();

        //$this->parentThis->loadModel('CalendarHoliday');
        $holiday = CalHoliday::get()->toArray();// $this->parentThis->CalendarHoliday->getDataBy2Year(date('Y'));

        $holidays = array();
        foreach ($holiday as $k=>$v){
            $startDate = $v['start_date'];
            $endDate = $v['end_date'];
            if($startDate==$endDate)
                $holidays[$startDate] = $v['title'];
            else{
                $startDateOrigin = $startDate;
                $startDate = date_create($startDate);
                $endDate = date_create($endDate);
                $dateDiff = date_diff($endDate,$startDate);
                for($i=0;$i<=$dateDiff->days;$i++){
                    $tempDate = date('Y-m-d',strtotime('+'.$i.' day',strtotime($startDateOrigin)));
                    $holidays[$tempDate] = $v['title'];
                }
            }
        }

        $daysName = array('1'=>'Sun','2'=>'Mon','3'=>'Tue','4'=>'Wed','5'=>'Thu','6'=>'Fri','7'=>'Sat');

        $today = date('Y-m-d',strtotime('+1 day',time()));
        $workingDay = 0;
        $approximateDate = '';
        for ($i=0;$i<100;$i++){
            $dayName = date("D", strtotime(date($today)));
            if(!in_array(array_search($dayName,$daysName),$weeklyHoliday) && !in_array($today,$holidays))
                $workingDay = $workingDay + 1;

            if($workingDay==$days) {
                $approximateDate = $today;
                break;
            }
            $today = date('Y-m-d',strtotime('+1 day',strtotime($today)));
        }
        return $approximateDate;
    }
}