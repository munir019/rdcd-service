<?php

namespace App\Http\Controllers;

use App\Models\CitizenStorage;
use DB;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\Storage;

class AttachmentController extends Controller
{
    public function index(Request $request)
    {


        exit();
    }

    public function upload(Request $request)
    {
        try {
            $file = $request->file('upload_file');
            $fileExt = $request->file('upload_file')->getClientOriginalExtension();
            $fileSize = $request->file('upload_file')->getSize();
            $fileName = time().'.'.$fileExt;
            $fileId = md5(time().session()->get('user')['id'].session()->get('user')['user_type'].uniqid());

            $path = 'citizen/'.date('Y/m/d').'/'.md5(session()->get('user')['id']).$fileName;

            Storage::disk('storage')->put($path, file_get_contents($file));

            $storage = new CitizenStorage();
            $storage->uid = session()->get('user')['id'];
            $storage->user_type = session()->get('user')['user_type'];
            $storage->doc_id = 0;
            $storage->fid = $fileId;
            $storage->file_name = $fileName;
            $storage->file_size = $fileSize;
            $storage->display_name = '';
            $storage->upload_on = date('Y-m-d H:i:s');
            $storage->status = 1;
            $storage->file_path = $path;
            $storage->save();

            return json_encode(array('status'=>'success','fid'=>$fileId));
        }catch(\Exception $ex){

        }

        return json_encode(array('status'=>'failed'));
    }
}