<?php

namespace App\Http\Controllers;

use App\Models\Service;
use App\Models\ServiceOffice;
use App\Models\ServiceRecipient;
use App\Models\ServiceSector;
use App\Orangebd\GeoInfo;
use App\Orangebd\OfficeInfo;
use App\Orangebd\ServiceInfo;
use DB;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\Storage;

class CronController extends Controller
{
    public function index(Request $request)
    {
        $office = new OfficeInfo();

        if ($request->type == 'geo' || $request->type == 'all')
            $geo = new GeoInfo();

        if ($request->type == 'office' || $request->type == 'all') {
            $office = new OfficeInfo();
            $office->cronOffice();
        }

        if ($request->type == 'service' || $request->type == 'all'){
            $serviceInfo = new ServiceInfo();
            $serviceInfo->cronServiceList();
        }

        exit();
    }
}