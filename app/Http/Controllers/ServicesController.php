<?php

namespace App\Http\Controllers;

use App\Models\Forms;
use App\Models\Service;
use App\Models\ServiceCitizenCharter;
use App\Models\ServiceOffice;
use App\Orangebd\OfficeInfo;
use App\Orangebd\ServiceInfo;
use DB;
use Illuminate\Http\Request;
use App\Http\Requests;

class ServicesController extends Controller
{
    public function index(Request $request){
        $title = 'Service List';
        //dd($request->all());
        if(isset($request->id)){
            header('Location:'.config('app.url').'services/info?id='.$request->id);
            exit();
        }

        $serviceInfo = new ServiceInfo();
        $officeWiseServices = json_decode(json_encode($serviceInfo->getRdcdRapidServiceList()));

        //Get Office Service
        $serviceByCat = [];
        $officeId = $request->office;
        foreach($officeWiseServices as $office){
            if($office->office_id == $request->office){
                $services = $office->services;

                foreach($services as $service){
                    if($service->service_type == 'g2c'){
                        $serviceByCat['g2c'][] = $service;
                    }else{
                        $serviceByCat['g2g'][] = $service;
                    }
                }
            }
        }
        return view('service.index', compact(['title','officeWiseServices', 'serviceByCat', 'officeId']));
    }

    public function getServiceListByDoptor($officeId)
    {
        $serviceInfo = new ServiceInfo();
        $officeWiseServices = json_decode(json_encode($serviceInfo->getRdcdRapidServiceList()));

        //Get Office Service
        $serviceByCat = [];
        foreach($officeWiseServices as $office){
            if($office->office_id == $officeId){
                $services = $office->services;

                foreach($services as $service){
                    if($service->service_type == 'g2c'){
                        $serviceByCat['g2c'][] = $service;
                    }else{
                        $serviceByCat['g2g'][] = $service;
                    }
                }
            }
        }
        $serviceListHTML = view('service.office-service-list', compact('serviceByCat', 'officeId'))->render();

        return response()->json(compact('serviceListHTML'));
    }



    public function indexbackup(Request $request)
    {
        $title = 'Service List';

        if(isset($request->id)){
            header('Location:'.config('app.url').'services/info?id='.$request->id);
            exit();
        }

        $officeInfo = new OfficeInfo();
        $serviceInfo = new ServiceInfo();
        $serviceCategory = $serviceInfo->getServiceCategoryInfo();

        $category = '';
        if(isset($request->category))
            $category = $request->category;

        $data = '';
        $origin = '';
        $level3 = '';
        $services = array();
        $officeLevel = array();
        if(isset($request->category) && $request->category=='office' && isset($request->data)) {
            $officeLevel = $officeInfo->getOfficeOriginByLevel($request->data);
            $defaultId = array_key_first($officeLevel);
            if($request->data=='office-level-1' && isset($request->origin))
                $services = $serviceInfo->getServices('ministry', $request->origin);
            else if($request->data=='office-level-1')
                $services = $serviceInfo->getServices('ministry', $defaultId);
            else if($request->data!='office-level-1' && isset($request->origin))
                $services = $serviceInfo->getServices('origin', $request->origin);
            else if($request->data!='office-level-1')
                $services = $serviceInfo->getServices('origin', $defaultId);

            if(isset($request->origin))
                $origin = $request->origin;
            else $origin = $defaultId;
            $data = $request->data;
        }else {
            if (isset($request->category) && isset($request->data))
                $services = $serviceInfo->getServices($request->category, $request->data);

            $data = $request->data;
        }

        if($request->level3)
            $level3 = $request->level3;

        return view('service.index', compact(['title','serviceCategory','category','data','origin','officeLevel','level3','services']));
    }

    public function info(Request $request){

        $title = '';

        /*$service = Service::where('sid',$request->id)->where('status', 1)->where('onlive', 1)->firstOrFail()->toArray();
        $serviceCitizen = ServiceCitizenCharter::where('sid', $service['id'])->first()->toArray();
        $title = $service['name'];*/

        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => config('services.mygov.service_guide_url'),
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => array('key' => 'AEC759B2196510EBAE2FF3B768B06C03','sid' => $request->id),
        ));
        $response = curl_exec($curl);
        curl_close($curl);
        $response = json_decode($response, true);

        if($response['status']==1){
            $service = $response['data'];
            $title = $service['name'];
            $service['recipient'] = $service['type'];
            $service['office'] = $request->office;
            $serviceCitizen = $response['guide'];
        }

        //Only for Co-operative Society & Loan Module
        $cooperativeSocietyAppUrl = '';
        if (session()->has('citizen')) {
            $data = session()->get('citizen');
            $key = 'er@rdcd@t0ken';
            $data->photo = '';
            $token = JWT::encode($data, $key, 'HS256');
            $cooperativeSocietyAppUrl = 'http://coop.rdcd.erainfotechbd.com?token=' . $token;
        }

        return view('service.info', compact(['title','service','serviceCitizen', 'cooperativeSocietyAppUrl']));
    }

    public function form(Request $request){

        $title = '';
        $service = Service::where('sid',$request->id)->where('status', 1)->where('onlive', 1)->firstOrFail()->toArray();
        $serviceCitizen = ServiceCitizenCharter::where('sid', $service['id'])->firstOrFail()->toArray();
        $forms = Forms::where('sid', $service['id'])->where('form_type', 1)->where('status', 1)->firstOrFail()->toArray();

        /*Service Location*/
        $serviceOffice = ServiceOffice::where('sid', $service['id'])->where('status', 1)->get()->toArray();

        $officeIds = array();
        foreach ($serviceOffice as $v){
            $officeIds[$v['office_id']] = $v['office_id'];
        }
        $office = new OfficeInfo();
        $getFullOfficeLayer = $office->getFullOfficeLayer();

        foreach($getFullOfficeLayer as $keys=>$value){
            if($keys=='ministryOffices' || $keys=='doptorOfffices') {
                foreach ($value as $key => $val) {
                    if (!in_array($val['id'], $officeIds))
                        unset($getFullOfficeLayer[$keys][$key]);
                }
            }else if($keys=='otherOffices' || $keys=='divisionOffices' || $keys=='districtOffices' || $keys=='localOffices'){
                foreach ($value as $key => $val) {
                    if(isset($val['offices'])) {
                        foreach ($val['offices'] as $k => $v) {
                            if (!in_array($v['id'], $officeIds))
                                unset($getFullOfficeLayer[$keys][$key]['offices'][$k]);
                        }
                        if(count($getFullOfficeLayer[$keys][$key]['offices'])==0)
                            unset($getFullOfficeLayer[$keys][$key]);
                    }else
                        unset($getFullOfficeLayer[$keys][$key]);
                }
            }else if($keys=='upazilaOffices'){
                foreach ($value as $key => $val) {
                    if(isset($val['offices'])) {
                        foreach ($val['offices'] as $k => $v) {
                            if (!in_array($v['id'], $officeIds))
                                unset($getFullOfficeLayer[$keys][$key]['offices'][$k]);
                        }
                        if(count($getFullOfficeLayer[$keys][$key]['offices'])==0)
                            unset($getFullOfficeLayer[$keys][$key]);
                    }else
                        unset($getFullOfficeLayer[$keys][$key]);
                }
            }
            if(count($getFullOfficeLayer[$keys])==0)
                unset($getFullOfficeLayer[$keys]);
        }
        //$getFullOfficeLayer = json_encode($getFullOfficeLayer, JSON_UNESCAPED_UNICODE);
        /*Service Location*/

        $title = $service['name'];

        return view('application.modify', compact(['title','service','forms','serviceCitizen','getFullOfficeLayer']));
    }

    public function getRdcdService(){
        $searchtext = trim(request()->get('searchtext'));
        $q = '%'. $searchtext .'%';

        // $services = Service::with(['serviceOffice', 'serviceSector', 'serviceRecipient'])
        //                     ->where('status', 1)
        //                     ->where('onlive', 1)
        //                     ->where(function($query) use ($q) {
        //                         $query->where('name', 'like', $q)
        //                               ->orWhere('name_en', 'like', $q);
        //                     })
        //                     ->get();

        $serviceInfo = new ServiceInfo();
        $officeWiseServices = json_decode(json_encode($serviceInfo->getRdcdRapidServiceList()));
        $serviceList = $serviceInfo->getRapidUniqueServices($officeWiseServices);

        $services = [];
        if(!empty($serviceList)){
            foreach($serviceList as $key=>$val){
                if($val['keyword']){
                    $pattern = explode(',', trim($val['keyword']));
                    if(isset($pattern) && ($pattern[count($pattern) - 1] == '')){
                        unset($pattern[count($pattern) - 1]);
                    }

                    // $search_for = '/';
                    // foreach($pattern as $val){
                    //     $search_for .= '('.$val.')';
                    // }
                    // $search_for .= '/';
                }

                if(preg_match($q, $val['name'], $matches)){
                    $services[] = $val;
                }
            }
        }

        return view('home.search', compact(['services', 'searchtext']));
    }

}
