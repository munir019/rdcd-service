<?php

namespace App\Http\Controllers;

use App\Models\ApplicationLog;
use App\Models\LogSetting;
use App\Models\Service;
use App\Models\ServiceRecipient;
use App\Models\ServiceSector;
use App\Models\WorkflowDecision;
use App\Orangebd\ServiceInfo;
use DB;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\Storage;

class DashboardController extends Controller
{
    public function index(Request $request)
    {
        $title = 'ড্যাশবোর্ড';

        $user = session()->get('user');

        $applicationLogObj = ApplicationLog::where('uid',$user['id'])->orderBy('id','desc')->paginate((isset($request->limit)?$request->limit:20));
        $applicationLog = $applicationLogObj->toArray();

        $log = LogSetting::get()->toArray();
        $tmp = array();
        foreach($log as $v)
            $tmp['L'.$v['id']] = $v;

        $workflowDecision = WorkflowDecision::get()->toArray();

        foreach($workflowDecision as $v)
            $tmp[$v['id']] = $v;
        $workflowDecision = $tmp;

        if(!empty(session()->get('message')))
            $message = session()->get('message');
        else $message = array();

        return view('dashboard.index', compact(['title','applicationLogObj','applicationLog','workflowDecision','message']));
    }
}
