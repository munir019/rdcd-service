<?php

namespace App\Http\Controllers;

use App\Models\Service;
use App\Models\ServiceRecipient;
use App\Models\ServiceSector;
use App\Models\ServiceOffice;
use App\Orangebd\ServiceInfo;
use DB;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\App;

class HomeController extends Controller
{
    public function index(Request $request)
    {
        $title = 'আবেদনের জন্য দপ্তর ভিত্তিক সেবা নির্বাচন';

        //$serviceInfo = new ServiceInfo();
        //$serviceCategory = $serviceInfo->getServiceCategoryInfo();

        //$rdcdOffices = $this->getRdcdOffices();

        // $officeServices = collect($rdcdOffices)->countBy('office_id');
        // $rdcdOffices = collect($rdcdOffices)->unique('office_id');

        $serviceInfo = new ServiceInfo();
        $officeWiseServices = json_decode(json_encode($serviceInfo->getRdcdRapidServiceList()));

        $user = [];
        if(session()->has('citizen')){
            $user = session()->get('citizen');
        }

        if(session()->has('employee')){
            $user = session()->get('employee');

            if($user->office_id){
                return redirect(config('app.url').'services?category=office&office='.$user->office_id);
            }
        }

        return view('home.index', compact(['title', 'user', 'officeWiseServices']));
    }

    public function getRdcdOffices(){
        $rdcd_offices = ServiceOffice::select(['sid', 'office_id', 'office_name as name_bn', 'name_en', 'office_layer', 'geo_ministry'])
                                        ->where('geo_ministry', 46)
                                        ->where('status', 1)
                                        //->groupBy('office_id')
                                        ->orderBy('office_layer')
                                        ->get();
        return $rdcd_offices;
    }

    public function privacyPolicy()
    {
        return view('home.privacy-policy');
    }

    public function changeLanguage($lang){
        session(['locale' => $lang]);
        App::setLocale($lang);

        return redirect(url()->previous());
    }
}
