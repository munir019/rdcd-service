<?php

namespace App\Http\Controllers;

use DB;
use App\Models\Forms;
use App\Http\Requests;
use App\Models\Service;
use App\Orangebd\Nothi;
use App\Orangebd\Queue;
use App\Orangebd\nDoptor;
use App\Models\LogSetting;
use App\Orangebd\Calendar;
use App\Orangebd\Workflow;
use App\Models\Application;
use App\Models\NothiStatus;
use App\Models\WorkflowStep;
use App\Orangebd\OfficeInfo;
use Illuminate\Http\Request;
use App\Models\ServiceOffice;
use App\Models\ServiceSector;
use App\Orangebd\DataProcess;
use App\Orangebd\ServiceInfo;
use App\Models\ApplicationLog;
use App\Models\ServicePayment;
use App\Models\WorkflowAction;
use App\Models\ServiceDecision;
use App\Models\ServiceRecipient;
use App\Models\WorkflowDecision;
use App\Models\ServiceIntegration;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Http;
use App\Models\ServiceCitizenCharter;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class ApplicationController extends Controller
{
    private $autoSubmit = 0;

    public function index(Request $request)
    {
        $title = '';

        return view('home.index', compact(['title']));
    }

    public function getStatus(){
        return view('application.status');
    }

    public function applicationStatus(){

        $rules = [
            'mobile' => 'required|numeric',
            'application_number' => 'required|numeric',
        ];

        $messages = [
            'mobile.required' => 'Mobile is required',
            'application_number.required' => 'Application number is required'
        ];

        $validatedData = Validator::make(request()->all(), $rules, $messages);

        if($validatedData->fails()) {
            return redirect()->back()->withErrors($validatedData->messages())->withInput();
        }

        $response = [];
        $application_number = request()->get('application_number');
        $mobile = request()->get('mobile');
        if($application_number && $mobile){
            // $response = Http::post('http://api.training.mygov.bd/tracking', [
            //     'key' => 'AEC759B2196510EBAE2FF3B768B06C03',
            //     'aid' => 106,
            //     'mobile' => '01717303200',
            //     'limit' => 10
            // ]);

            $curl = curl_init();
            curl_setopt_array($curl, array(
                CURLOPT_URL => 'http://api.training.mygov.bd/tracking',
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => '',
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 0,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => 'POST',
                CURLOPT_POSTFIELDS => array('key' => 'AEC759B2196510EBAE2FF3B768B06C03','aid' => $application_number,'mobile' => $mobile),
            ));

            $response = curl_exec($curl);
            curl_close($curl);
            $response = json_decode($response, true);
            //dd($response);
            if($response['status'] == 1){
                return view('application.status', compact('response', 'application_number', 'mobile'));
            }
        }
        return view('application.status');
    }

    public function status(Request $request){

        $title = '';

        if(isset($request->aid)) {
            $aid = $request->aid;
            $applicationLog = ApplicationLog::where('aid', $aid)->get()->toArray();
            if (isset($applicationLog[0]))
                $applicationLog = $applicationLog[0];
            $sid = $applicationLog['sid'];

            $log = LogSetting::get()->toArray();
            $tmp = array();
            foreach($log as $v)
                $tmp['L'.$v['id']] = $v;

            $workflowDecision = WorkflowDecision::get()->toArray();

            foreach($workflowDecision as $v)
                $tmp[$v['id']] = $v;
            $workflowDecision = $tmp;

            $workflowStep = WorkflowStep::get()->toArray();
            $tmp = array();
            foreach($workflowStep as $v)
                $tmp[$v['id']] = $v;
            $workflowStep = $tmp;

            $workflowAction = WorkflowAction::get()->toArray();
            $tmp = array();
            foreach($workflowAction as $v)
                $tmp[$v['id']] = $v;
            $workflowAction = $tmp;

            $serviceDecision = ServiceDecision::where('sid',$sid)->get()->toArray();

            if(isset($serviceDecision[0]))
                $serviceDecision = $serviceDecision[0];
        }else
            return $this->invalidApplication();

        return view('application.status',compact(['title','applicationLog','serviceDecision','workflowDecision','workflowStep','workflowAction']));
    }

    public function view(Request $request){
        return $this->loadApplicationForViewAndModify($request, 'view');
    }

    public function modify(Request $request){
        return $this->loadApplicationForViewAndModify($request, 'modify');
    }

    private function loadApplicationForViewAndModify(Request $request, $type = 'view'){
        if(isset($request->aid)){
            $aid = $request->aid;
            $application = Application::where('aid',$aid)->get()->toArray();
            if(isset($application[0]))
                $application = $application[0];
            $application = Application::find($application['id']);

            $appData = new DataProcess();
            $data = $appData->getApplicationView($application);

            $service = Service::where('id',$application->sid)->where('status',1)->where('onlive',1)->limit(1)->get()->toArray();

            if(isset($service[0]))
                $service = $service[0];

            $serviceCitizen = ServiceCitizenCharter::where('sid',$service['id'])->get()->toArray();
            if(isset($serviceCitizen[0]))
                $serviceCitizen = $serviceCitizen[0];

            $forms = Forms::where('sid',$service['id'])->where('form_type',1)->where('status',1)->get()->toArray();
            if(isset($forms[0]))
                $forms = $forms[0];

            /*Service Location*/
            $serviceOffice = ServiceOffice::where('sid',$service['id'])->where('status',1)->get()->toArray();

            $officeIds = array();
            foreach ($serviceOffice as $v){
                $officeIds[$v['office_id']] = $v['office_id'];
            }
            $office = new OfficeInfo();
            $getFullOfficeLayer = $office->getFullOfficeLayer();

            foreach($getFullOfficeLayer as $keys=>$value){
                if($keys=='ministryOffices' || $keys=='doptorOfffices') {
                    foreach ($value as $key => $val) {
                        if (!in_array($val['id'], $officeIds))
                            unset($getFullOfficeLayer[$keys][$key]);
                    }
                }else if($keys=='otherOffices' || $keys=='divisionOffices' || $keys=='districtOffices' || $keys=='localOffices'){
                    foreach ($value as $key => $val) {
                        if(isset($val['offices'])) {
                            foreach ($val['offices'] as $k => $v) {
                                if (!in_array($v['id'], $officeIds))
                                    unset($getFullOfficeLayer[$keys][$key]['offices'][$k]);
                            }
                            if(count($getFullOfficeLayer[$keys][$key]['offices'])==0)
                                unset($getFullOfficeLayer[$keys][$key]);
                        }else
                            unset($getFullOfficeLayer[$keys][$key]);
                    }
                }else if($keys=='upazilaOffices'){
                    foreach ($value as $key => $val) {
                        if(isset($val['offices'])) {
                            foreach ($val['offices'] as $k => $v) {
                                if (!in_array($v['id'], $officeIds))
                                    unset($getFullOfficeLayer[$keys][$key]['offices'][$k]);
                            }
                            if(count($getFullOfficeLayer[$keys][$key]['offices'])==0)
                                unset($getFullOfficeLayer[$keys][$key]);
                        }else
                            unset($getFullOfficeLayer[$keys][$key]);
                    }
                }
                if(count($getFullOfficeLayer[$keys])==0)
                    unset($getFullOfficeLayer[$keys]);
            }
            /*Service Location*/

            $title = $service['name'];

            if($type == 'view')
                return view('application.view',compact(['title','service','forms','application','data','serviceCitizen','getFullOfficeLayer']));
            else
                return view('application.modify',compact(['title','service','forms','application','data','serviceCitizen','getFullOfficeLayer']));
        }
        return $this->invalidApplication();
    }

    public function apply(Request $request){

        if(isset($request->aid)){
            return $this->update($request);
        }

        $user = session()->get('user');

        $app_from = 'myGov';
        $uid = $user['id'];
        $user_type = $user['user_type'];

        $service = Service::where('sid',$request->id)->where('status',1)->where('onlive',1)->limit(1)->get()->toArray();
        $sid = '';
        if(isset($service[0])) {
            $service = $service[0];
            $service_name = $service['name'];
            $sid = $service['id'];
        }

        $oid = $request->office_id;

        $form = Forms::select('id')->where('sid',$sid)->where('form_type',1)->where('status',1)->limit(1)->get()->toArray();
        $fid = '';
        if(isset($form[0])) {
            $fid = $form[0]['id'];
        }

        $values = $request->all();
        unset($values['_token']);

        $data = json_encode($values,JSON_UNESCAPED_UNICODE);

        /*Application type*/
        $applicant_type = 0;
        if(isset($request->applicant_type))
            $applicant_type = $request->applicant_type;

        /*Application date*/
        $application_date = date('Y-m-d H:i:s');
        $is_submitted = 0;
        $is_complete = 0;

        /*If service has payment amount*/
        $amount = 0;
        $payment_type = 0;
        $has_payment = 0;
        if($service['payment_gateway']==0) {

            $payment = ServicePayment::where('sid',$sid)->get()->toArray();
            if(isset($payment[0])) {
                $payment = $payment[0];
                $has_payment = $payment['has_payment'];
                $payment_type = $payment['payment_type'];
                $amount = $payment['amount'];
            }
            if ($request->payment_amount)
                $amount = $request->payment_amount;
        }

        /*Get message id*/
        $countAid = Application::select(DB::raw('count(aid) as aid'))->where('oid',$oid)->where('sid',$sid)->whereDate('application_date',date('Y-m-d',strtotime($application_date)))->get()->toArray();
        if(!empty($countAid[0]['aid']))
            $countAid = (int)$countAid[0]['aid'] + 1;
        else $countAid = 1;

        $tempSid = $sid;
        for ($i = strlen($tempSid); $i < 6; $i++)
            $tempSid = '0'.$tempSid;

        $tempOid = $oid;
        for ($i = strlen($tempOid); $i < 7; $i++)
            $tempOid = '0'.$tempOid;

        $aid = $tempSid .'.'. $tempOid.'.' . date('Ymd') . '.'.$countAid;

        $app_id = '';
        $tid = '';
        $app_data = '';
        $attachment = '';
        /*Attachment*/
        if(isset($request->attachment))
            $attachment = json_encode($request->attachment,JSON_UNESCAPED_UNICODE);
        /*Own Application*/
        if($user_type==1){
            $applicant_mobile = $user['mobile'];
        }else if($user_type==3){

        }

        /*Get uaid*/
        $countUaid = ApplicationLog::select(DB::raw('max(uaid) as total'))->where('uid',$uid)->where('user_type',$user_type)->get()->toArray();
        if(!empty($countUaid[0]['total']))
            $uaid = (int)$countUaid[0]['total'] + 1;
        else $uaid = 100;

        $application = new Application();
        $application->aid = $aid;
        $application->uaid = $uaid;
        $application->app_from = $app_from;
        $application->app_id = $app_id;
        $application->app_data = $app_data;
        $application->uid = $uid;
        $application->user_type = $user_type;
        $application->applicant_type = $applicant_type;
        $application->applicant_mobile = $applicant_mobile;
        $application->sid = $sid;
        $application->oid = $oid;
        $application->fid = $fid;
        $application->tid = $tid;
        $application->data = $data;
        $application->attachment = $attachment;
        $application->is_complete = $is_complete;
        $application->application_date = $application_date;
        $application->payment_type = $payment_type;
        $application->has_payment = $has_payment;
        $application->amount = $amount;
        $application->is_submitted = $is_submitted;
        //$message->submitted_date = $submitted_date;
        //$message->re_submitted = $re_submitted;
        //$message->re_submitted_date = $re_submitted_date;
        //$message->submitted_to = $submitted_to;
        //$message->approximate_solve_date = $approximate_solve_date;
        $application->solve_status = 0;
        //$message->solve_date = $solve_date;
        //$message->action = $action;
        //$message->action_type = $action_type;
        //$message->action_log = $action_log;
        //$message->back_office = $back_office;
        //$message->tracking_id = $tracking_id;
        //$message->tracking_info = $tracking_info;
        //$message->cdesk = $cdesk;
        //$message->cdesk_details = $cdesk_details;
        //$message->note = $note;
        //$message->certificate = $certificate;
        //$message->rating = $rating;
        //$message->feedback = $feedback;
        //$message->display_name = $display_name;
        //$message->feedback_date = $feedback_date;
        $application->status = 1;
        $application->created_at = date('Y-m-d H:i:s');
        $application->updated_at = date('Y-m-d H:i:s');
        $application->save();

        $time = time()-1;
        $status[$time] = array('action'=>'L1','time'=> date('Y-m-d H:i:s'),'app'=>$app_from);
        $app_log = json_encode($status);

        $applicationLog = new ApplicationLog();
        $applicationLog->uid = $uid;
        $applicationLog->user_type = $user_type;
        $applicationLog->app_from = $app_from;
        $applicationLog->app_id = $app_id;
        $applicationLog->app_data = $app_data;
        $applicationLog->applicant_type = $applicant_type;
        $applicationLog->applicant_mobile = $applicant_mobile;
        $applicationLog->aid = $aid;
        $applicationLog->uaid = $uaid;
        $applicationLog->sid = $sid;
        $applicationLog->service_name = $service_name;
        $applicationLog->oid = $oid;
        $applicationLog->app_log = $app_log;
        $applicationLog->attachment = $attachment;
        //$applicationLog->certificate = $certificate;
        $applicationLog->is_submitted = $is_submitted;
        //$applicationLog->next_step = $next_step;
        $applicationLog->payment_type = $payment_type;
        $applicationLog->has_payment = $has_payment;
        //$applicationLog->is_payment = $is_payment;
        //$applicationLog->approximate_solve_date = $approximate_solve_date;
        //$applicationLog->rating = $rating;
        //$applicationLog->feedback = $feedback;
        //$applicationLog->display_name = $display_name;
        //$applicationLog->feedback_date = $feedback_date;
        $applicationLog->status = 1;
        $applicationLog->create_on = date('Y-m-d H:i:s');
        $applicationLog->created_at = date('Y-m-d H:i:s');
        $applicationLog->updated_at = date('Y-m-d H:i:s');
        $applicationLog->save();

        if(isset($request->apply_application) && $request->apply_application=='apply'){
            $request->merge(['aid'=>$aid]);
            return $this->submit($request);
        }

        $message = array();
        $message['type'] = 'applicationSaved';
        $message['title'] = 'আপনার আবেদনটি সফলভাবে সংরক্ষণ করা হয়েছে।';
        $message['text'] = "<div class='sweet-html'>আবেদনপত্রের নম্বর: <b class='text-primary'>".$uaid."</b>.<br>অ্যাপ্লিকেশন সিস্টেম আইডি: <span class='text-primary'>".$aid."</span></div>";
        $message['icon'] = 'success';
        $message['button'] = 'ধন্যবাদ';
        return redirect(config('app.url').'dashboard')->with(['message'=>$message]);
    }

    private function submit(Request $request){

        $user = session()->get('user');

        $application = Application::where('aid',$request->aid)->get()->toArray();
        if(isset($application[0])) {
            $application = $application[0];
            $application = Application::find($application['id']);

            /*if($application->is_submitted==1)
                return $this->alreadySubmitted($application);
            else if($application->is_submitted==2)
                return $this->alreadySubmitted($application);*/

            $applicationLog = ApplicationLog::where('aid',$application->aid)->get()->toArray();
            $applicationLog = $applicationLog[0];
            $applicationLog = ApplicationLog::find($applicationLog['id']);
        }
        else
            return $this->invalidApplication();

        $payment = ServicePayment::where('sid',$application->sid)->get()->toArray();
        $payment = $payment[0];

        $serviceCitizenCharter = ServiceCitizenCharter::where('sid',$application->sid)->get()->toArray();
        $serviceCitizenCharter = $serviceCitizenCharter[0];

        if($payment['has_payment']==1 && $payment['payment_type']==1 && $application->tid=='')
            return redirect('/message/payment/'.$application->aid);

        if(($user['user_type']==1 || $user['user_type']==2) && $user['id']!=$application->uid)
            return $this->unauthorizedApplication();

        $app_from = 'myGov';

        /*Update Application*/
        $application->app_from = $app_from;
        $application->is_submitted = 2;
        $application->submitted_date = date('Y-m-d H:i:s');

        /*Update Application Log*/
        /*Pre Log*/
        $log = json_decode($applicationLog->app_log,true);
        /*New Log*/
        $time = time();
        $log[$time] = array('action' => 'L4', 'time' => date('Y-m-d H:i:s'),'app'=>$app_from);

        $applicationLog->is_submitted = 2;
        $applicationLog->app_log = json_encode($log,JSON_UNESCAPED_UNICODE);

        /*Approximate Date*/
        $calendar = new Calendar();
        $approximate_solve_date = $calendar->nextWorkingDay($serviceCitizenCharter['time_duration']);

        $application->approximate_solve_date = $approximate_solve_date;
        $applicationLog->approximate_solve_date = $approximate_solve_date;


        $application->save();
        $applicationLog->save();

        //$queue = new Queue();
        //$queue->queue(config('app.url').'message/autoSubmit?aid='.$message->aid, 'general');

        return $this->autoSubmit($request);
    }

    private function update(Request $request){

        $application = Application::where('aid',$request->aid)->get()->toArray();

        if(isset($application[0])) {

            $application = Application::find($application[0]['id']);
            $aid = $application->aid;
            $uaid = $application->uaid;

            $applicationLog = ApplicationLog::where('aid',$application->aid)->get()->toArray();
            if(isset($applicationLog[0]))
                $applicationLog = ApplicationLog::find($applicationLog[0]['id']);

            $values = $request->all();
            unset($values['aid']);
            unset($values['_token']);

            $attachment = '';
            /*Attachment*/
            if(isset($request->attachment))
                $attachment = json_encode($request->attachment,JSON_UNESCAPED_UNICODE);
            /*Own Application*/

            $data = json_encode($values, JSON_UNESCAPED_UNICODE);
            $application_date = date('Y-m-d H:i:s');

            $application->application_date = $application_date;
            $application->attachment = $attachment;
            $application->data = $data;

            $log = json_decode($applicationLog->app_log,true);
            $time = time() + 1;
            $log[$time] = array('action' => 'L1', 'time' => date('Y-m-d H:i:s'), 'app' => $application->app_from);
            $applicationLog->app_log = json_encode($log);
            $applicationLog->attachment = $attachment;
            $applicationLog->create_on = date('Y-m-d H:i:s');

            $application->save();
            $applicationLog->save();

            if(isset($request->apply_application) && $request->apply_application=='apply'){
                $request->merge(['aid'=>$aid]);
                return $this->submit($request);
            }

            $message = array();
            $message['type'] = 'applicationSaved';
            $message['title'] = 'আপনার আবেদনটি সফলভাবে সংরক্ষণ করা হয়েছে।';
            $message['text'] = "<div class='sweet-html'>আবেদনপত্রের নম্বর: <b class='text-primary'>".$uaid."</b>.<br>অ্যাপ্লিকেশন সিস্টেম আইডি: <span class='text-primary'>".$aid."</span></div>";
            $message['icon'] = 'success';
            $message['button'] = 'ধন্যবাদ';
            return redirect(config('app.url').'dashboard')->with(['message'=>$message]);
        }
        return $this->invalidApplication();
    }

    public function autoSubmit(Request $request){
        $this->autoSubmit = 2;
        return $this->finalSubmit($request);
    }

    private function finalSubmit(Request $request){

        $application = Application::where('aid',$request->aid)->get()->toArray();
        if(isset($application[0])) {
            $application = $application[0];
            $application = Application::find($application['id']);

            $sid = $application->sid;
            $oid = $application->oid;

            if($application->is_submitted==1)
                return $this->alreadySubmitted($application);

            $applicationLog = ApplicationLog::where('aid',$application->aid)->get()->toArray();
            $applicationLog = $applicationLog[0];
            $applicationLog = ApplicationLog::find($applicationLog['id']);
        }
        else
            return $this->invalidApplication();

        /*Submit date*/
        if ($application->is_submitted == 0 || $this->autoSubmit==2) {
            $application->is_submitted = 1;
            $application->submitted_date = date('Y-m-d H:i:s');
            $applicationLog->is_submitted = 1;
        }else{
            $application->re_submitted = 1;
            $application->re_submitted_date = date('Y-m-d H:i:s');
        }

        $serviceIntegration = ServiceIntegration::where('sid',$application->sid)->get()->toArray();
        if(isset($serviceIntegration[0]))
            $serviceIntegration = $serviceIntegration[0];

        $serviceOffice = ServiceOffice::where('sid',$sid)->where('office_id',$oid)->get()->toArray();
        if(isset($serviceOffice[0])) {
            $serviceOffice = $serviceOffice[0];
            $officers = $serviceOffice['submit_desk'];
        }

        $nDoptor = new nDoptor();

        if (empty($officers) || $officers == 0) {
            $frontDesk = $nDoptor->data('officefrontdesk?officeId=' . $oid);
            $frontDesk = json_decode($frontDesk, true);
            if (!empty($frontDesk) && isset($frontDesk[0]))
                $officers = $frontDesk[0]['officeUnitOrganogramId'];
        } else {
            $officeUnitOrganogram = $nDoptor->data('officeunitorganogram?refOrigin=' . $officers . '&office=' . $oid . '&xgselect=unit+nameBn+name+office+id+superiorUnit');
            $officeUnitOrganogram = json_decode($officeUnitOrganogram, true);
            if (isset($officeUnitOrganogram[0])) {
                $officers = $officeUnitOrganogram[0]['id'];
            }
        }

        $application->submitted_to = $officers;

        $applicationSubmitted = false;

        $appData = new DataProcess();
        $data = $appData->getApplicationData($application->aid, $officers, $application->data);

        /*If message recipient */
        if($serviceIntegration['application_recipient']==1) {
            $workFlow = new Workflow();
            $sendStatus = $workFlow->send($data,$application->sid,array('email'=>''));

            if($sendStatus['status']==1 && $sendStatus['data']['status']==1)
                $applicationSubmitted = true;
            else $applicationSubmitted = false;

            $back_office = 'workflow';
            $tracking_info = json_encode($sendStatus,JSON_UNESCAPED_UNICODE);
        }/*If message recipient eNothi*/
        else if($serviceIntegration['application_recipient']==2){
            $nothi = new Nothi();
            $sendStatus = $nothi->sendToNothi($data, $application->toArray());

            $applicationSubmitted = $nothi->checkNothiStatus($sendStatus, $application, $data);
            $back_office = 'eNothi';

            if(isset($sendStatus['data']['dak_received_no']))
                $tracking_id = $sendStatus['data']['dak_received_no'];
            $tracking_info = json_encode($sendStatus,JSON_UNESCAPED_UNICODE);
        }

        if($applicationSubmitted){
            $application->back_office = $back_office;

            if(isset($tracking_id))
                $application->tracking_id = $tracking_id;

            $application->tracking_info = $tracking_info;

            $log = json_decode($applicationLog->app_log,true);
            $time = time() + 1;
            $log[$time] = array('action' => 'L2', 'time' => date('Y-m-d H:i:s'), 'app' => $application->app_from);
            $applicationLog->next_step = 1;
            $applicationLog->app_log = json_encode($log);

            $application->save();
            $applicationLog->save();

            return $this->successfullyApply($application);
        }

        return $this->unexpectedError();
    }

    private function successfullyApply($application){

        $message = array();
        $message['type'] = 'successfullyApply';
        $message['title'] = 'আপনার আবেদনটি সফলভাবে প্রেরণ করা হয়েছে।';
        $message['text'] = "<div class='sweet-html'><div>আবেদনপত্রের নম্বর: ".$application->aid."।</div><div>আপনার আবেদনপত্রের অবস্থা জানতে <a href='" . config('app.url') . 'application/status/' . $application->aid . "'>এখানে</a> ক্লিক করুন।</div></div>";
        $message['icon'] = 'success';
        $message['button'] = 'ধন্যবাদ';
        return redirect(config('app.url').'dashboard')->with(['message'=>$message]);
    }

    private function alreadySubmitted($application){

        $message = array();
        $message['type'] = 'applicationAlreadySubmitted';
        $message['title'] = 'আপনার আবেদনটি সফলভাবে সংরক্ষণ করা হয়েছে।';
        $message['text'] = "<div class='sweet-html'><div>আপনার আবেদনটি (<a href='" . config('app.url') . 'application/status/' . $application->aid . "'>" . $application->aid . "</a>) বর্তমানে প্রক্রিয়াধীন আছে।</div><div>আপনি এখন আবেদনটি পরিবর্তন করতে পারবেন না।</div></div>";
        $message['icon'] = 'success';
        $message['button'] = 'ধন্যবাদ';
        return redirect(config('app.url').'dashboard')->with(['message'=>$message]);
    }

    private function alreadyInQueued(Request $request){
    }

    private function invalidApplication(){
        $message = array();
        $message['type'] = 'invalidApplication';
        $message['title'] = 'দুঃখিত আবেদনের তথ্যটি খুঁজে পাওয়া যায়নি';
        $message['text'] = "";
        $message['icon'] = 'warning';
        $message['button'] = 'ধন্যবাদ';
        return redirect(config('app.url').'dashboard')->with(['message'=>$message]);
    }

    private function unauthorizedApplication(){
        $message = array();
        $message['type'] = 'invalidApplication';
        $message['title'] = 'দুঃখিত, আপনার এই আবেদনটি প্রেরণ করার অনুমতি নেই';
        $message['text'] = "";
        $message['icon'] = 'warning';
        $message['button'] = 'ধন্যবাদ';
        return redirect(config('app.url').'dashboard')->with(['message'=>$message]);
    }

    private function unexpectedError(){
        $message = array();
        $message['type'] = 'unexpectedError';
        $message['title'] = 'কারিগরি ত্রুটির জন্য দুঃখিত';
        $message['text'] = "";
        $message['icon'] = 'warning';
        $message['button'] = 'ধন্যবাদ';
        return redirect(config('app.url').'dashboard')->with(['message'=>$message]);
    }
}
