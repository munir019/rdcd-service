<?php

namespace App\Http\Controllers;

use App\Orangebd\SiteInfo;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\App;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function __construct()
    {
        $version = '1.0.1'.time();
        $baseUrl = config('app.url');

        $lang = 'bn';
        if(isset(session()->get('user')['lang']))
            $lang = session()->get('user')['lang'];

        App::setLocale($lang);


        $siteInfo = new SiteInfo();
        $site = $siteInfo->getInfo();


        /*$module = new SystemModule();
        $systemModuleJson = $module->getModuleJson();
        $systemModuleView = $module->getModuleView($lang);
        $systemNavigation = $module->getNavigationJson();

        $currentNavigation = str_replace($baseUrl,'',Request::url());

        $language = new SystemLanguage();
        $systemLanguage = $language->getLanguage($lang);*/

        view()->share(['version'=>$version,
            'baseUrl'=>$baseUrl,
            'lang'=>$lang,
            'site'=>$site,
            /*'systemModuleView'=>$systemModuleView,
            'systemLanguage'=>$systemLanguage,
            'systemNavigation'=>$systemNavigation,
            'currentNavigation'=>$currentNavigation*/]);
    }
}
