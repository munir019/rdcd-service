<?php

namespace App\Http\Controllers;

use App\Models\CitizenLogin;
use Illuminate\Http\Request;
use App\Models\CitizenProfile;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    public $data = array("id" => "6067c21d69d6b",
        "mobile" => "01717303200",
        "email" => "munir019@yahoo.com",
        "nid" => 6427882573,
        "name" => "মোঃ মুনির হোসেন",
        "name_en" => "MD. MUNIR HOSSAIN",
        "mother_name" => "রাহিমা বেগম",
        "mother_name_en" => "Rahima Begum",
        "father_name" => "মোঃ আব্দুল মালেক",
        "father_name_en" => "Md.Abdul Malek",
        "spouse_name" => "নুরনাহার",
        "spouse_name_en" => "Nurnahar",
        "gender" => "Male",
        "date_of_birth" => "1986-11-09",
        "occupation" => "Private Service",
        "religion" => "Islam",
        "pre_address" => "বাসা/হোল্ডিং:গাজীপুরা (পশ্চিম পাড়া), গ্রাম/রাস্তা:999        , ওয়ার্ড নং-৫০, ডাকঘর:এরশাদ নগর-1712, টঙ্গী, গাজীপুর সিটি কর্পোরেশন, গাজীপুর",
        "per_address" => "বাসা/হোল্ডিং:১০/১১ মালেকের বাড়ি, গ্রাম/রাস্তা:999, ডাকঘর:এরশাদ নগর-1712, গাজীপুর সদর, গাজীপুর",
        "photo" => "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAKAAAACgCAYAAACLz2ctAAAAAXNSR0IArs4c6QAAFWFJREFUeF7tnXl4VEW2wM+5HbIjQhLDKg8REHEGxeXzjaI+8Sk6LiOkg6CQDgNxUEBUgiwmqSaILHEBAiqifSOODLnBjc9Bx9EB9Tmfjg7iwhJ2iVkgIUhWkvSt93VDZ5KQTm93qbp982e66mz1u1W36ladQjD/2kXgngwSG90cGYe06VoZYDwi3A4U+gcVJoRSBNiKiO/JtPGbxm5RdVvXk/qgZBm0EhrUL7/csqaTyxGE2yilswHoYL8qKVdovwCwDpwtH23euGSPcmL5khRWAKakZY8RBCGDUrQCULZ8p0ABYAtFWFskku18YRS8tWw1QvB+eKlJMcVmtyOFGYCQqLh4dQWeQIQ3CgfCPCBEVleVftINCaDVRp4DwBkANEa/0CqpGc8g0vxCB5mrpFQWZBkGwPun5Q6PaHHuZiGoatsgRFgu37whyxDvjVwDaLOR6DpKVwLiTLUbnUn5iKviKJ0viqSRSfv8MIpLACdPXhnXaKn/CIDe4IePxi9C4QuIgzuldaSWN2e5AvBcj/dvQBzOW6A1sRfpnjiKo3jqEXkBEK1ppBgQLtWkIflXclQScwYBoGtph+k/1gHElHTyZ6QwkekosmvcB5JI7mbXPABmAUyZQu5GAbayHDxubBPgPul18j6L9jIHoNVK4iEO9gFAXxYDxrFNZRALQ1mbqDAFYGr64rmUyis5bmTmTUdAUijm2FkxlBkArTb7SQDak5XAGNsOrJHEnAtY8FF3ACfYyPUywD9ZCEa42SCjcP0WR/ZXevqtK4ApaWQjIjykZwBM3bBFEkmKXnHQDUCrjdQBQKxejpt620QAoVZykO56xERzACdNerZnc+SZSgAQ9HDY1Ok1AjJgZG/JsfCEljHSFMDUNHITRdihpYOmroAjcKckkg8DrhVkBc0AtKZl/xFQ2BCknWY1DSOAVJ5ZWLB4rRYqNQEwNZ3kUgpPa+GQqUOZCCDAmkKRzFZGmncpqgNoteVsAcBxajtiylclAm9JInlQFcnnhKoKoNVGigBgvJoOmLJVjgCFd6QColoHohqAqenkLWqAXSwWi1D2mxGXHB5z69WDe13YPdEioFMQBEABQcCz4ZMpBSpTkGUZnDK1nDxVU/nJJ98e/OGnQ5c4Zbm3yohoIV61tUJVAEy1kRcowBwtIqOgDhofF9OUMu7mH0eNHHK1gnJbRe3e+/NPf/7L3wbV1Da4DkupEns17HbJRIQlhQ6SpbR8xYMwwWafJQNdrbShasnr0yexes4j4y0xMZGafhutr286nf/y21hSekKXBeBg4kkRpxY5chzB1PVWR1EAx00lt1lk+FhJA9WSdcvoK0vH3TeaiS1f72794vinO3ZepJavispF5y2SI1extVzFAJw4kSS2RIGmq+jBBHbYkP57Hn34fibPlLxW8MHeXT8cuiwYvzStY4EE6TVyUgmdigFotREn65/Xli/JaIiJjmL6sHpjY1PjvKdfiVaicVWUIUsisSghXxEAremkBijEK2GQGjJiY6NOLFuckaSGbLVkzs9aX1XfcCZBLfkKyK2XRBIXqpyQAbTayLsAcF+ohqhVv3+/pF/mPf5AP7Xkqyl35YubfzlWcpxd2xVYIwwJwJQ0+xhE+nc1GyEU2T0vjD9mfzp9QCgy9K6bnesoPfVrLROTpU5jIdDR0uv2L4KNU0gAWm2E4XOn6FydN1OR95Rgg6tUvdlz850AlFlfJJEEzVHQFa02choAmF3DenHFzBZBwAilINBTjizLzjnz1jILIABWS2JOr2BiFBSA1jT7IkC6JBiFWtSx3n/zL6Nv+C27705BBOGz//u+ouidHclBVNWkCgLOLxRzlgeqLGAAzyYGqmM3CQ5Cy+qVswzR83VszNmZa1qAAru+xUL3QM8dBwyg1UbKAIDZD+yTJowpu/7ay/sE+iTyUP6fX+2u2CR9wmwvCBRKpAIS0KQvIABTbfZ7KFAmUzx4AFqdN4sHloK2cfbcNUHX1aIiAvy+UCR/9VdXIACi1cZ2ruKBF/eue3K2NeTFUX+Dp0e5vFWb634+dpxpHwOZFfsNoDUt5z1AvFePoPurc9G8h44mX9RzoL/leSxXWlZ1bNlzbwU0zGntJ0XYVOQgk/zR6yeAFK02O/OZ2o0+/HoalPVh2GWnv72gXwBa08kRoMB0z4KIzatWzuzmz1PHe5nHMvNbKKXszoZdAUY4IDnIEF+x9gmgOy0uQIMvQXr/Hh0d2bxiycNhAeC8p19pbmxsYt/XOoiSJNLUFRs+AUxNt++mlDK5f66tY0mJPY5mzZ/CdC+t1ENqX1pQVnXyNPtLTQg7JQcZFTSA1kdIPNRDjVKBU1POsKEDvn404w/XqamDFdmrX3pn54GDJVexYk9XdsQBxHSVNL3LHtCaRj4HhBt5cPSaUUO/nDLpjt/xYGuoNjo2fvj1zl37OXnYcIck5tzizWevAPLy7udxbPjQAV/PCJMeMH/9OzuLi/noAV3tU9u9KnrbmjVnOoPQK4CpNrKaAnDzWSE5uVfxoswHh4bau/BQ/5nlbx6uOFE9iAdb3RNiCi8UFpAnAgKQ7b1+57tiAsg2jt7WBTvtAcenLR4hoPwj2y61t84EkO3WkgGGbxHJ3o5Wdgogb72fyykTQLYBBAAqieS8pKQmgMy32/kG8vYO6PGgs2H4PAA5zeti9oAcPEiI+GyhI2dhW1PPA9Bqc989G8WBP+1MNIdgLlqsURJJu8QAHQDkY9dLZ6E2AeQCQJDEHKHtLZ7tALTayAoAyOTDFXMWzGM7IYUlhQX/SfPWEcDjAMBVCgtPI5g9IDc4HpdE0nqupSOADB807zrAJoDcANhus2orgOOmZP+PRRA+5ccNcwjmta2Qws2FBeQz92c6jxPWNCIBgm53hoUaTLMHDDWC2tWnFDYVFZw9M9IWQBmQr7zFbUNmAqgdQCFrokClgrNfRf4DINOJhny7bALoO0YslfB8FXEDOGFy7nDZ4tzNkoGB2pKc3LN4UeZD4bEda8WbhyqOV18SaIxYKu/ZnOAGMDXN/jhF+jxLBgZoi7w6b1ZY3b45e+4a14qFzzM9AcZRs+IU4NEikaxzO2C1kWIA8HmETjPrAlT04oqZTkFAhtOXBeiQH8VlmTrnzMvn2ee9kkiGewDkdv1v1JVD9tkeGjvMjzYzXBHHxm0Hdu46cCmvjrneA7kH8Fn79Jq4uGhmE2WqCUdtXUPtwpwNzCaH9+W7G8B7MkhsdBPU+SrM6u/hko7DW/x5SNPhlZ26X2PRmr4gCWiU6xswl38mgGyna+sKqohISMJUW+5dFJwfcEkfAJgA8gsgOGEMptjIegSYbgLIZwR4HoIRYJ0r6WQpALCfZ8QLH2YPyHEPCPSQC0Bul2BcTJoA8gwggAkgnyNvq9U8D8EuJ0wATQB1jYAJoK7hD1252QOGHsOQJJjvgOY7YEgAhVrZBNAEMFSGQqpvAmgCGBJAoVY2ATQBDJWhkOqbAJoAhgRQqJWftWfUxMVFheV2rLr6xtoF2a9yux3LEOuAo0YO2W+bPJbb3dyhPICvb/zw4He79g8ORYbedblfB3QFMCy35FPqnJPJ9ZZ8N/uGABAAzENJendlQeo3CoAQ2S2iPm/pjBieD9f72Yb0yQXrzjQ3O6P9LM90MReAJQDQj2kr/TTuwh7xJxdnpffyszh3xf7+j2+Pv//BlxdxZ7h3g4vRmk5eAQoZRnHKqMsyczLzG2VKDdHrtWFtlasHHAsA24wC4DL7tF9j42J6GMUflx8GOAPceXMIEaPRmr40CWgTt4eSOno2+ne/PW0dd/MFRgLws8+/31P03g7mbywNNOYRZyAJrdbnYyDudH2glZktj9CyeuUsti9zDjB4vG+58uau6yZN7g+md+bcUjL9dHx8tCF6QadTbnn8qbWGeqA8bWaIzAidATh82MC9M6bfe1mAHQ2TxRfmvFpbW9fI9ec2b4FtCyDXyYk6c9Aos2GjDr8A8JMkkivO5ge0LX5EBnktk11AkEY9PstaPmhg795BVmei2rr1753eW/yzIV4lOgZUQJi+2UE2uAEcbyOXCQB7mIi6gkbw3AtSSuljmfnc5v/z1YyCDMM2v0GKDZOitzOHcxaklSckXMBlL7hB/Ovx7388aKSvHu2aqF2KXtcvVhtxAoChsoxaLJbmF5Y/0s3X08ja7y0tctMT89dGsmaXcvagUxJz3DP71h4wNd3+FqV0onJK2JA0e8b9xZcO7s9V7uiFOa+W1NY19mcjgspbgRTeLCwgk9sBaJ2acyPI+Lny6nSX2Lw6bxY3vWBdXUPDgpwN7W6U1D2CChtAEW4ocpAv2wF4bhjmOk+Mtzj9/o7rS+7432u56FEMvOzS2jxtL67ueFdcGQBw+dLu6yF9ftmMpoiICKbfqzZu+tuhf327j+vrF3y1AwCUSiJp3f7XDsAUm30xAs3yQwh3ReLjYiqX2qclsmp445mmhnmLXjH00OuOPYUcqYAs9rRDx3Um1/YsmdVGCtWuuXNSKy7un9x6VWio8pSs/1hmvpNSyvO1C36FQxLdV3S1vuqdt9BptdnrAahhn8TVebOYu+Bl+XObSn8pq+zrVwvyXahBEklsWxfOBzCNZAOCnW8/vVvfLSKi+bllM5iZFW//7Lvyt9//3JDv3R1bAVHIKnRkL+kSQCPPhj2Or1z6p5qoyG5MHGYPh1mvJ+5tZ7/e3gHd/7faSAsAGPZ9xARQl/FNlkRyHlOdfuyekJY7WEbnAV3M1ECpCaAGQe6gQqCWSzcXZB08b1j2Zgrvycu7CrEJoPYAdjb8uqzwut0nxUaeQYCF2puqvkYTQPVj3EHDMkkkCzrT6hXAjIyMbtVNfZs0N1UDhSaAGgS5rYo6iJIk0ilLXW54tKaRjwDhdo3NVV2dCaDqIW6r4BNJJLd509g1gFYSCXFwRlNzNVBmAqhBkM+pcB29FEXSGBSA7iWZdPJvoHCVdiarr8kEUP0Yn9OwSxLJlV1p83nmgBAi/HQEXLulDfNnAqhNU544At22b3evKXv98wmguxdMIz8AwhXamK2+FpYOKxn2Swile6QC++W+WtMvAN0Qcn6pYdtAmAD6wiL0372t+3WU7D+AaWQdIMwI3TS9JaBzdd5MZj4zLszZcKC2ruFSvaOipH4EeLVQJH6l/PMbQKP0gn17J+yfP3cSM0nNfz5WcSpvVeGFSgKgtyx/ez+XnYEBODnrZrBYtuvtYCj6WRp+PX7MzlzTAhSMkYAIYYzkIJ/620YBAXhuQrIPELg65ugJxr133XDwtltHMXetwcFDpSdXrdtihNTCRySRDPIXvoB7QFcFm41E1wE0BKKEhbL9+iYefeqJiQNZsKUzG15+bevh3XuOBNR4zPlSd0GsJD0REBsB94Aup1Nt5E8U4CXmAuDFoN+MGPTL9PS7mU/Env/SOzuLD5ZwueiPFDILC0heoEwEBeC5ofgEIDB7ysxlo0UQmpbnZjgjo7pxc8bldE3d6axcRyyVKT/vhBSqpQIS1CtE0ACyPCtGxOapaXceHnnFYC7fVV2x3fXDwcOOjR/2l2WZmfMr3nq3QGa9Qa8DdqZ8wuTc4bLFuTvQblfN8nff9d9lt996TR81dWgp+7vvD5S8/saH/QBoSJ2FajZT4RqpIPvbYOWH7FSqjeRTgEeDNUCpeldfNXRv2oN3GCItb2cx2bP3aOVLG7YmsAQiUnAUFpCpobRhyACeG4p1S+kRFxO9f+niaZcgIjNfN0JpEF91jxwpL38+X2LhGOcpSSQ9fdnr63dFADwHoaYn6aKiutWvWPJwNCIaKqehrwbz/H74SPmhF9cVXazTZKXTE27+2t62nGIA3nnnrKj45MQGtYcIQcBa+9NTG3tcEMv0DDyYxgimztnJyrZ+sky1S7wUGX2htH7+r8HYq+gkpKMwlXNNO6dOHltx5cgh4ZDCIuC21WqyIoBw3WYx+18BG+ilgmI9oEf+hHRyt0xhq1IGuof3+24qGT16JBf5/ZT0OxhZak5WBICJm0Xyl2Ds8lZHcQDdwKSTmUBhTaiG/tfFvb95fFbKNYiqmBmqeUzXP3K0vPz5NcpNVhDAXigSorTTqrVsShpZiQhzgzG4R4/4itysdCbTqAXjj551lJisUAqOohCXWzTtAT3KUmzkZQR42N8GiIuLrsrN+mP3iAhBuxdqf43jvFywX1YowqYiB5mklvuq9YAeg1PTyXJKYV5XDrhmtgszH6y9KKknC+tbasWaCbk7vysudbz5ketLkT9t/5okkmlqGu6PESHr7+qd8NGH/1A5bMgAc0kl5CgHJmD33iNVL7u/rHj5o0KOVJDdmko3MOn+l9YEQPfExJY7FsDZejN7r57di8kiG7ebBfwPMdslt3301U/bPv56RAcrH5RE8pYWlmsGoMuZCVPIUFnAvfOffKC8b59Ew2wY0KKh1NbxWOaaFkohQul1Pl92awqgy5hTp071bHTKJ8IhIbev4DP2uzxj7pqEd0VySku7NAfQ41xpZVUVAgS1iVHLAIWJrpo+iQm6XAurG4Cuhi2rrCwAwClh0shsuomwuU9CwgN6GacrgC6nSyqqR1os8nd6BSC89dLr+iQmKvZdN5hY6g5gmyH5JAKEvL8smCCEWx0E+LV3YgITh+GZAfDskHxyBgBdF25AaOmvgJCZnJAQ8Ok1tWxkCkCXk6WUxmJV9S4Aaqh8KWo1YAByj9GEXpf1RawPoI7qRZkD0OPx8ePVNzkFeYfqEQgDBTLC7f0SEj5m0VVmAfQEq7yy6kUK8BiLwWPdJkphU9+kBNU2EijhP/MAepwsq6xyHf8croTTYSBjX5/EBC5OCHIDoAuaQkotN1ZWfYWIV4cBRAG7iIjfJ/fqeTUidpkWN2DBKlbgCkBPHI4dOxZjiY59GxHGqhgbjkTT7S0NDXcNGDAgoMRALDjIJYCewH1Dabd+VdW5APQpFoKptQ0UcFl1RVnOiBEjuL1QiGsA2zZ4aXX1QHTKrsvwjH5AnQrOliHJycnnXfyn9QOghD7DANg2GOVVVXOoTJ8BxHa3cysRMJ1kNKCAq3r36tXpfWs62aSIWkMC6IkMpRTLq6rnAVBX7poBikRMMyG0HCiu753YiyAi1UytxooMDWDHWJZXVV0PANMoBdcOHKbSnrlnrlQubKF0Q/+kpH9ozIFu6sIKwI5Rrqg4dYlTaB6DKLiOj2p8PIDuQ8Q8pyx/3i8paZ9uBOisOKwB7Cz2lNLospqaeEtDyzAaQcdTgHFAIbjc0ghHAYQtQgt92xkTsa9P9+61iOj14j6dWdBF/f8DS8Yggamxn2cAAAAASUVORK5CYII=",
        "nid_verify" => 1,
        "brn" => null,
        "brn_verify" => 0,
        "passport" => null,
        "passport_verify" => 0,
        "tin" => null,
        "tin_verify" => 0,
        "bin" => null,
        "bin_verify" => 0,
        "token" => "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.IjYwNjdjMjFkNjlkNmIi.bxANttOrLVcYDshfSiQORw0c7kNFdXXdattmbNF6n-o");

    public function idpLogin(Request $request)
    {
        // $data = $this->data;
        // $data = $this->idpUserValidate($data);
        // session()->put('user', $data);
        // if(!empty(session()->get('redirect_uri')))
        //     return redirect(session()->get('redirect_uri'));
        // return redirect(config('app.url'));

        $redirect = '';
        if(isset($request->redirect) && empty(session()->get('citizen')))
            $redirect = '?redirect='.$request->redirect;
        else if(isset($request->redirect) && !empty(session()->get('citizen')))
            return redirect($request->redirect);

        $client = new \IDP_Client();
        $client->setClientId('WsijVtQp36sFPCtuZ3qc');
        $client->setClientSecret('oDxb2HcjzPpXjA7gYhY9.VMfEWCXt8g');
        $client->setRedirectUri('http://service.rdcd.gov.bd/auth'.$redirect);

        $authUrl = $client->loginRequest();
        header("Location: ".$authUrl);
        die();
    }

    public function idpAuth(Request $request){
        $client = new \IDP_Client();
        $client->setClientId('WsijVtQp36sFPCtuZ3qc');
        $client->setClientSecret('oDxb2HcjzPpXjA7gYhY9.VMfEWCXt8g');

        $data = $client->responseRequest($_POST);

        if(isset($data->id)){
            $data->type = 'citizen';
            session()->put('citizen', $data);

            if(isset($request->redirect))
                return redirect($request->redirect);

            //return redirect(config('app.url'));
        }
        return redirect(config('app.url'));
    }

    public function idpLogout(){
        $client = new \IDP_Client();
        $client->setClientId('WsijVtQp36sFPCtuZ3qc');
        $client->setClientSecret('oDxb2HcjzPpXjA7gYhY9.VMfEWCXt8g');
        $client->setRedirectUri(config('app.url'));
        $authUrl = $client->logoutRequest();
        session()->forget('citizen');
        //Auth::logout();
        header("Location: ".$authUrl);
        die();
    }

    private function idpUserValidate($data){
        $citizen = CitizenProfile::where('mobile', (int)$data['mobile'])->limit(1)->get()->toArray();

        if(!empty($citizen)) {
            $citizen = new CitizenProfile();
            $uid = 'CI'.time();

            $citizen->status = 1;
            $citizen->uid = $uid;
        }else{
            $citizen = CitizenProfile::find($citizen[0]['id']);
        }

        $citizen->code = '880';
        $citizen->mobile = (int)$data['mobile'];
        $citizen->email = $data['email'];
        $citizen->name = $data['name'];
        $citizen->name_en = $data['name_en'];
        $citizen->mname = $data['mother_name'];
        $citizen->mname_en = $data['mother_name_en'];
        $citizen->fname = $data['father_name'];
        $citizen->fname_en = $data['father_name_en'];
        $citizen->sname = $data['spouse_name'];
        $citizen->sname_en = $data['spouse_name_en'];
        $citizen->gender = $data['gender'];
        $citizen->dob = $data['date_of_birth'];
        $citizen->occupation = $data['occupation'];
        $citizen->religion = $data['religion'];
        $citizen->national_id_no = $data['nid'];
        $citizen->verified_national_id_no = $data['nid_verify'];
        $citizen->birth_certificate_no = $data['brn'];
        $citizen->verified_birth_certificate_no = $data['brn_verify'];
        $citizen->passport_no = $data['passport'];
        $citizen->verified_passport_no = $data['passport_verify'];
        $citizen->save();

        $userType = 0;
        $citizenLogin = CitizenLogin::where('uid', $citizen->id)->limit(1)->get()->toArray();
        if(empty($citizenLogin)){
            $citizenLogin = new CitizenLogin();
            $citizenLogin->uid = $citizen->id;
            $citizenLogin->code = '880';
            $citizenLogin->mobile = (int)$data['mobile'];
            $citizenLogin->user_type = 1;
            $citizenLogin->status = 1;
            $citizenLogin->pincode = '';
            $citizenLogin->hashcode = '';
            $citizenLogin->save();
            $userType = 1;
        }else{
            $userType = $citizenLogin[0]['user_type'];
        }

        $user = CitizenProfile::where('mobile', (int)$data['mobile'])->firstOrFail()->toArray();

        $user['user_type'] = $userType;
        $user['photo'] = $data['photo'];

        return $user;
    }

    public function doptorLogin(){
        return redirect()->to(config('services.doptor.doptor_sso_login_url').'?referer='.base64_encode(config('services.doptor.app_login_url')));
    }

    public function doptorAuth(Request $request)
    {
        $user_doptor_info=null;
        try{
            if(!isset($request->data)){
                return redirect(config('app.url'));
            }
            $user_doptor_info=json_decode(gzuncompress(base64_decode($request->data)));
            if(!isset($user_doptor_info->status) || $user_doptor_info->status!="success"){
                return redirect(config('app.url'));
            }
            $user = $user_doptor_info=$user_doptor_info->user_info;
        }catch(Exception $ex){
            return redirect(config('app.url'));
        }

        $user = $this->getFormatedDoptorUser($user);
        session()->put('employee', $user);

        if($user->office_id){
            return redirect(config('app.url').'services?category=office&office='.$user->office_id);
        }

        return redirect(config('app.url'));
    }

    public function doptorLogout()
    {
        session()->forget('employee');
        //Auth::logout();
        return redirect()->away(config('services.doptor.doptor_sso_logout_url'));
    }

    /**Format  Doptor Employee Data Before Set in Session*/
    protected function getFormatedDoptorUser($employee){

        $employee = collect($employee);
        $userinfo = array($employee['user']);
        $employee_info = $employee['employee_info'];
        $office_info = $employee['office_info'];
        $organogram_info = collect($employee['organogram_info'])[$office_info[0]->office_unit_organogram_id];

        $user = collect($userinfo)->map(function ($user) use($employee_info, $office_info, $organogram_info){
            return [
                'user_id' => $user->id,
                'username' => $user->username,
                'user_alias' => $user->user_alias,
                'user_role_id' => $user->user_role_id,
                'is_admin' => $user->is_admin,
                'status' => $user->user_status,
                'is_email_verified' => $user->is_email_verified,
                'photo' => $user->photo,
                'employee_id' => $employee_info->id,
                'name_en' => $employee_info->name_eng,
                'name_bn' => $employee_info->name_bng,
                'father_name_en' => $employee_info->father_name_eng,
                'father_name_bn' => $employee_info->father_name_bng,
                'mother_name_en' => $employee_info->mother_name_eng,
                'mother_name_bn' => $employee_info->mother_name_bng,
                'date_of_birth' => $employee_info->date_of_birth,
                'nid' => $employee_info->nid,
                'brn' => $employee_info->bcn,
                'passport' => $employee_info->ppn,
                'personal_email' => $employee_info->personal_email,
                'personal_mobile' => $employee_info->personal_mobile,
                'is_cadre' => $employee_info->is_cadre,
                'employee_grade' => $employee_info->employee_grade,
                'joining_date' => $employee_info->joining_date,
                'default_sign' => $employee_info->default_sign,
                'office_info_id' => $office_info[0]->id,
                'office_id' => $office_info[0]->office_id,
                'office_unit_id' => $office_info[0]->office_unit_id,
                'office_unit_organogram_id' => $office_info[0]->office_unit_organogram_id,
                'designation_bn' => $office_info[0]->designation,
                'designation_en' => $office_info[0]->designation_en,
                'designation_level' => $office_info[0]->designation_level,
                'designation_sequence' => $office_info[0]->designation_sequence,
                'office_head' => $office_info[0]->office_head,
                'incharge_label' => $office_info[0]->incharge_label,
                'joining_date' => $office_info[0]->joining_date? date( 'Y-m-d', strtotime($office_info[0]->joining_date)):'',
                'last_office_date' => $office_info[0]->last_office_date? date( 'Y-m-d', strtotime($office_info[0]->last_office_date)) : '',
                'unit_name_bn' => $office_info[0]->unit_name_bn,
                'unit_name_en' => $office_info[0]->unit_name_en,
                'office_name_bn' => $office_info[0]->office_name_bn,
                'office_name_en' => $office_info[0]->office_name_en,
                'protikolpo_status' => $office_info[0]->protikolpo_status,
                'organogram_info_id' => $organogram_info->id,
                'superior_unit_id' => $organogram_info->superior_unit_id,
                'superior_designation_id' => $organogram_info->superior_designation_id,
                'ref_origin_unit_org_id' => $organogram_info->ref_origin_unit_org_id,
                'ref_sup_origin_unit_desig_id' => $organogram_info->ref_sup_origin_unit_desig_id,
                'ref_sup_origin_unit_id' => $organogram_info->ref_sup_origin_unit_id,
                'short_name_en' => $organogram_info->short_name_eng,
                'short_name_bn' => $organogram_info->short_name_bng,
                'designation_description' => $organogram_info->designation_description,
                'is_unit_admin' => $organogram_info->is_unit_admin,
                'is_unit_head' => $organogram_info->is_unit_head,
                'is_office_head' => $organogram_info->is_office_head,
                'created_by' => $organogram_info->created_by,
                'modified_by' => $organogram_info->modified_by,
                'type' => 'employee'
            ];
        })->first();

        return $user = (object) $user;
    }
}
