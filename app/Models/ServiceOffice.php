<?php

namespace App\Models;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class ServiceOffice extends Authenticatable
{
    use Notifiable;

    protected $table = 'my_gov_service_office';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $fillable = [
    ];

    public function services(){
        return $this->belongsTo(Service::class, 'sid');
    }
}
