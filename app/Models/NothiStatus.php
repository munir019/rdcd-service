<?php

namespace App\Models;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class NothiStatus extends Authenticatable
{
    use Notifiable;

    protected $connection = 'applicationDB';

    protected $table = 'nsp_nothi_status';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $fillable = [
    ];
}
