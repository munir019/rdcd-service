<?php

namespace App\Models;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class WorkflowDecision extends Authenticatable
{
    use Notifiable;

    protected $table = 'my_gov_workflow_decision';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $fillable = [
    ];
}
