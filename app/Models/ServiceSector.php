<?php

namespace App\Models;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class ServiceSector extends Authenticatable
{
    use Notifiable;

    protected $table = 'my_gov_service_sector';

    public function services(){
        return $this->hasMany(Service::class, 'sector', 'id');
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
    ];
}
