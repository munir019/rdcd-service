<?php

namespace App\Models;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class CitizenLogin extends Authenticatable
{
    use Notifiable;

    protected $connection = 'citizenDB';

    protected $table = 'nsp_login';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $fillable = [
    ];
}
