<?php

namespace App\Models;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class Service extends Authenticatable
{
    use Notifiable;

    protected $table = 'my_gov_service';

    public function serviceOffice()
    {
        return $this->hasOne(ServiceOffice::class, 'sid');
    }

    public function serviceSector(){
        return $this->belongsTo(ServiceSector::class, 'sector');
    }

    public function serviceRecipient(){
        return $this->belongsTo(ServiceRecipient::class, 'recipient');
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['id', 'sid', 'name', 'name_en', 'recipient', 'sector', 'application_recipient', 'payment_gateway',
                             'keyword', 'description', 'pre_requisite', 'status', 'onlive', 'office', 'hierarchy', 'icon',
                             'organization_id', 'created_at', 'updated_at'];
}
