<?php

namespace App\Models;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class LogSetting extends Authenticatable
{
    use Notifiable;

    protected $table = 'my_gov_log_setting';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $fillable = [

    ];
}
