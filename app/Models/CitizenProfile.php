<?php

namespace App\Models;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class CitizenProfile extends Authenticatable
{
    use Notifiable;

    protected $connection = 'citizenDB';

    protected $table = 'nsp_profile';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $fillable = [
    ];
}
