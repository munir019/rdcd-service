<?php

namespace App\Models;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class ApplicationLog extends Authenticatable
{
    use Notifiable;

    protected $connection = 'citizenDB';

    protected $table = 'nsp_application_log';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $fillable = [
    ];
}
