<?php

namespace App\Models;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class CalHoliday extends Authenticatable
{
    use Notifiable;

    protected $table = 'my_cal_holiday';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $fillable = [
    ];
}
