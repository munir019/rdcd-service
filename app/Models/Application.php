<?php

namespace App\Models;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class Application extends Authenticatable
{
    use Notifiable;

    protected $connection = 'applicationDB';

    protected $table = 'nsp_service';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $fillable = [
    ];
}
