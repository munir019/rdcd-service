$(document).ready(function() {

    /**Site Language Change*/
    $('#dropdownMenuButton').change(function () {
        var lang = $(this).val();

        switch (lang) {
            case 'en':
                window.location.href = 'http://service.rdcd.gov.bd/changeLanguage/en';
            break;
            case 'bn':
                window.location.href = 'http://service.rdcd.gov.bd/changeLanguage/bn';
            break;
            default:
                window.location.href = 'http://service.rdcd.gov.bd/changeLanguage/bn';
        }
    });

    /**Service 1st Level Menu Item*/
    $('.service-category').click(function(){
        if($(this).children().find('#expand-cat')){
            $(this).find("#expand-cat").toggleClass('fa-caret-right fa-caret-down');
        }
    });

    $('.office-list>li').click(function() {
        $(this).find('.office').addClass('active').removeClass('inactive');

        if($(this).siblings().children().hasClass('active')){
            $(this).siblings().children().removeClass('active').addClass('inactive');
        }
    });

    //$('.service-category-list>li').click(function() {
        //$(this).addClass('active').removeClass('inactive');

        // if($(this).children().find('#expand-cat')){
        //     $(this).children().find("#expand-cat").toggleClass('fa-caret-right fa-caret-down');
        // }

        // if($(this).siblings().hasClass('active')){
        //     $(this).siblings().removeClass('active').addClass('inactive');

        //     if($(this).siblings().children().find('#expand-cat').hasClass('fa-caret-down')){
        //         $(this).siblings().children().find('#expand-cat').addClass('fa-caret-right').removeClass('fa-caret-down');
        //     }
        // }
   // });

    /**Search Service*/
    var options = {
        //url: "../cache/service/search_service.json",
        url: "../cache/service/rapid_services.json",
        getValue: "name",

        list: {
            match: {
            enabled: true
            }
        },
    };

    $("#service_search").easyAutocomplete(options);
});
