/**
 * Created by Md. Munir Hossain on 06/06/2021.
 */
function engToBngNum(str){
    str = str.toString();
    str = str.replace(/0/g,'০');
    str = str.replace(/1/g,'১');
    str = str.replace(/2/g,'২');
    str = str.replace(/3/g,'৩');
    str = str.replace(/4/g,'৪');
    str = str.replace(/5/g,'৫');
    str = str.replace(/6/g,'৬');
    str = str.replace(/7/g,'৭');
    str = str.replace(/8/g,'৮');
    str = str.replace(/9/g,'৯');
    return str;
}

function removeRequiredError() {
    $('.required-error').on('change, input',function(){
        $(this).removeClass('required-error');
    });
}

jQuery(document).ready(function(){

    /*Service Category on service list*/
    if($('.service-category').length){
        $('.service-category').on('click',function(){
            if($(this).parent('li').find('ul').css('display')=='none') {
                $('.service-category').parent('li').find('ul').slideUp();
                $(this).parent('li').find('ul').slideDown();
            }else{
                $('.service-category').parent('li').find('ul').slideUp();
            }
        });
    }
    /*Service Category on service list*/

    /*List Search*/
    if($('.my-search-input').length) {
        $(".my-search-input").on("keyup", function () {
            var value = $(this).val().toLowerCase();
            $(this).parent('.my-search').find(".my-search-list li").filter(function () {
                $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
            });
        });
    }
    /*List Search*/

    /*Office Layer 3 on service list*/
    if($('.office-level').length) {
        $('.office-level').change(function () {
            var v = $(this).val();
            $('.office-level3').addClass('d-none');
            $('.office-level3-' + v).removeClass('d-none');
        });

        $('.office-level3').addClass('d-none');
        var l = $('.office-level').attr('data-id');
        $('.office-level3-' + l).removeClass('d-none');
        $('.office-level').find('option:nth-child(' + l + ')').attr('selected', 'selected');
    }
    /*Office Layer 3 on service list*/

    /*Service Form*/

    $('select').on('change',function(){
        $(this).find('option').removeAttr('selected');
        $(this).find('option[value="'+$(this).val()+'"]').attr('selected','selected');
    });



    if($('.apply-application')){
        $('.apply-application').on('click',function(){

            var requiredMissing = 1;
            var isMissing = 0;

            $('#office-selection select').each(function(){
                if($(this).is(':visible') && $(this).val()==''){
                    if(isMissing==0) {
                        $("html, body").animate({scrollTop: ($(this).offset().top - 150)}, "slow");
                        isMissing = 1;
                    }
                    $(this).addClass('required-error');
                    requiredMissing = 0;
                }
            });

            if(requiredMissing==1 && $(this).attr('data-type')=='apply'){
                $('[required="required"]').each(function(){
                    if($(this).is(':visible') || $(this).hasClass('attachment-value')) {

                        if($(this).attr('type')=='radio' && !$('input[name="'+$(this).attr('name')+'"]').is(':checked')){
                            if (isMissing == 0) {
                                $("html, body").animate({scrollTop: ($(this).offset().top - 150)}, "slow");
                                requiredMissing = 0;
                                isMissing = 1;
                            }
                            $(this).addClass('required-error');
                        }else if($(this).attr('type')=='checkbox' && !$('input[name="'+$(this).attr('name')+'"]').is(':checked')){
                            if (isMissing == 0) {
                                $("html, body").animate({scrollTop: ($(this).offset().top - 150)}, "slow");
                                requiredMissing = 0;
                                isMissing = 1;
                            }
                            $(this).addClass('required-error');
                        }else if ($(this).val() == '' || $(this).val() == null) {
                            if (isMissing == 0) {
                                $("html, body").animate({scrollTop: ($(this).offset().top - 150)}, "slow");
                                requiredMissing = 0;
                                isMissing = 1;
                            }
                            $(this).addClass('required-error');

                            /*if ($(this).hasClass('attachment-value')) {
                                var p = $(this).parent('div.selected-attachment');
                                p.find('.selected-attachment-name').addClass('error p-2');
                                p.find('.selected-attachment-name').html('সংযুক্তি প্রদান করুন * ');

                                if($(this).parents('td').attr('id')=='att7'){
                                    $('input[name="applicant_name"]').addClass('error');
                                }
                            } else if ($(this).hasClass('select2-hidden-accessible')) {
                                var p = $(this).parent('div');
                                p.find('.select2-selection__rendered').addClass('error');
                                //p.find('.selected-attachment-name').html('সংযুক্তি প্রদান করুন * ');
                            }
                            s = 0;
                            vi = 1;
                            error();*/
                        } else $(this).removeClass('required-error');
                    }
                });
            }

            if(requiredMissing==0)
                removeRequiredError()

            if(requiredMissing==1) {

                if($(this).attr('data-type')=='apply')
                    $('input[name="is_complete"]').val(1);
                else
                    $('input[name="is_complete"]').val(0);

                var html = $('.application-form form#application-form').clone();
                $('#previewApplicationModal').find('.modal-body').html(html);
                $('#previewApplicationModal').find('.modal-body').find('[obd="add-content"]').remove();
                $('#previewApplicationModal').find('.modal-body').find('select').attr('disabled', 'disabled');
                $('#previewApplicationModal').find('.modal-body').find('input').attr('disabled', 'disabled');
                $('#previewApplicationModal').find('.modal-body').find('textarea').attr('disabled', 'disabled');
                $('#previewApplicationModal').find('.modal-body').find('[obd="dsignature"]').each(function(){
                    $(this).find('canvas').remove();
                    var tmp = $(this).find('input[type="hidden"]').val();
                    $(this).find('input[type="hidden"]').remove();
                    $(this).prepend('<img src="'+tmp+'" />');
                });
                $('#previewApplicationModal').find('[data-type="apply"],[data-type="draft"]').hide();
                $('#previewApplicationModal').find('[data-type="' + $(this).attr('data-type') + '"]').show();

                $('#previewApplicationModal').modal('show');

                $('.apply-application-btn').on('click',function(){
                    $(this).parent('div.modal-footer').html('<i class="fa fa-spinner fa-spin me-2 text-warning"></i>Processing...');;
                    $('#application-form').find('[name="apply_application"]').remove();
                    if(requiredMissing==1 && $(this).attr('data-type')=='apply')
                        $('#application-form').append('<input type="hidden" name="apply_application" value="apply"/>');
                    $('#application-form').submit();
                });
            }
        });
    }

    /*Signature*/
    var canvasG = [];
    var ctxG = [];
    var drawingG = [];
    var prevXG = [];
    var prevYG = [];
    var currXG = [];
    var currYG = [];
    var signatureG = [];

    if($('[obd="dsignature"]').length){
        var i = 1;
        $('[obd="dsignature"]').each(function(){
            var id = $(this).find('canvas').attr('id');
            if(id !== undefined && id !== null) {
                dsignature(id, i);
                i = i + 1;
            }
        });
    }
    function dsignature(id, i){
        canvasG[i] = document.getElementById(id);
        var ctx = canvasG[i].getContext("2d");
        drawingG[i] = false;
        signatureG[i] = document.getElementsByName(id)[0];

        canvasG[i].addEventListener("mousemove", draw);
        canvasG[i].addEventListener("mouseup", stop);
        canvasG[i].addEventListener("mousedown", start);

        function start(e) {
            drawingG[i] = true;
        }

        function stop() {
            drawingG[i] = false;
            prevXG[i] = prevYG[i] = null;
            signatureG[i].value = canvasG[i].toDataURL();
        }

        function draw(e) {

            if (!drawingG[i]) {
                return;
            }
            var clientX = e.type === 'touchmove' ? e.touches[0].clientX : e.pageX;
            var clientY = e.type === 'touchmove' ? e.touches[0].clientY : e.pageY;
            currXG[i] = clientX - $('#'+id).offset().left;
            currYG[i] = clientY - $('#'+id).offset().top;

            if (!prevXG[i] && !prevYG[i]) {
                prevXG[i] = currXG[i];
                prevYG[i] = currYG[i];
            }

            ctx.beginPath();
            ctx.moveTo(prevXG[i], prevYG[i]);
            ctx.lineTo(currXG[i], currYG[i]);
            ctx.strokeStyle = 'black';
            ctx.lineWidth = 2;
            ctx.stroke();
            ctx.closePath();

            prevXG[i] = currXG[i];
            prevYG[i] = currYG[i];
        }

        $('[obd="clear-dsignature"]').on('click',function(){
            var p = $(this).parents('[obd-type="digital-signature"]');
            var n = p.find('canvas').attr('id');
            n = n.replace('dsignature','');
            var ctx = canvasG[n].getContext("2d");
            ctx.clearRect(0, 0, p.find('canvas').attr('width'), p.find('canvas').attr('height'));
        });
    }
    /*Signature*/

    /*Upload image and signature*/
    if($('[obd="uphoto"]').length){
        var i = 1;
        $('[obd="uphoto"]').each(function(){
            imageCrop('uphoto' + i, 'photo');
            i = i + 1;
        });
    }
    if($('[obd="usignature"]').length){
        var i = 1;
        $('[obd="usignature"]').each(function(){
            imageCrop('usignature' + i, 'signature');
            i = i + 1;
        });
    }
    function imageCrop(id, type){
        var $modal = $('#cropImageModal');
        var image = document.getElementById('imageForCropped');
        var cropper;

        var resizeForm = '';
        var imageName = '';
        var resizeWidth = 0;
        var resizeHeight = 0;

        $('[obd="upload-image"]').on('click',function(e){
            if($(this).parents('[obd="usignature"]').length)
                type = 'signature';
            else type = 'photo';

            var p;
            if(type=='signature'){
                $modal.find('[obd="cropImage-title"]').html('Resize Signature');
                $modal.find('[obd="cropImage-title-1"]').html('Your Signature');
                $modal.find('[obd="cropImage-title-2"]').html('New Resize Signature');
                $modal.find('#imageForCropped').attr('data-width',150);
                $modal.find('#imageForCropped').attr('data-height',75);
                $modal.find('#imageForCropped').attr('data-type','signature');
            }else{
                $modal.find('[obd="cropImage-title"]').html('Resize Photo');
                $modal.find('[obd="cropImage-title-1"]').html('Your Photo');
                $modal.find('[obd="cropImage-title-2"]').html('New Resize Photo');
                $modal.find('#imageForCropped').attr('data-width',100);
                $modal.find('#imageForCropped').attr('data-height',120);
                $modal.find('#imageForCropped').attr('data-type','photo');
            }

            if(type=='signature')
                p = $(this).parents('[obd="usignature"]');
            else
                p = $(this).parents('[obd="uphoto"]');
            p.find('input[type="file"]').trigger('click');
            e.stopImmediatePropagation();
        });

        $('#'+id).on("change", function(e){
            resizeForm = $('#'+$(this).attr('data-form-id'));
            imageName = $('input[name="'+id+'"]').attr('name');

            $('[obd="previewCroppedImage"]').css('width',$('#cropImageModal').find('#imageForCropped').attr('data-width'));
            $('[obd="previewCroppedImage"]').css('height',$('#cropImageModal').find('#imageForCropped').attr('data-height'));

            var files = e.target.files;
            var done = function (url) {
                image.src = url;
                $modal.modal('show');
                $modal.find('[obd="cropSignature"]').attr('data-id',id);
                $('#resizeSignatureModal').css('padding','0');
                $('#resizeSignatureModal').css('background','url(img/black-bg.png)');
            };
            var reader;
            var file;
            var url;

            if (files && files.length > 0) {
                file = files[0];

                if (URL) {
                    done(URL.createObjectURL(file));
                } else if (FileReader) {
                    reader = new FileReader();
                    reader.onload = function (e) {
                        done(reader.result);
                    };
                    reader.readAsDataURL(file);
                }
            }
            e.stopImmediatePropagation();
        });

        $modal.on('shown.bs.modal', function () {
            resizeWidth = $('#cropImageModal').find('#imageForCropped').attr('data-width');
            resizeHeight = $('#cropImageModal').find('#imageForCropped').attr('data-height');
            cropper = new Cropper(image, {
                aspectRatio: resizeWidth/resizeHeight,
                viewMode: 1,
                preview: '[obd="previewCroppedImage"]'
            });
        }).on('hidden.bs.modal', function () {
            cropper.destroy();
            cropper = null;
        });

        $('[obd="cropSignature"]').click(function(){
            var id = $(this).attr('data-id');
            canvas = cropper.getCroppedCanvas({
                width: $('#cropImageModal').find('#imageForCropped').attr('data-width'),
                height: $('#cropImageModal').find('#imageForCropped').attr('data-height'),
            });
            canvas.toBlob(function(blob) {
                url = URL.createObjectURL(blob);
                var reader = new FileReader();
                reader.readAsDataURL(blob);
                reader.onloadend = function() {
                    var base64data = reader.result;

                    $('input[name="'+id+'"]').val(base64data);
                    if($modal.find('#imageForCropped').attr('data-type')=='signature')
                        $('input[name="'+id+'"]').parents('[obd="usignature"]').find('img').attr('src',base64data);
                    else
                        $('input[name="'+id+'"]').parents('[obd="uphoto"]').find('img').attr('src',base64data);
                    $modal.modal('hide');
                }
            });
        });

        $('[obd="clear-image"]').on('click',function(){
            var p = $(this).parent('div');
            p.find('img').removeAttr('src');
            p.find('input[type="hidden"]').val('');
            p.find('input[type="file"]').val('');
        });
    }
    /*Upload image and signature*/

    if($('#attachments').length){
        $('#attachments').append('<form id="attachmentUpload" method="post" action="'+baseUrl+'attachment/upload" enctype="multipart/form-data"></form>');
        $('.attachment').each(function(){
            $(this).find('[type="file"]').hide();
            $(this).append('<i class="fas fa-cloud-upload-alt upload-but"></i>');
        });

        $('.upload-but').on('click',function(){
            var par = $(this).parent('.attachment');
            par.find('[type="file"]').trigger('click');
        });

        $('.attachment [type="file"]').change(function (e) {
            var file = $(this).val();
            var fName = file.match(/\\([^\\]+)$/)[1];

            var fSize = parseFloat(this.files[0].size) / 1024;
            if (fSize / 1024 > 1) {
                if (((fSize / 1024) / 1024) > 1) {
                    fSize = (Math.round(((fSize / 1024) / 1024) * 100) / 100);
                    fSize = ( fSize + "Gb");
                }
                else {
                    fSize = (Math.round((fSize / 1024) * 100) / 100)
                    fSize = ( fSize + "Mb");
                }
            }
            else {
                fSize = (Math.round(fSize * 100) / 100)
                fSize = ( fSize + "kb");
            }

            var ext = fName.split('.');
            ext = ext[ext.length - 1];
            ext = ext.toLowerCase();

            var par = $(this).parent('.attachment');
            par.find('.fileInfo').remove();
            par.append('<div class="fileInfo fs-8"><span class="text-primary">File Name:</span> '+fName+'. <span class="ms-2 text-primary">File Size:</span> '+fSize+'. </div>');
            /**/

            var clone = $(this).clone();
            $('form#attachmentUpload').html('');
            $('form#attachmentUpload').append('<input type="hidden" id="type" name="type" value="message" />');
            $('form#attachmentUpload').append('<input type="hidden" id="file_type" name="file_type" value="'+par.find('[type="file"]').attr('name')+'" />');
            $('form#attachmentUpload').append(clone);
            $('form#attachmentUpload').find('[type="file"]').attr('name','upload_file');

            $('form#attachmentUpload').ajaxSubmit({
                beforeSubmit: function() {
                    par.append('<div class="uploadLoading"><div></div></div>');
                },
                uploadProgress: function (event, position, total, percentComplete){
                    par.find('.uploadLoading div').css({'width':percentComplete+'%'});
                },
                success:function (data){
                    data = JSON.parse(data);
                    if(data['status']=='success'){
                        par.find('[type="hidden"]').remove();
                        par.append('<input type="hidden" name="attachment[]" value="'+data['fid']+'" />');
                        par.find('.uploadLoading').remove();
                    }else {
                        par.find('.uploadLoading').remove();
                    }
                },
                resetForm: true
            });
            e.stopImmediatePropagation();
        });
    }
    /*Service Form*/

    /*Application AutoFill*/

    if(appData!=''){
        var canvas = '';
        var ctx = '';
        var image = '';
        $.when(
            $.each(appData, function(i,v) {

                if ($.isArray(v)) {
                    var ele = $('[name="' + i + '[]"]');
                    ele.each(function(){
                        if($.inArray($(this).val(),v)>=0)
                            $(this).attr('checked', 'checked');
                    });
                } else {
                    var ele = $('[name="' + i + '"]');
                    if (ele.prop('tagName') == 'INPUT') {

                        if (ele.attr('type') == 'checkbox') {
                            ele.attr('checked', 'checked');
                        }else if (ele.attr('type') == 'radio') {
                            ele.each(function () {
                                if ($(this).val() == v)
                                    $(this).attr('checked', 'checked');
                            });
                        }else if (ele.attr('type') == 'hidden') {
                            if(i.indexOf("dsignature") > -1){
                                canvas = document.getElementById(i);
                                ctx = canvas.getContext("2d");
                                image = new Image();
                                image.onload = function() {
                                    ctx.drawImage(image, 0, 0);
                                };
                                image.src = v;
                                ele.val(v);
                            }
                        }else
                            ele.val(v);

                    } else if (ele.prop('tagName') == 'SELECT') {
                        ele.find('option[value="' + v + '"]').attr('selected', 'selected');
                        var dataType = v.replace(/\d+/g, '');
                        ele.attr('data-type',dataType);
                        ele.attr('data',v.replace(dataType,''));
                    } else if (ele.prop('tagName') == 'TEXTAREA') {
                        ele.html(v);
                    }
                }

                //$('input[name="name"],input[name="name_en"],input[name="fname"],input[name="mname"],input[name="mobile"]').attr('readonly','readonly');
                //<?php if($getUserData['verified_national_id_no']==1) {?>
                //    $('input[name="national_id_no"],input[name="dob"]').attr('readonly','readonly');
                //    $('input[name="dob"]').removeClass('date');
                //<?php } ?>
                //<?php if($getUserData['verified_birth_certificate_no']==1) {?>
                //    $('input[name="birth_certificate_no"],input[name="dob"]').attr('readonly','readonly');
                //    $('input[name="dob"]').removeClass('date');
                //<?php } ?>
            })).then(function(){ /*$("select").select2(); loadDistrict();*/ });
    }
    /*Application AutoFill*/

    var hash = window.location.hash;
    if(hash=='#print'){
        var pri = $('.printable').clone();
        pri.find('.noprint').remove();
        $('.wrapper').html(pri.html());
        window.print();
    }
});