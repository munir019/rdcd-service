<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your message. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', function () {
    return view('welcome');
});*/

/*Auto Routes*/
$appUrl = rtrim(config('app.url'),'/');
$url = Url();
$parseUrl = parse_url($appUrl);
$tmpAppUrl = explode('/',$appUrl);
$tmpCurUrl = explode('/',$url->current());
$urlParts = array_slice($tmpCurUrl,count($tmpAppUrl));

$projectUrl = '';
$projectUrl = trim($appUrl, '/');
$projectUrl = preg_replace('#^https?://#', '', rtrim($projectUrl,'/'));
$projectUrl = ltrim(str_replace($parseUrl['host'],'',$projectUrl),'/');

$controller = 'HomeController';
$method = 'index';

if(isset($urlParts[0]) && !empty($urlParts[0]))
    $controller = ucfirst($urlParts[0]).'Controller';
if(isset($urlParts[1]))
    $method = $urlParts[1];
$controller = $controller.'@'.$method;
$namespace = 'App\\Http\\Controllers\\';
/*Auto Routes*/

Route::get($projectUrl.'/', $namespace.'HomeController@index');
Route::get($projectUrl.'/changeLanguage/{lang}', $namespace.'HomeController@changeLanguage');
Route::get($projectUrl.'/privacy-policy', $namespace.'HomeController@privacyPolicy');

Route::get($projectUrl.'/services', $namespace.'ServicesController@index');
Route::post($projectUrl.'/search', $namespace.'ServicesController@getRdcdService');
Route::get($projectUrl.'/get-service-list/{officeId}', $namespace.'ServicesController@getServiceListByDoptor');
Route::get($projectUrl.'/services/info', $namespace.'ServicesController@info');
Route::get($projectUrl.'/application/status', $namespace.'ApplicationController@getStatus');
Route::post($projectUrl.'/application/get-status', $namespace.'ApplicationController@applicationStatus');
Route::get($projectUrl.'/cron', $namespace.'CronController@index');

Route::get($projectUrl.'/login', $namespace.'AuthController@idpLogin')->name('login');
Route::post($projectUrl.'/auth', $namespace.'AuthController@idpAuth');
Route::get($projectUrl.'/logout', $namespace.'AuthController@idpLogout');

Route::get($projectUrl.'/doptorlogin', $namespace.'AuthController@doptorLogin')->name('login');
Route::get($projectUrl.'/doptorauth', $namespace.'AuthController@doptorAuth');
Route::get($projectUrl.'/doptorlogout', $namespace.'AuthController@doptorLogout');

Route::any('{all}', $namespace.$controller)->where('all', '.*')->middleware('session.user');
