<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Mailgun, Postmark, AWS and more. This file provides the de facto
    | location for this type of information, allowing packages to have
    | a conventional file to locate the various service credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
        'endpoint' => env('MAILGUN_ENDPOINT', 'api.mailgun.net'),
    ],

    'postmark' => [
        'token' => env('POSTMARK_TOKEN'),
    ],

    'ses' => [
        'key' => env('AWS_ACCESS_KEY_ID'),
        'secret' => env('AWS_SECRET_ACCESS_KEY'),
        'region' => env('AWS_DEFAULT_REGION', 'us-east-1'),
    ],

    'doptor' => [
        'doptor_sso_login_url' => env('DOPTOR_SSO_LOGIN_URL'),
        'doptor_sso_logout_url' => env('DOPTOR_SSO_LOGOUT_URL'),
        'app_login_url' => env('APP_LOGIN_URL'),
        'doptor_api_login' => env('DOPTOR_API_LOGIN'),
        'doptor_api_url' => env('DOPTOR_API_URL')
    ],

    'cdap' => [
        'api_key' => env('CDAP_API_KEY'),
        'client_id' => env('CDAP_CLIENT_ID'),
        'client_secret' => env('CDAP_CLIENT_SECRET'),
    ],

    'mygov' => [
        'service_guide_url' => env('SERVICE_GUIDE_URL'),
        'mygov_url' => env('MYGOV_URL'),
    ],
];
